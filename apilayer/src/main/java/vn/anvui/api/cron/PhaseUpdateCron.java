package vn.anvui.api.cron;

import java.util.Date;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import vn.anvui.bsn.beans.miinContract.phase.PhaseInterface;
import vn.anvui.dt.utils.DateTimeHelper;

@Component
public class PhaseUpdateCron {
    private static final Logger log = Logger.getLogger(ReportCron.class.getName());
    
    @Autowired
    PhaseInterface phaseInterface;
    
    @Scheduled(cron = "0 3 0 * * *", zone = "GMT+7:00")
    public void updatePhase() {
        Date currentDate = new Date();
        
        phaseInterface.updateContractPhase(DateTimeHelper.getFirstTimeOfDay(currentDate));
    }
}
