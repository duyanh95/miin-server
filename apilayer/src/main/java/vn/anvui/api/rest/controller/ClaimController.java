package vn.anvui.api.rest.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import vn.anvui.bsn.beans.claim.ClaimServiceInterface;
import vn.anvui.bsn.beans.claim.ClaimTypeHelper;
import vn.anvui.bsn.beans.storage.StorageService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.bsn.dto.claim.ClaimCreationRequest;
import vn.anvui.bsn.dto.claim.ClaimDocCreationRequest;
import vn.anvui.bsn.dto.claim.ClaimDocUpdateRequest;
import vn.anvui.bsn.dto.file.FileObject;
import vn.anvui.dt.entity.claim.Claim;
import vn.anvui.dt.entity.claim.ClaimDocs;
import vn.anvui.dt.entity.claim.ClaimType;
import vn.anvui.dt.model.ClaimInfo;

@RestController
@RequestMapping("/claim")
public class ClaimController {
    private Logger log = Logger.getLogger(ClaimController.class.getName());
    
    @Autowired
    StorageService storageService;
    
    @Autowired
    ClaimServiceInterface claimService;
    
    @Autowired
    ClaimTypeHelper claimTypeService;
    
    @GetMapping("/product/{id}/requirement") 
    public ResponseEntity<Object> getProductRequirementDocs(@PathVariable("id") Integer productId) {
        ResponseObject<Object> response = new ResponseObject<Object>();
        
        List<ClaimType> claimTypes = claimTypeService.getProductClaimTypes(productId);
        response.setResponseData(claimTypes);
        
        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PostMapping("/docs")
    @ResponseBody
    public ResponseEntity<Object> uploadDocument(@RequestParam("file") MultipartFile file) {
        ResponseObject<Object> response = new ResponseObject<Object>();
        
        String fileName = null;
        try {
            byte[] bytes = file.getBytes();
            
            log.info(String.valueOf(bytes.length));
            
            fileName = "/pti/claim/docs/" + System.currentTimeMillis() + 
                    file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            
            fileName = storageService.uploadFile(bytes, fileName).getResourceUrl();
            
        } catch (IOException e) {
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }
        
        response.setResponseData(fileName);
        
        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }
    
    @GetMapping("/docs/{doc:.+}")
    @ResponseBody 
    public ResponseEntity<byte[]> getDoc(HttpServletRequest req, @PathVariable("doc") String doc) {
        
        FileObject file = storageService.getFile(req.getRequestURI());
        
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());
        headers.setContentType(MediaType.parseMediaTypes(file.getContentType()).get(0));
        
        return new ResponseEntity<byte[]>(file.getContent(), headers, HttpStatus.OK);
    }
    
    @PutMapping("/{id}/docs")
    @ResponseBody
    public ResponseEntity<Object> updateClaimDoc(@PathVariable("id") Integer id, 
            @RequestBody @Valid ClaimDocCreationRequest req) {
        ResponseObject<Object> response = new ResponseObject<Object>();
        
        ClaimDocs doc = claimService.updateClaimDocuments(id, req);
        response.setResponseData(doc);
        
        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PutMapping("/{id}/docs/response")
    @ResponseBody
    public ResponseEntity<Object> responseToClaimDocs(@PathVariable("id") Integer id,
            @RequestBody @Valid ClaimDocUpdateRequest req) {
        ResponseObject<Object> response = new ResponseObject<Object>();
        
        ClaimDocs doc = claimService.updateClaimDocsRequest(id, req);
        response.setResponseData(doc);
        
        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PostMapping
    @ResponseBody
    public ResponseEntity<Object> createClaim(@RequestBody @Valid ClaimCreationRequest req) {
        ResponseObject<Object> response = new ResponseObject<Object>();
        
        Claim claim = claimService.createClaim(req);
        response.setResponseData(claim);
        
        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }
    
    @GetMapping
    @ResponseBody
    public ResponseEntity<Object> getClaims(@RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer count, @RequestParam(required = false) Date fromDate,
            @RequestParam(required = false) Date toDate, @RequestParam(required = false) String phoneNumber,
            @RequestParam(required = false) String citizenId, @RequestParam(required = false) String contractCode,
            @RequestParam(required = false) Integer productId, @RequestParam(required = false) List<Integer> claimStatuses) {
        ResponseObject<Object> response = new ResponseObject<Object>();
        
        List<ClaimInfo> rs = claimService.getListClaim(page, count, fromDate, toDate, phoneNumber, 
                citizenId, contractCode, productId, claimStatuses);
        response.setResponseData(rs);
        
        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<Object> getClaimsDetailed(@PathVariable("id") Integer id) {
        ResponseObject<Object> response = new ResponseObject<Object>();
        
        Claim claim = claimService.getClaimInfo(id);
        response.setResponseData(claim);
        
        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PutMapping("/{id}")
    @ResponseBody
    public ResponseEntity<Object> updateClaim(@PathVariable(name = "id") Integer id,
            @RequestBody  ClaimCreationRequest req) {
        ResponseObject<Object> response = new ResponseObject<Object>();
        
        Claim claim = claimService.updateClaimInfo(id, req);
        response.setResponseData(claim);
        
        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PutMapping("/{id}/status/{status}")
    @ResponseBody
    public ResponseEntity<Object> updateClaimStatus(@PathVariable(name = "id") Integer id,
            @PathVariable(name = "status") Integer claimStatus) {
        
        ResponseObject<Object> response = new ResponseObject<Object>();
        
        Claim claim = claimService.updateClaimStatus(id, claimStatus);
        response.setResponseData(claim);
        
        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }
}
