package vn.anvui.api.rest.controller;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import vn.anvui.bsn.beans.contactLog.ContactLogService;
import vn.anvui.bsn.beans.contract.ContractService;
import vn.anvui.bsn.beans.customer.CustomerService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.dt.entity.ContactLog;
import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.model.ContractInfo;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.model.IncomeUser;

@RestController
@RequestMapping("/crm")
public class CRMController {
    final Logger log = Logger.getLogger(CRMController.class.getSimpleName());

    @Autowired
    ContractService contractService;

    @Autowired
    CustomerService customerService;

    @Autowired
    ContactLogService logService;

    @CrossOrigin
    @GetMapping("/contracts")
    @ResponseBody
    ResponseEntity<Object> getContracts(@RequestParam(required = false) String citizenId,
            @RequestParam(required = false) String source, @RequestParam(required = false) String medium,
            @RequestParam(required = false) String campaign, @RequestParam(required = false) String customerName,
            @RequestParam(required = false) String email, @RequestParam(required = false) String phoneNumber,
            @RequestParam(required = false) Long fromDate, @RequestParam(required = false) Long toDate,
            @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size,
            @RequestParam(required = false) List<Integer> userIds) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        if (page == null)
            page = 0;
        if (size == null)
            size = 50;

        ContractInfo contractRes;
        contractRes = contractService.listContractPTI(customerName, citizenId, email, phoneNumber, fromDate, toDate,
                page, size, userIds, source, medium, campaign);

        res.setResponseData(contractRes);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/customer/potential")
    @ResponseBody
    ResponseEntity<Object> getPotentialCustomer(@RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size, @RequestParam boolean onContract,
            @RequestParam(required = false) String customerName, @RequestParam(required = false) String phoneNumber,
            @RequestParam(required = false) String email, @RequestParam(required = false) Long toDate,
            @RequestParam(required = false) Long fromDate, @RequestParam(required = false) Integer tag)
            throws AnvuiBaseException {
        ResponseObject<Object> response = new ResponseObject<>();
        if (page == null)
            page = 0;

        List<Customer> customers = customerService.getPotentialCustomerInfo(page, size, customerName, email,
                phoneNumber, fromDate, toDate, tag, onContract);

        response.setResponseData(customers);

        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/income-report")
    public @ResponseBody ResponseEntity<Object> getReport(@RequestParam(required = true) Long fromDate,
            @RequestParam(required = true) Long toDate) {
        ResponseObject<Object> res = new ResponseObject<>();

        try {
            List<IncomeUser> rs = contractService.getIncomeReportByPTI(fromDate, toDate);

            res.setResponseData(rs);
        } catch (AnvuiBaseException e) {
            res.setError(e.getError());
            return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.valueOf(e.getError().getHttpStatus()));
        }

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/contract/{id}")
    public @ResponseBody ResponseEntity<Object> getContractById(@PathVariable("id") Integer id) {
        ResponseObject<Object> res = new ResponseObject<>();

        try {
            ContractWrapper rs = contractService.getContractById(id);

            res.setResponseData(rs);
        } catch (AnvuiBaseException e) {
            res.setError(e.getError());
            return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.valueOf(e.getError().getHttpStatus()));
        }

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @PutMapping("/contract/{id}/confirm")
    public @ResponseBody ResponseEntity<Object> confirmCompletedContract(@PathVariable("id") Integer id)
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        ContractWrapper rs = contractService.confirmContract(id);

        res.setResponseData(rs);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping("/customer/{id}")
    public @ResponseBody ResponseEntity<Object> deleteContact(@PathVariable("id") Integer id)
            throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        customerService.deleteCustomers(id);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/customer/contact-history")
    ResponseEntity<Object> getContactHistory(@RequestParam String phoneNumber) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<Object>();

        List<ContactLog> ctsLog = logService.getContactLog(phoneNumber);

        res.setResponseData(ctsLog);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("contract/{id}/send-mail")
    ResponseEntity<Object> sendContractMail(@PathVariable Integer id) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<Object>();

        ContractWrapper contract = contractService.getContractById(id);

        contractService.sendMailToCustomer(contract);

        res.setResponseData(contract);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/customer/export")
    public ResponseEntity<Object> testQuery(@RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") Date fromDate,
            @RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") Date toDate,
            @RequestParam(required = false) Boolean notExportedOnly) throws AnvuiBaseException {
        if (notExportedOnly == null)
            notExportedOnly = true;

        String data = customerService.exportCsv(fromDate, toDate, notExportedOnly);

        HttpHeaders header = new HttpHeaders();
        header.add("content-type", "application/csv");
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"contacts.csv\"");

        return new ResponseEntity<Object>(data, header, HttpStatus.OK);
    }
}
