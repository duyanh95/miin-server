package vn.anvui.api.rest.global;

import org.springframework.security.core.AuthenticationException;

public class AccountNotActiveLoginExceptionHandler extends AuthenticationException {

	private static final long serialVersionUID = 1L;

	public AccountNotActiveLoginExceptionHandler(String msg) {
		super(msg);
	}

	public AccountNotActiveLoginExceptionHandler(String msg, Throwable t) {
		super(msg, t);
	}
}
