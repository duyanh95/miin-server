package vn.anvui.api.rest.controller;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import vn.anvui.bsn.beans.storage.StorageService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.bsn.dto.file.FileObject;

@RestController
@RequestMapping("/image")
public class ImageStorageController {
    
    private final Logger log = Logger.getLogger(ImageStorageController.class.getName());
    
    @Autowired
    StorageService storageService;
    
    @PostMapping("/upload")
    public @ResponseBody ResponseEntity<Object> uploadImage(@RequestParam("file") MultipartFile file) {
        ResponseObject<Object> response = new ResponseObject<Object>();
        
        String fileName = null;
        try {
            byte[] bytes = file.getBytes();
            fileName = "image/view/" + System.currentTimeMillis() + 
                    file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            
            storageService.uploadFile(bytes, fileName);
        } catch (IOException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }
        
        response.setResponseData("/pti/" + fileName);
        
        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }
    
    @GetMapping("/view/{image:.+}")
    @ResponseBody public ResponseEntity<byte[]> getImage(HttpServletRequest req,
            @PathVariable("image") String image) {
        FileObject file = storageService.getFile(req.getRequestURI());
        
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());
        headers.setContentType(MediaType.parseMediaTypes(file.getContentType()).get(0));
        
        return new ResponseEntity<byte[]>(file.getContent(), headers, HttpStatus.OK);
    }
}
