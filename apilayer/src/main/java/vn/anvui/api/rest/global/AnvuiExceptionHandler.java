package vn.anvui.api.rest.global;

import java.util.logging.Logger;

import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.ResponseObject;

@RestControllerAdvice(basePackages = "vn.anvui.api.rest.controller")
public class AnvuiExceptionHandler extends ResponseEntityExceptionHandler {
    static final Logger log = Logger.getLogger(AnvuiExceptionHandler.class.getSimpleName());
    
    @ExceptionHandler(AnvuiBaseException.class)
    protected ResponseEntity<Object> handlingCustomException(AnvuiBaseException e) {
        ResponseObject<Object> response = new ResponseObject<Object>();
        response.setError(e.getError());
        
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON_UTF8);

        return new ResponseEntity<Object>(response, header,
                HttpStatus.valueOf(e.getError().getHttpStatus()));
    }
    
    @Override
    protected ResponseEntity<Object> handleMissingServletRequestPart(MissingServletRequestPartException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        ResponseObject<Object> response = new ResponseObject<Object>();
        ErrorInfo err = ErrorInfo.PARAMETER_MISSING;
        err.addInfo("error", ex.getMessage());
        response.setError(err);
        
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.valueOf(err.getHttpStatus()));
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        ResponseObject<Object> response = new ResponseObject<Object>();
        ErrorInfo err = ErrorInfo.BAD_REQUEST;
        err.addInfo("error" ,ex.getBindingResult().getFieldError().getField() + " "
                + ex.getBindingResult().getFieldError().getDefaultMessage());
        err.addInfo("field", ex.getBindingResult().getFieldError().getField());
        response.setError(err);

        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.valueOf(err.getHttpStatus()));
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        ResponseObject<Object> response = new ResponseObject<Object>();
        ErrorInfo err = ErrorInfo.PARAMETER_MISSING;
        err.addInfo("field", ex.getParameterName());
        err.addInfo("error", ex.getMessage());
        response.setError(err);

        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.valueOf(err.getHttpStatus()));
    }
    
    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        
        ResponseObject<Object> response = new ResponseObject<Object>();
        ErrorInfo err = ErrorInfo.TYPE_MISMATCHED;
        err.addInfo("error", ex.getMessage());
        response.setError(err);

        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.valueOf(err.getHttpStatus()));
    }
    
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        ResponseObject<Object> response = new ResponseObject<Object>();
        ErrorInfo err = ErrorInfo.BAD_REQUEST;
        err.addInfo("error", ex.getMessage());
        
        response.setError(err);
        
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.valueOf(err.getHttpStatus()));
    }
}
