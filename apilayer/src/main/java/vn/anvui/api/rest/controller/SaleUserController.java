package vn.anvui.api.rest.controller;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import vn.anvui.bsn.beans.contract.ContractService;
import vn.anvui.bsn.beans.customer.CustomerService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.bsn.dto.customer.CoupleRequest;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.model.ContractInfo;
import vn.anvui.dt.model.ContractWrapper;

@RestController
@RequestMapping("/sale")
public class SaleUserController {
    public static final Logger log = Logger.getLogger(SaleUserController.class.getName());

    @Autowired
    CustomerService customerService;

    @Autowired
    ContractService contractService;

    @GetMapping("/contract")
    @ResponseBody
    public ResponseEntity<Object> getCodContract(@RequestParam(required = false) String citizenId,
            @RequestParam(required = false) String customerName, @RequestParam(required = false) String email,
            @RequestParam(required = false) String phoneNumber, @RequestParam(required = false) Long fromDate,
            @RequestParam(required = false) Long toDate, @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size) throws AnvuiBaseException {
        if (page == null)
            page = 0;
        if (size == null)
            size = 50;

        ResponseObject<Object> res = new ResponseObject<>();

        ContractInfo rs = contractService.getSaleContract(citizenId, customerName, email, phoneNumber, fromDate, toDate,
                page, size);

        res.setResponseData(rs);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/contract/{id}/completed")
    @ResponseBody
    public ResponseEntity<Object> completedCodContract(@PathVariable("id") Integer id) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        Contract contract = contractService.completeCodContract(id);

        res.setResponseData(contract);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/customer/{id}/contract")
    @ResponseBody
    public ResponseEntity<Object> createContractFromCustomer(@RequestBody CoupleRequest req,
            @PathVariable("id") Integer id) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        ContractWrapper contract = customerService.createContractFromExistedCustomer(req, id);

        res.setResponseData(contract);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }

}
