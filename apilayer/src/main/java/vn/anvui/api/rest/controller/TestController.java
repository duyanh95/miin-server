package vn.anvui.api.rest.controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.core.io.UrlResource;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.DocumentException;

import vn.anvui.api.amqp.MessageProducer;
import vn.anvui.bsn.beans.contract.ContractService;
import vn.anvui.bsn.beans.incomeReport.IncomeReportService;
import vn.anvui.bsn.beans.mailing.MailingService;
import vn.anvui.bsn.beans.miinContract.MiinContractServiceDispenser;
import vn.anvui.bsn.beans.miinContract.general.MiinGeneralContractService;
import vn.anvui.bsn.beans.miinContract.health.HealthContractPdf;
import vn.anvui.bsn.beans.miinContract.health.HealthContractService;
import vn.anvui.bsn.beans.miinContract.health.HealthContractServiceImpl;
import vn.anvui.bsn.beans.misc.MiscellaneousService;
import vn.anvui.bsn.beans.notification.NotificationService;
import vn.anvui.bsn.beans.sms.SmsService;
import vn.anvui.bsn.beans.transaction.PayooMailService;
import vn.anvui.bsn.beans.user.UserService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.bsn.dto.mailing.AttachmentFile;
import vn.anvui.bsn.dto.mailing.AttachmentFile.FileType;
import vn.anvui.bsn.dto.mailing.MailRequest;
import vn.anvui.bsn.dto.notification.NotificationDto;
import vn.anvui.dt.entity.History;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.repos.contract.ContractRepository;
import vn.anvui.dt.repos.customer.CustomerRepository;
import vn.anvui.dt.repos.insurancePackage.InsurancePackageRepository;
import vn.anvui.dt.repos.userClosure.UserClosureRepository;
import vn.anvui.dt.utils.DateTimeHelper;

@RestController
@RequestMapping
public class TestController {
    
    Logger log = Logger.getLogger(TestController.class.getName());
    
    @Autowired
    MailingService sendGridEmailSevice;
    
    @Autowired
    ContractService contractService;
    
    @Autowired
    ContractRepository contractRepo;
    
    @Autowired
    UserClosureRepository closureRepo;
    
    @Autowired
    CustomerRepository customerRepo;
    
    @Autowired
    MessageProducer messageProducer;
    
    @Autowired
    SmsService smsService;
    
    @Autowired
    UserService userService;
    
    @Autowired
    MiscellaneousService miscService;
    
    @Autowired
    IncomeReportService reportService;
    
    @Autowired
    PayooMailService payooMailService;
    
    @GetMapping("/test/time")
    public Long getTime() {
        return DateTimeHelper.getFirstTimeOfDay(new Date(), TimeZone.getTimeZone("GMT+7:00")).getTime();
    }
    
    @GetMapping("/test/ping")
    public String ping() {
        return "pong";
    }

    @GetMapping("/test/payoo-mail")
    public String testPayooMail(@RequestParam String name, @RequestParam String email, @RequestParam String paymentId) throws IOException {
        
        payooMailService.sendGuideMail(email, name, paymentId);
        
        return "done";
    }
        
//    @PostMapping("/add-closure")
//    public ResponseEntity<Object> addClosure(@RequestParam Integer userId, 
//            @RequestParam(required = false) Integer parentId) {
//        ResponseObject<Object> response = new ResponseObject<Object>();
//        
//        if (parentId == null) {
//            UserClosure closure = new UserClosure(userId, 1);
//            closure = closureRepo.save(closure);
//            
//        } else {
//            if (!userRepo.exists(parentId))
//                throw new AnvuiBaseException(ErrorInfo.USER_PARENT_NOT_EXISTED);
//            
//            Integer depth = closureRepo.findDepthFromParent(parentId) + 1;
//            
//            List<UserClosure> ascendanceClosures = closureRepo.findAscendanceFromId(parentId);
//            List<UserClosure> closures = new LinkedList<>();
//            
//            ascendanceClosures.forEach((ascClosure) -> {
//                UserClosure closure = ascClosure.clone();
//                closure.setDescendanceId(userId);
//                closures.add(closure);
//            });
//            
//            closures.add(new UserClosure(userId, depth));
//            closureRepo.save(closures);
//        }
//        
//        response.setSuccessMessage("success");
//        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK); 
//    }

    @PostMapping("/test/send-test-mail")
    public String send(@RequestBody @Valid MailRequest req) {
        log.info("test mail requested");
        
        boolean success = false;
        try {
            AttachmentFile attachment = new AttachmentFile("/tmp/rs.pdf", "CNBH.pdf", FileType.PDF);
            List<AttachmentFile> files = new ArrayList<>(Arrays.asList(attachment));
            
            success = sendGridEmailSevice.sendEmailWithAttachment(req.getToList(), req.getCcList(), req.getBccList(), req.getSubject(), 
                    req.getContent(), "", files);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        if (success)
            return "success";
        else
            return "fail";
    }
    
    @PostMapping("/test/test-contract-mail")
    public String pdf(@RequestParam Integer id) {
        try { 
            ContractWrapper contract = contractService.getContractById(id);
            
            messageProducer.sendMailQueue(contract);
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
            return "fail";
        }
        
        return "success";
    }
    
    @PostMapping("/test/send-sms")
    public ResponseEntity<Object> sendSms() throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<Object>();

        smsService.sendSms(Arrays.asList("0362205275"), "test sms");

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);   
    }

    @GetMapping("/test/report")
    public ResponseEntity<Object> testReport(
            @RequestParam @DateTimeFormat(pattern="dd-MM-yyyy") Date date,
            @RequestParam Integer days) throws Exception {
        ResponseObject<Object> res = new ResponseObject<Object>();
        
        reportService.createIncomeReportByDay(days, date);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);   
    }
    
//    @GetMapping("test/query")
//    public ResponseEntity<Object> testQuery() throws Exception {
//        ResponseObject<Object> res = new ResponseObject<Object>();
//        
//        List<ContractWrapper> rs = contractRepo.getContractWithAgencyUser();
//        res.setResponseData(rs);
//        
//        res.setSuccessMessage("success");
//        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);   
//    }
    
    @Autowired
    InsurancePackageRepository pkgRepo;
    
    @Autowired
    ObjectMapper mapper;
    
    @CacheEvict(allEntries = true, cacheNames = {"package", "token"})
    @DeleteMapping("/cache/all")
    public String deleteAllCache() {
        return "ok";
    }
    
    @Autowired
    HealthContractService healthService;
    
    @Autowired
    MiinGeneralContractService generalService;
    
    @Autowired
    MiinContractServiceDispenser serviceDispenser;
    
    @GetMapping("/test/mail")
    public ResponseEntity<Object> testMail(@RequestParam Integer contractId) {
        ResponseObject<Object> res = new ResponseObject<Object>();
        
        ContractWrapper contract = contractService.getContractById(contractId);
        
        messageProducer.sendMailQueue(contract);
        
        res.setResponseData(contract);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @Resource(name = "redisObjectTemplate")
    private ValueOperations<String, Object> valueOps;
    
    @GetMapping("/test/redis")
    public ResponseEntity<Object> testRedis(@RequestParam String key, @RequestParam String value) throws JsonProcessingException {
        History hist = new History();
        hist.setUserId(1);
        hist.setPackageId(1);
        hist.setProductId(1);
        
        valueOps.set(key, hist, 200, TimeUnit.SECONDS);
        
        return new ResponseEntity<Object>(valueOps.get(key), new HttpHeaders(), HttpStatus.OK);
    }
    
    @Autowired
    HealthContractPdf contractPdf;
    
    @GetMapping("test/pdf")
    public ResponseEntity<Object> testPdf(@RequestParam Integer contractId) {
        ResponseObject<Object> res = new ResponseObject<Object>();
        
        String pdfFile = "/tmp/" + System.currentTimeMillis() + ".pdf";
        
        ContractWrapper wrapper = contractService.getContractById(contractId);
        
        File file = null;
        try {
            file = contractPdf.createPdfFile(pdfFile, wrapper);
        } catch (DocumentException | IOException e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }
        
        Path path = Paths.get(file.getAbsolutePath());
        org.springframework.core.io.Resource resource = null;

        try {
            resource = new UrlResource(path.toUri());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if (resource.exists()) {
            return ResponseEntity.ok().header("content-type", "application/pdf")
                    .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + resource.getFilename() + "\"")
                    .body(resource);

        } else {
            return new ResponseEntity<Object>("File Not Found ", HttpStatus.NOT_FOUND);
        }
    }
    
    @PostMapping("/test/noti") 
    public ResponseEntity<Object> testReport(@RequestParam Integer contractId) throws Exception {
        ResponseObject<Object> res = new ResponseObject<Object>();
        
        ContractWrapper wrapper = contractService.getContractById(contractId);
        
        healthService.sendStatusNotify(wrapper.getContract(), true);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
}

