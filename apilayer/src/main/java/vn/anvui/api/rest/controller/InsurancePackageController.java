package vn.anvui.api.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import vn.anvui.bsn.beans.insurancePackage.InsurancePackageService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.dt.entity.InsurancePackage;

@RestController
@RequestMapping("/packages")
public class InsurancePackageController {

    @Autowired
    InsurancePackageService packageService;

    @CrossOrigin
    @GetMapping
    @ResponseBody
    ResponseEntity<Object> getAllPackages() throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<>();

        List<InsurancePackage> list = packageService.getAllPackages();
        res.setResponseData(list);

        res.setSuccessMessage("Success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
}
