package vn.anvui.api.rest.controller;

import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import vn.anvui.bsn.beans.role.RoleService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.bsn.dto.role.RoleRequest;
import vn.anvui.dt.entity.Role;

@RestController
@RequestMapping("/role")
public class RoleController {
    final Logger log = Logger.getLogger(RoleController.class.getSimpleName());

    @Autowired
    RoleService roleService;

    @PostMapping
    @ResponseBody
    ResponseEntity<Object> createRole(@RequestBody @Valid RoleRequest req) throws AnvuiBaseException {
        ResponseObject<Object> res = new ResponseObject<Object>();

        Role role = roleService.createRole(req);

        res.setResponseData(role);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
}
