package vn.anvui.api.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vn.anvui.bsn.beans.notification.NotificationService;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.dt.entity.Notification;

@RestController
@RequestMapping("/miin")
public class MiinNotificationController {
    
    @Autowired
    NotificationService notiService;
    
    @GetMapping("/notifications") 
    public ResponseEntity<Object> listNotificationCustomer(
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer count) throws Exception {
        if (page == null)
            page = 0;
        if (count == null || count > 200)
            count = 200;
        
        ResponseObject<Object> res = new ResponseObject<Object>();
        
        List<Notification> notis = notiService.listNotification(page, count, false);
        res.setResponseData(notis);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);   
    }
    
    @GetMapping("/notifications/agency") 
    public ResponseEntity<Object> listNotificationAgency(
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer count) throws Exception {
        if (page == null)
            page = 0;
        if (count == null || count > 200)
            count = 200;
        
        ResponseObject<Object> res = new ResponseObject<Object>();
        
        List<Notification> notis = notiService.listNotification(page, count, true);
        res.setResponseData(notis);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);   
    }
    
    @PostMapping("/notifications/read")
    public ResponseEntity<Object> markAsRead(
            @RequestBody NotificationId ids) throws Exception {
        ResponseObject<Object> res = new ResponseObject<Object>();
        
        notiService.markAsRead(ids.getIds());
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);   
    }
    
    public static class NotificationId {
        private List<Integer> ids;

        public List<Integer> getIds() {
            return ids;
        }

        public void setIds(List<Integer> ids) {
            this.ids = ids;
        }
    }
}
