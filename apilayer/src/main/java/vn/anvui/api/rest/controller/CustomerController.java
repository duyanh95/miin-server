package vn.anvui.api.rest.controller;

import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import vn.anvui.api.amqp.MessageProducer;
import vn.anvui.bsn.beans.contract.ContractService;
import vn.anvui.bsn.beans.customer.CustomerService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.bsn.dto.customer.CoupleRequest;
import vn.anvui.bsn.dto.customer.CustomerRequest;
import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.model.ContractWrapper;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    Logger log = Logger.getLogger(CustomerController.class.getName());

    @Autowired
    CustomerService customerService;

    @Autowired
    ContractService contractService;

    @Autowired
    MessageProducer messageProducer;

    @CrossOrigin
    @PostMapping
    @ResponseBody
    ResponseEntity<Object> createCustomer(@RequestBody @Valid CustomerRequest req) throws AnvuiBaseException {
        ResponseObject<Object> response = new ResponseObject<Object>();
        
        messageProducer.createCustomerQueue(req);
        
        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping("/love_insurance")
    @ResponseBody
    ResponseEntity<Object> createLoveInsuranceContract(@RequestBody @Valid CoupleRequest req) throws AnvuiBaseException {
        ResponseObject<Object> response = new ResponseObject<Object>();
        ContractWrapper contract = customerService.createCouple(req, ContractStatus.CREATED);

        response.setResponseData(contract);

        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    ResponseEntity<Object> getCustomer(@PathVariable("id") Integer id) throws AnvuiBaseException {
        ResponseObject<Object> response = new ResponseObject<>();

        Customer customer = customerService.get(id);

        response.setResponseData(customer);

        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    ResponseEntity<Object> updateCustomer(@PathVariable("id") Integer id, @RequestBody CustomerRequest req)
            throws AnvuiBaseException {

        ResponseObject<Object> response = new ResponseObject<Object>();
        Customer customer = customerService.updateInfo(id, req);

        response.setResponseData(customer);

        response.setSuccessMessage("success");
        return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
    }
}
