package vn.anvui.api.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import vn.anvui.bsn.beans.misc.MiscellaneousService;
import vn.anvui.bsn.dto.ResponseObject;

@RestController
@RequestMapping("/misc")
public class MiscController {
    
    @Autowired
    MiscellaneousService miscService;

    @GetMapping("/provinces")
    public ResponseEntity<Object> listProvince() {
        ResponseObject<Object> res = new ResponseObject<Object>();

        List<String> province = miscService.listProvince();
        res.setResponseData(province);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    
}
