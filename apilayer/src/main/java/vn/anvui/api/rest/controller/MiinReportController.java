package vn.anvui.api.rest.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import vn.anvui.bsn.beans.contract.ContractService;
import vn.anvui.bsn.beans.incomeReport.IncomeReportService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.ResponseObject;
import vn.anvui.dt.entity.DailyIncomeReport;
import vn.anvui.dt.model.IncomeUser;
import vn.anvui.dt.model.MonthsReport;

@RestController
@RequestMapping("/miin/report")
public class MiinReportController {
    
    @Autowired
    IncomeReportService reportService;
    
    @Autowired
    ContractService contractService;
    
    @GetMapping("/months")
    @ResponseBody
    public ResponseEntity<Object> getMonthsReport(@RequestParam(required = false) Integer productId) {
        ResponseObject<Object> res = new ResponseObject<>();
        
        List<MonthsReport> rep = reportService.getMonthsReport(productId);
        res.setResponseData(rep);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @GetMapping("/days")
    @ResponseBody
    public ResponseEntity<Object> getDailyReport(@DateTimeFormat(pattern = "dd-MM-yyyy") @RequestParam(required = false) Date fromDate,
            @DateTimeFormat(pattern = "dd-MM-yyyy") @RequestParam(required = false) Date toDate,
            @RequestParam(required = false) Integer productId) {
        ResponseObject<Object> res = new ResponseObject<>();
        
        List<DailyIncomeReport> rep = reportService.getDailyIncomeReport(fromDate, toDate, productId);
        
        res.setResponseData(rep);
        
        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
    
    @GetMapping("/sub-agents")
    @ResponseBody
    public ResponseEntity<Object> getSubagentsResport(
            @DateTimeFormat(pattern = "dd-MM-yyyy") @RequestParam(required = false) Date fromDate,
            @DateTimeFormat(pattern = "dd-MM-yyyy") @RequestParam(required = false) Date toDate,
            @RequestParam(required = false) Integer userId,
            @RequestParam(required = false) Integer productId,
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer count) throws AnvuiBaseException {
        if (page == null)
            page = 0;
        if (count == null || count >= 200)
            count = 50;

        ResponseObject<Object> res = new ResponseObject<Object>();

        List<IncomeUser> rsList = reportService.getIncomeReportByAgency(fromDate, toDate, 
                userId, productId, page, count);
        res.setResponseData(rsList);

        res.setSuccessMessage("success");
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.OK);
    }
}
