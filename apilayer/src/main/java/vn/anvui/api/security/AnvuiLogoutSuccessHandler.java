package vn.anvui.api.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import vn.anvui.bsn.auth.AnvuiAuthenticationToken;
import vn.anvui.bsn.auth.CredentialService;

public class AnvuiLogoutSuccessHandler implements LogoutSuccessHandler {

	@Autowired
	private CredentialService credentialService;

	@Override
	public void onLogoutSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {

		if (authentication instanceof AnvuiAuthenticationToken) {

			AnvuiAuthenticationToken result = (AnvuiAuthenticationToken) authentication;
			
			// Remove access token
			credentialService.remove(result.getToken());
		}
	}
}
