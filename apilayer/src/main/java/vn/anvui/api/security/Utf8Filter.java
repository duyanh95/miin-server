package vn.anvui.api.security;

import org.springframework.web.filter.CharacterEncodingFilter;

public class Utf8Filter extends CharacterEncodingFilter {

	public Utf8Filter() {
		super.setEncoding("utf-8");
	}
}
