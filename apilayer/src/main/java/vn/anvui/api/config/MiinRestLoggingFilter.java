package vn.anvui.api.config;

import java.util.Arrays;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.AbstractRequestLoggingFilter;

public class MiinRestLoggingFilter extends AbstractRequestLoggingFilter {
    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        Collection<String> excludeUrls = Arrays.asList("/test/ping");
        AntPathMatcher matcher = new AntPathMatcher();
        
        return excludeUrls.stream().anyMatch(p -> matcher.match(p, request.getServletPath()));
    }
    
    @Override
    protected void beforeRequest(HttpServletRequest request, String message) {
        logger.info(message);
    }
    
    @Override
    protected void afterRequest(HttpServletRequest request, String message) {
        logger.info(message);
    }
}
