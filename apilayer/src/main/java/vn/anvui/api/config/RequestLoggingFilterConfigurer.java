package vn.anvui.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RequestLoggingFilterConfigurer {
    
    @Bean
    public MiinRestLoggingFilter requestLoggingFilter() {
        MiinRestLoggingFilter logFilter = new MiinRestLoggingFilter();
        logFilter.setIncludeHeaders(true);
        logFilter.setIncludePayload(true);
        logFilter.setMaxPayloadLength(1024);
        logFilter.setIncludeQueryString(true);
        
        return logFilter;
    }
        
}
