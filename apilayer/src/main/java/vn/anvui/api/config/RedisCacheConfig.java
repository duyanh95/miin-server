package vn.anvui.api.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.auth.oauth2.AccessToken;

@Configuration
public class RedisCacheConfig {
//    @Bean
//    public CacheManager cacheManager() {
//        return new EhCacheCacheManager(ehCacheCacheManager().getObject());
//    }
// 
//    @Bean
//    public EhCacheManagerFactoryBean ehCacheCacheManager() {
//        EhCacheManagerFactoryBean factory = new EhCacheManagerFactoryBean();
//        factory.setConfigLocation(new ClassPathResource("ehcache.xml"));
//        factory.setShared(true);
//        return factory;
//    }
    @Value("${spring.redis.port}")
    String redisPort;
    
    @Value("${spring.redis.host}")
    String redisHost;
    
    @Value("${spring.redis.password}")
    String redisPassword;
    
    @Autowired
    ObjectMapper mapper;
    
    @Bean
    JedisConnectionFactory jedisConnectionFactory() {
        JedisConnectionFactory connectionFactory = new JedisConnectionFactory();
        connectionFactory.setHostName(redisHost);
        connectionFactory.setPort(Integer.parseInt(redisPort));
        connectionFactory.setPassword(redisPassword);
        
        return connectionFactory;
    }
    
    @Bean
    public RedisTemplate<String, Object> redisTemplate() {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(jedisConnectionFactory());
        
        return template;
    }
    
    @Bean
    public RedisTemplate<String, Object> redisObjectTemplate() {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(jedisConnectionFactory());

        template.setValueSerializer(new GenericJackson2JsonRedisSerializer(mapper));
        
        return template;
    }
    
    @Bean
    public RedisCacheManager redisCachemanager() {
        RedisCacheManager cacheManager = new RedisCacheManager(redisTemplate());
        
        cacheManager.setUsePrefix(true);
        cacheManager.setDefaultExpiration(3600);
        
        Map<String, Long> expires = new HashMap<>();
        expires.put("token", 1600L);
        cacheManager.setExpires(expires);
        
        return cacheManager;
    }
}
