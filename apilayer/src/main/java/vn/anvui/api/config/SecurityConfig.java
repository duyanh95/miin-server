package vn.anvui.api.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.csrf.CsrfFilter;

import vn.anvui.api.security.AnvuiLogoutSuccessHandler;
import vn.anvui.api.security.AuthenticationFilter;
import vn.anvui.api.security.CORSFilter;
import vn.anvui.api.security.RestAuthenticationEntryPoint;
import vn.anvui.api.security.TokenAuthenticationFilter;
import vn.anvui.api.security.UserAuthenticationProvider;
import vn.anvui.api.security.Utf8Filter;
import vn.anvui.dt.common.ApiConts;
import vn.anvui.dt.enumeration.PTIPrivileges;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
@ComponentScan(basePackages = { ApiConts.SPRING_SECURITY_PACKAGE })
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    
    public static final String CLIENT_ID = "stset";

    @Autowired
    private RestAuthenticationEntryPoint authEntryPoint;

    @Bean
    public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public AuthenticationProvider userAuthenticationProvider() {
        return new UserAuthenticationProvider();
    }

    @Bean
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(userAuthenticationProvider());
    }
    

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().httpBasic().disable().cors().disable()
        
                .exceptionHandling().authenticationEntryPoint(authEntryPoint)

                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)

                .and().authorizeRequests()
                .antMatchers(HttpMethod.GET, "/packages")
                .permitAll()
                .antMatchers(HttpMethod.GET, "/contract/transaction/report")
                .hasAuthority(PTIPrivileges.READ_ADMIN.getValue())
                .antMatchers("/contract/transact/**", "/contract/transaction/**",
                        "/contract/print/*")
                .permitAll()
                .antMatchers("/customers/**")
                .permitAll()
                
                .antMatchers("/contract/not-exported", "/contract/export", "/contract/reject-reason",
                        "/contract/*/cancel-cod")
                .hasAuthority(PTIPrivileges.UPDATE_ADMIN.getValue())
                .antMatchers(HttpMethod.PUT, "/contract/*")
                .hasAuthority(PTIPrivileges.UPDATE_CONTRACT.getValue())
                .antMatchers(HttpMethod.GET, "/contract/preview")
                .hasAuthority(PTIPrivileges.READ_CONTRACT.getValue())
                .antMatchers(HttpMethod.DELETE, "/contact/*")
                .hasAnyAuthority(PTIPrivileges.DELETE_CONTRACT.getValue())
                .antMatchers(HttpMethod.DELETE, "/cache/all")
                .hasAnyAuthority(PTIPrivileges.DELETE_ADMIN.getValue())
                
                .antMatchers(HttpMethod.POST, "/customer/reject-note")
                .hasAnyAuthority(PTIPrivileges.CREATE_ADMIN.getValue())
                .antMatchers("/crm/print/**", "/crm/customer/export").permitAll()
                .antMatchers(HttpMethod.DELETE, "/crm/customer/*")
                .hasAnyAuthority(PTIPrivileges.UPDATE_ADMIN.getValue())
                .antMatchers(HttpMethod.PUT, "/crm/*/confirm")
                .hasAnyAuthority(PTIPrivileges.UPDATE_CONTRACT.getValue())
                .antMatchers(HttpMethod.GET, "/crm/contracts")
                .hasAnyAuthority(PTIPrivileges.READ_ADMIN.getValue(), PTIPrivileges.MARKETING_PARTNER.getValue())
                .antMatchers("/crm/**")
                .hasAnyAuthority(PTIPrivileges.READ_ADMIN.getValue())
                
                .antMatchers(HttpMethod.POST, "/crm/contract")
                .hasAnyAuthority(PTIPrivileges.CREATE_CONTRACT.getValue())
                
                .antMatchers("/user/agency/register")
                .authenticated()
                .antMatchers("/user/*/agency/confirm")
                .hasAnyAuthority(PTIPrivileges.UPDATE_ADMIN.getValue())
                .antMatchers("/user/agency/pending-request")
                .hasAnyAuthority(PTIPrivileges.UPDATE_ADMIN.getValue())
                .antMatchers("/user/*/reset-password")
                .hasAnyAuthority(PTIPrivileges.READ_AGENCY.getValue())
                .antMatchers(HttpMethod.POST, "/user/agency")
                .hasAnyAuthority(PTIPrivileges.CREATE_AGENCY.getValue())
                .antMatchers(HttpMethod.GET, "/user/agency")
                .hasAnyAuthority(PTIPrivileges.READ_AGENCY.getValue())
                .antMatchers(HttpMethod.DELETE, "/user/agency/*")
                .hasAnyAuthority(PTIPrivileges.DELETE_AGENCY.getValue())
                .antMatchers("/user/admin", "/role/**")
                .hasAuthority(PTIPrivileges.CREATE_ADMIN.getValue())
                .antMatchers(HttpMethod.POST, "/user/agency/*/token")
                .hasAnyAuthority(PTIPrivileges.CREATE_ADMIN.getValue())
                
                .antMatchers(HttpMethod.POST, "/user/pti-user")
                .hasAnyAuthority(PTIPrivileges.CREATE_ADMIN.getValue())
                .antMatchers(HttpMethod.GET, "/user/pti-user")
                .hasAnyAuthority(PTIPrivileges.DELETE_AGENCY.getValue())
                .antMatchers(HttpMethod.GET, "/user/agency")
                .hasAnyAuthority(PTIPrivileges.READ_ADMIN.getValue())
                .antMatchers(HttpMethod.GET, "user/agency/children")
                .hasAnyAuthority(PTIPrivileges.READ_USER.getValue())
                
                .antMatchers(HttpMethod.POST, "/user/agency-user")
                .hasAnyAuthority(PTIPrivileges.CREATE_USER.getValue())
                .antMatchers(HttpMethod.GET, "/user/agency-user")
                .hasAnyAuthority(PTIPrivileges.READ_USER.getValue())
                .antMatchers(HttpMethod.DELETE, "/user/agency-user/*")
                .hasAnyAuthority(PTIPrivileges.DELETE_USER.getValue())
                
                
                .antMatchers(HttpMethod.POST, "agency/contract/*/transfer-confirm")
                .hasAnyAuthority(PTIPrivileges.UPDATE_ADMIN.getValue())
                .antMatchers(HttpMethod.POST, "/agency/contract")
                .hasAnyAuthority(PTIPrivileges.CREATE_CONTRACT.getValue())
                .antMatchers(HttpMethod.GET, "/agency/contract")
                .hasAnyAuthority(PTIPrivileges.READ_CONTRACT.getValue())
                .antMatchers(HttpMethod.GET, "/agency/contract/income-report")
                .hasAnyAuthority(PTIPrivileges.READ_AGENCY.getValue())
                
                .antMatchers(HttpMethod.POST, "miin/agency/product/*/contract")
                .hasAnyAuthority(PTIPrivileges.CREATE_CONTRACT.getValue())
                .antMatchers("/miin/report/**")
                .hasAnyAuthority(PTIPrivileges.READ_AGENCY.getValue())
                .antMatchers("/miin/notifications/agency")
                .hasAnyAuthority(PTIPrivileges.READ_AGENCY.getValue())
                
                .antMatchers(HttpMethod.POST, "/sale/contract", "/sale/customer/*/contract")
                .hasAnyAuthority(PTIPrivileges.CREATE_CONTRACT.getValue())
                .antMatchers(HttpMethod.GET, "/sale/contract")
                .hasAnyAuthority(PTIPrivileges.READ_ADMIN.getValue())
                .antMatchers(HttpMethod.POST, "/sale/contract/*/completed")
                .hasAnyAuthority(PTIPrivileges.UPDATE_ADMIN.getValue())
                
                .antMatchers("agency/contract/income-report")
                .hasAnyAuthority(PTIPrivileges.READ_AGENCY.getValue())
                
                .antMatchers("/role/**")
                .hasAnyAuthority(PTIPrivileges.CREATE_ADMIN.getValue())
                
                .antMatchers(HttpMethod.POST, "/banner", "/promo")
                .hasAnyAuthority(PTIPrivileges.CREATE_ADMIN.getValue())
                .antMatchers(HttpMethod.DELETE, "/banner/*", "/promo/*")
                .hasAnyAuthority(PTIPrivileges.DELETE_ADMIN.getValue())
                .antMatchers(HttpMethod.GET, "/banner", "/promo").permitAll()
                .antMatchers("/login/send-otp, /login/verify").authenticated()
                .antMatchers("/history").authenticated()
                
                // Allow some requests can be accessed without authentication
                .antMatchers("/prometheus", "/test/**", "/tracking/**", "/*", "/health/**", "/login/**",
                        "/image/view/*", "/misc/*", "/miin/products", "/miin/product/*/packages",
                        "/claim/product/3/requirement")
                .permitAll()
                
                .and().authorizeRequests()
                // all other request need to be authenticated
                .anyRequest().authenticated()

                .and()
                // logout handler
                .logout().logoutUrl(ApiConts.LOGOUT_URL).invalidateHttpSession(false)
                .logoutSuccessHandler(anvuiLogoutSuccessHandler())

                .and()
                // Filters
                .addFilterBefore(new Utf8Filter(), CsrfFilter.class)
                .addFilterBefore(new CORSFilter(), ChannelProcessingFilter.class)
                
                .addFilterBefore(new AuthenticationFilter(ApiConts.LOGIN_URL, authenticationManager()),
                        UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(tokenAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);

                // token filter must be before logout filter to have Logout
                // handler works
                //.addFilterBefore(tokenAuthenticationFilter(), LogoutFilter.class);
        
        http.headers().frameOptions().disable();
    }

    @Bean
    public TokenAuthenticationFilter tokenAuthenticationFilter() {
        return new TokenAuthenticationFilter();
    }

    @Bean
    public AnvuiLogoutSuccessHandler anvuiLogoutSuccessHandler() {
        return new AnvuiLogoutSuccessHandler();
    }
}
