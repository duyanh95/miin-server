package vn.anvui.dt.enumeration;

public enum TransactionSource {
    BHTY(0),
    MIIN_ANDROID(1),
    MIIN_IOS(2),
    AGENCY(3);
    
    private final int value;
    
    TransactionSource(int newValue) {
        value = newValue;
    }

    public int getValue() {
        return value;
    }
}
