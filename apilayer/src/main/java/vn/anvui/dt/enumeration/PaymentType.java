package vn.anvui.dt.enumeration;

public enum PaymentType {
    PAYOO(0),
    EPAY(1),
    NAPAS(2),
    VTCPAY(3),
    ZALOPAY(4),
    VIPAY(5),
    VIETTEL(6);
    
    final int value;
    
    PaymentType(final int newValue)
    {
        value = newValue;
    }

    public int getValue()
    {
        return value;
    }
}
