package vn.anvui.dt.enumeration;

public enum NotificationTarget {
    CUSTOMER(0),
    AGENCY(1),
    ALL(2);
    
    final int value;
    
    NotificationTarget(final int newValue)
    {
        value = newValue;
    }

    public int getValue()
    {
        return value;
    }
}
