package vn.anvui.dt.enumeration;

public enum TransactionStatus {
    PENDING(0),
    SUCCESS(1),
    FAIL(2),
    EXPIRED(3);
    
    final int value;
    
    TransactionStatus(final int newValue)
    {
        value = newValue;
    }

    public int getValue()
    {
        return value;
    }
    
}
