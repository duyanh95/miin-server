package vn.anvui.dt.enumeration;

public enum CustomerTag {
    NOT_PROCESS(0),
    PENDING(1),
    DONE(2),
    COD(3),
    APPROVE(4),
    ONLINE(5),
    PRE_COD(6);
    
    final int value;
    
    CustomerTag(final int newValue) {
        value = newValue;
    }
    
    public int getValue() {
        return value;
    }
}
