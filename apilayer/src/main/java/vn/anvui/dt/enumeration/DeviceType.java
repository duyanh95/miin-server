package vn.anvui.dt.enumeration;

public enum DeviceType {
    ANDROID(0),
    IOS(1);
    
    final int value;
    
    DeviceType(final int newValue)
    {
        value = newValue;
    }

    public int getValue()
    {
        return value;
    }
}
