package vn.anvui.dt.enumeration;

public enum UserType {

    ADMIN("ADMIN"), 
    AGENCY("AGENCY"), 
    AGENCY_USER("AGENCY_USER"),
    PTI_USER("PTI_USER"),
    CUSTOMER("CUSTOMER"),
    COLLAB("COLLAB")
    ;
    
    private final String value;

    UserType(final String newValue)
    {
        value = newValue;
    }

    public String getValue()
    {
        return value;
    }
    
}
