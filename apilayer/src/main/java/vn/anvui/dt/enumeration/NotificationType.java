package vn.anvui.dt.enumeration;

public enum NotificationType {
    TRANSACTION_SUCCESS(0),
    TRANSACTION_FAIL(1),
    WARNING(2),
    TERMINATE(3);
    
    final int value;
    
    NotificationType(final int newValue)
    {
        value = newValue;
    }

    public int getValue()
    {
        return value;
    }
}
