package vn.anvui.dt.enumeration;

public enum ContactLogType {
    CREATED(0),
    UPDATED(1),
    REJECTED(2),
    COD(3),
    ONLINE(4),
    DELETED(5);
    
    final int value;
    
    ContactLogType(final int newValue)
    {
        value = newValue;
    }

    public int getValue()
    {
        return value;
    }
}
