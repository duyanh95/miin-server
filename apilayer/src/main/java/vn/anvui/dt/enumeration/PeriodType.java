package vn.anvui.dt.enumeration;

public enum PeriodType {
    WEEKLY(0),
    MONTHLY(1),
    YEARLY(2);
    
    final int value;
    
    private static final String[] periodTypeStr = {"tuần", "tháng", "năm"};
    
    public static String getPeriodTypeStr(int val) {
        return periodTypeStr[val];
    }
    
    PeriodType(final int newValue) {
        value = newValue;
    }

    public int getValue() {
        return value;
    }
}
