package vn.anvui.dt.enumeration;

public enum PhaseActionType {
    WARNING_NOTIFY(0),
    WARNING_EMAIL(1),
    WARNING_SMS(2),
    TERMINATE_NOTIFY(3),
    TERMINATE_EMAIL(4),
    TERMINATE_SMS(5),
    EXTEND_NOTIFY(6),
    EXTEND_EMAIL(7),
    EXTEND_SMS(8);
    
    final int value;
    
    PhaseActionType(final int newValue) {
        value = newValue;
    }

    public int getValue() {
        return value;
    }
    
    public static PhaseActionType fromValue(int value) {
        switch (value) {
        case 0:
            return PhaseActionType.WARNING_NOTIFY;
            
        case 1: 
            return PhaseActionType.WARNING_EMAIL;
            
        case 2: 
            return PhaseActionType.WARNING_SMS;
            
        case 3:
            return PhaseActionType.TERMINATE_NOTIFY;
            
        case 4:
            return PhaseActionType.TERMINATE_EMAIL;
            
        case 5:
            return PhaseActionType.TERMINATE_SMS;
            
        case 6:
            return PhaseActionType.EXTEND_NOTIFY;
            
        case 7:
            return PhaseActionType.EXTEND_EMAIL;

        case 8:
            return PhaseActionType.EXTEND_SMS;
            
        default:
            throw new IllegalArgumentException();
        }
    }
}
