package vn.anvui.dt.enumeration;

import java.util.Arrays;
import java.util.Collection;

public enum ClaimStatus {
    REJECTED(0),
    DONE(1),
    PAPER_REQUIRED(2),
    PROCESSING(3),
    REVISED_REQUIRED(4);
    
    final int value;
    
    public static final Collection<Integer> UPDATE_ALLOWED_STATUS = Arrays.asList(
            ClaimStatus.REJECTED.getValue(), ClaimStatus.PROCESSING.getValue(), ClaimStatus.DONE.getValue());
    
    ClaimStatus(final int newValue)
    {
        value = newValue;
    }

    public int getValue()
    {
        return value;
    }
}
