package vn.anvui.dt.dbconfig;

import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.spi.EvaluationContextExtension;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

import vn.anvui.dt.repos.DefaultRepositoryFactoryBean;

@Configuration
@EnableTransactionManagement
@PropertySource({ "classpath:${SYSTEM_ENVIRONMENT_VARIABLE}_mysql.properties" })
@EnableJpaRepositories(basePackages = DbConst.REPOSITORY_PACKAGE, repositoryFactoryBeanClass = DefaultRepositoryFactoryBean.class)
public class MySqlConfig {
	
	@Value("classpath:/org/springframework/batch/core/schema-drop-mysql.sql")
	private Resource dropSpringBatchSchema;

	@Autowired
	private Environment env;
	
	@Bean
	public EvaluationContextExtension securityExtension() {
		return new SecurityEvaluationContextExtension();
	}
	
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws Exception {
		final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource());
		em.setPackagesToScan(new String[] { DbConst.ENTITY_PACKAGE });

		final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setDatabase(Database.MYSQL);
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(additionalProperties());

		return em;
	}
	
	@Bean(destroyMethod = "close")
	public DataSource dataSource() throws Exception {

		//
		// First we load the underlying JDBC driver.
		// You need this if you don't use the jdbc.drivers
		// system property.
		//
		// Class.forName(env.getProperty("jdbc.driverClassName"));
		//
		// //
		// // Next, we'll create a ConnectionFactory that the
		// // pool will use to create Connections.
		// // We'll use the DriverManagerConnectionFactory,
		// // using the connect string passed in the command line
		// // arguments.
		// //
		// ConnectionFactory connection = new DriverManagerConnectionFactory(
		// env.getProperty("jdbc.url"), env.getProperty("jdbc.user"),
		// env.getProperty("jdbc.pass"));
		//
		// //
		// // Next we'll create the PoolableConnectionFactory, which wraps
		// // the "real" Connections created by the ConnectionFactory with
		// // the classes that implement the pooling functionality.
		// //
		// PoolableConnectionFactory poolableConnection = new
		// PoolableConnectionFactory(
		// connection, null);
		//
		// //
		// // Now we'll need a ObjectPool that serves as the
		// // actual pool of connections.
		// //
		// // We'll use a GenericObjectPool instance, although
		// // any ObjectPool implementation will suffice.
		// //
		// ObjectPool<PoolableConnection> connectionPool = new
		// GenericObjectPool<>(
		// poolableConnection);
		//
		// // Set the factory's pool property to the owning pool
		// poolableConnection.setPool(connectionPool);
		//
		// //
		// // Finally, we create the PoolingDriver itself,
		// // passing in the object pool we created.
		// //
		// PoolingDataSource<PoolableConnection> dataSource = new
		// PoolingDataSource<>(
		// connectionPool);
		//
		// return dataSource;

		// final DriverManagerDataSource dataSource = new
		// DriverManagerDataSource();
		// dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
		// dataSource.setUrl(env.getProperty("jdbc.url"));
		// dataSource.setUsername(env.getProperty("jdbc.user"));
		// dataSource.setPassword(env.getProperty("jdbc.pass"));
		//
		// return dataSource;

		final HikariDataSource dataSource = new HikariDataSource();

		dataSource.setDataSourceClassName(
				env.getProperty("dataSourceClassName", "com.mysql.jdbc.jdbc2.optional.MysqlDataSource"));
		dataSource.setMaximumPoolSize(env.getProperty("pool.size", Integer.class, 20));
		dataSource.setConnectionTestQuery("/* ping */");
		dataSource.setIdleTimeout(env.getProperty("pool.idletime", Integer.class, 600000));
		dataSource.setMaxLifetime(4 * 60 * 60 * 1000);

		dataSource.setDataSourceProperties(new Properties() {

			private static final long serialVersionUID = 1L;

			{
				setProperty("url", env.getProperty("jdbc.url"));
				setProperty("user", env.getProperty("jdbc.user"));
				setProperty("password", env.getProperty("jdbc.pass"));
				setProperty("cachePrepStmts", "true");
				setProperty("prepStmtCacheSize", env.getProperty("dataSource.prepStmtCacheSize", "250"));
				setProperty("prepStmtCacheSqlLimit", env.getProperty("dataSource.prepStmtCacheSize", "2048"));
			}
		});

		return dataSource;
	}

	/**
	 * @author Duong Bui
	 * @return
	 * @throws SQLException
	 */
	@Bean
	public PlatformTransactionManager transactionManager() throws Exception {
		final JpaTransactionManager transactionManager = new JpaTransactionManager();

		return transactionManager;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	@SuppressWarnings("serial")
	private Properties additionalProperties() {

		return new Properties() {
			{
				/**
				 * Disable hbm2ddl of Hibernate, because it seems to run before
				 * Flyway runs the migration
				 */
				setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
				setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
				setProperty("hibernate.globally_quoted_identifiers", "true");

			}
		};

	}

	@Bean
	public DataSourceInitializer dataSourceInitializer(final DataSource dataSource) {
		final DataSourceInitializer initializer = new DataSourceInitializer();
		initializer.setDataSource(dataSource);

		final ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
		populator.addScript(dropSpringBatchSchema);

		initializer.setDatabasePopulator(populator);
		return initializer;
	}
}
