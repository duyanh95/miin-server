package vn.anvui.dt.dbconfig;

public interface DbConst {

	public static final String REPOSITORY_PACKAGE = "vn.anvui.dt.repos";
	public static final String ENTITY_PACKAGE = "vn.anvui.dt.entity";
}
