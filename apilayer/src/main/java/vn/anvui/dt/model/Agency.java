package vn.anvui.dt.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Agency implements Serializable {

	public String companyId;
    public double commission = 0.0;
    
    public Agency(String companyId, double commission)
    {
        this.companyId = companyId;
        this.commission = commission;
    }
    
    public Agency()
    {
        super();
    }

    public String getCompanyId()
    {
        return companyId;
    }
    public void setCompanyId(String companyId)
    {
        this.companyId = companyId;
    }
    public double getCommission()
    {
        return commission;
    }
    public void setCommission(double commission)
    {
        this.commission = commission;
    }
}
