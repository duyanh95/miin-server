package vn.anvui.dt.model;

public class MonthsReport {
    Integer month;
    
    Integer year;
    
    Integer totalContract;
    
    Double totalFee;
    
    Double totalIncome;
    
    public MonthsReport(Integer month, Integer year, Integer totalContract, Double totalFee, Double totalIncome) {
        super();
        this.month = month;
        this.year = year;
        this.totalContract = totalContract;
        this.totalFee = totalFee;
        this.totalIncome = totalIncome;
    }


    public MonthsReport() {
        
    }
    
    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getTotalContract() {
        return totalContract;
    }

    public void setTotalContract(Integer totalContract) {
        this.totalContract = totalContract;
    }

    public Double getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(Double totalFee) {
        this.totalFee = totalFee;
    }

    public Double getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(Double totalIncome) {
        this.totalIncome = totalIncome;
    }
}
