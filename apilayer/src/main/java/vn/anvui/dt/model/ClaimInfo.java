package vn.anvui.dt.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ClaimInfo {
    private Integer id;
    
    @JsonFormat(pattern = "dd/MM/yyyy", timezone = "GMT+7:00")
    Date createdDate;
    
    String customerName;
    
    String contractCode;
    
    String phoneNumber;
    
    Integer claimStatus;
    
    Integer productId;
    
    public ClaimInfo() {
        super();
    }

    public ClaimInfo(Integer id, Date createdDate, String customerName, String contractCode, String phoneNumber,
            Integer claimStatus, Integer productId) {
        super();
        this.setId(id);
        this.createdDate = createdDate;
        this.customerName = customerName;
        this.contractCode = contractCode;
        this.phoneNumber = phoneNumber;
        this.claimStatus = claimStatus;
        this.productId = productId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getClaimStatus() {
        return claimStatus;
    }

    public void setClaimStatus(Integer claimStatus) {
        this.claimStatus = claimStatus;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

}
