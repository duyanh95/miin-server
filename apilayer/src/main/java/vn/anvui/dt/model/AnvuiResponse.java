package vn.anvui.dt.model;

public class AnvuiResponse {
    Integer code;
    
    Integer count;
    
    String status;
    
    String type;
    
    ResultResponse results;
    
    public Integer getCode() {
        return code;
    }


    public void setCode(Integer code) {
        this.code = code;
    }


    public Integer getCount() {
        return count;
    }


    public void setCount(Integer count) {
        this.count = count;
    }


    public String getStatus() {
        return status;
    }


    public void setStatus(String status) {
        this.status = status;
    }


    public String getType() {
        return type;
    }


    public void setType(String type) {
        this.type = type;
    }


    public ResultResponse getResults() {
        return results;
    }


    public void setResults(ResultResponse results) {
        this.results = results;
    }


    public class ResultResponse {
        String redirect;
        Transaction transaction;
        
        public String getRedirect() {
            return redirect;
        }


        public void setRedirect(String redirect) {
            this.redirect = redirect;
        }


        public Transaction getTransaction() {
            return transaction;
        }


        public void setTransaction(Transaction transaction) {
            this.transaction = transaction;
        }


        public class Transaction {
            String id;
            String merchantId;
            Long createdDate;
            String phoneNumber;
            Long amount;
            Integer status;
            String ip;
            String req;
            public String getId() {
                return id;
            }
            public void setId(String id) {
                this.id = id;
            }
            public String getMerchantId() {
                return merchantId;
            }
            public void setMerchantId(String merchantId) {
                this.merchantId = merchantId;
            }
            public Long getCreatedDate() {
                return createdDate;
            }
            public void setCreatedDate(Long createdDate) {
                this.createdDate = createdDate;
            }
            public String getPhoneNumber() {
                return phoneNumber;
            }
            public void setPhoneNumber(String phoneNumber) {
                this.phoneNumber = phoneNumber;
            }
            public Long getAmount() {
                return amount;
            }
            public void setAmount(Long amount) {
                this.amount = amount;
            }
            public Integer getStatus() {
                return status;
            }
            public void setStatus(Integer status) {
                this.status = status;
            }
            public String getIp() {
                return ip;
            }
            public void setIp(String ip) {
                this.ip = ip;
            }
            public String getReq() {
                return req;
            }
            public void setReq(String req) {
                this.req = req;
            }
        }
    }
}
