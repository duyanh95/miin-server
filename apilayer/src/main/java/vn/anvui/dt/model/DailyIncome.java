package vn.anvui.dt.model;

public class DailyIncome {
    Integer userId;
    
    Integer productId;
    
    Double totalIncome;
    
    Integer totalContract;
    
    private Double totalFee;
    
    public DailyIncome() {
        
    }
    
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Double getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(Double totalIncome) {
        this.totalIncome = totalIncome;
    }

    public Integer getTotalContract() {
        return totalContract;
    }

    public void setTotalContract(Integer totalContract) {
        this.totalContract = totalContract;
    }

    public Double getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(Double totalFee) {
        this.totalFee = totalFee;
    }
}
