package vn.anvui.dt.model;

import java.io.Serializable;
import java.util.List;

import vn.anvui.dt.entity.Contract;

@SuppressWarnings("serial")
public class ContractInfo implements Serializable {
    private List<Contract> contractList;
    
    private Long total;
    
    private Double totalIncome;

    public List<Contract> getContractList() {
        return contractList;
    }

    public void setContractList(List<Contract> contractList) {
        this.contractList = contractList;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Double getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(Double totalIncome) {
        this.totalIncome = totalIncome;
    }
    
    
}
