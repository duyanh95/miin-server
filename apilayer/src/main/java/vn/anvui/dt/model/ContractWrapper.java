package vn.anvui.dt.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import vn.anvui.bsn.dto.user.UserInfo;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.entity.InsurancePackage;
import vn.anvui.dt.entity.OnlineTransaction;
import vn.anvui.dt.entity.PackagePeriod;

public class ContractWrapper {
    Contract contract;

    @JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
    InsurancePackage insurancePackage;

    @JsonIgnore
    OnlineTransaction txn;
    
    private PackagePeriod period;

    UserInfo user;

    public ContractWrapper() {

    }

    public ContractWrapper(Integer userId, String userName, Integer contractId, Integer contractStatus,
            Integer packageId, String contractCode, Date createdDate, Date appliedDate, Date startDate, Date endDate,
            Date createdTime, Date updateTime, Integer customerId, String customerName, String customerCitizenId,
            Integer customerGender, Date customerDob, String customerPhoto, String customerEmail,
            String customerPhoneNumber, Integer customerTag, String customerNote, String customerAddress,
            String customerProvince, Integer partnerId, String partnerName, String partnerCitizenId,
            Integer partnerGender, Date partnerDob, String partnerPhoto, String partnerEmail, String partnerPhoneNumber,
            Integer partnerTag, String partnerNote, String partnerAddress, String partnerProvince, Double commission,
            Double fee, Double income, Integer productId) {
        super();
        this.setUser(new UserInfo(userId, userName));

        this.contract = new Contract();
        this.contract.setId(contract.getId());
        this.contract.setContractStatus(contractStatus);
        this.contract.setPackageId(packageId);
        this.contract.setContractCode(contractCode);
        this.contract.setCreatedDate(createdDate);
        this.contract.setAppliedDate(appliedDate);
        this.contract.setStartDate(startDate);
        this.contract.setEndDate(endDate);
        this.contract.setCreatedTime(createdTime);
        this.contract.setUpdatedTime(updateTime);

        this.contract.setInheritCustomer(new Customer(customerId, customerName, customerCitizenId, customerGender,
                customerDob, customerPhoto, customerEmail, customerPhoneNumber, customerTag, customerNote,
                customerAddress, customerProvince));

        this.contract.setPartner(
                new Customer(partnerId, partnerName, partnerCitizenId, partnerGender, partnerDob, partnerPhoto,
                        partnerEmail, partnerPhoneNumber, partnerTag, partnerNote, partnerAddress, partnerProvince));

        this.contract.setCommission(commission);
        this.contract.setFee(fee);
        this.contract.setIncome(income);
        this.contract.setProductId(productId);

    }

    public ContractWrapper(Contract contract) {
        this.contract = contract;
    }

    public ContractWrapper(Contract contract, InsurancePackage insurancePackage, OnlineTransaction txn) {
        super();
        this.contract = contract;
        this.insurancePackage = insurancePackage;
        this.txn = txn;
    }

    public ContractWrapper(Contract contract, OnlineTransaction txn) {
        super();
        this.contract = contract;
        this.txn = txn;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public InsurancePackage getInsurancePackage() {
        return insurancePackage;
    }

    public void setInsurancePackage(InsurancePackage insurancePackage) {
        this.insurancePackage = insurancePackage;
    }

    public OnlineTransaction getTxn() {
        return txn;
    }

    public void setTxn(OnlineTransaction txn) {
        this.txn = txn;
    }

    public UserInfo getUser() {
        return user;
    }

    public void setUser(UserInfo user) {
        this.user = user;
    }

    public PackagePeriod getPeriod() {
        return period;
    }

    public void setPeriod(PackagePeriod period) {
        this.period = period;
    }

}
