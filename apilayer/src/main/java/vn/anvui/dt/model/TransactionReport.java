package vn.anvui.dt.model;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class TransactionReport {
    private String txnType;
    
    private Double amount;
    
    private List<TransactionReportDetail> txnList;
    
    public TransactionReport(String txnType) {
        this.txnType = txnType;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public List<TransactionReportDetail> getTxnList() {
        return txnList;
    }

    public void setTxnList(Collection<TransactionReportDetail> txnList) {
        this.txnList = new LinkedList<>(txnList);
    }
}
