package vn.anvui.dt.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class DailyReport {
    @JsonFormat(pattern = "dd-MM-yyyy", timezone = "GMT+7:00")
    private Date date;
    
    private Long codContracts = 0L;
    
    private Double codIncome = 0.0;
    
    private Long onlineContracts = 0L;
    
    private Double onlineIncome = 0.0;
    
    public DailyReport(Long codContracts, Double codIncome, Long onlineContracts, Double onlineIncome) {
        this.codContracts = codContracts;
        this.codIncome = codIncome;
        this.onlineContracts = onlineContracts;
        this.onlineIncome = onlineIncome;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getCodContracts() {
        return codContracts;
    }

    public void setCodContracts(Long codContracts) {
        this.codContracts = codContracts;
    }

    public Double getCodIncome() {
        return codIncome;
    }

    public void setCodIncome(Double codIncome) {
        this.codIncome = codIncome;
    }

    public Long getOnlineContracts() {
        return onlineContracts;
    }

    public void setOnlineContracts(Long onlineContracts) {
        this.onlineContracts = onlineContracts;
    }

    public Double getOnlineIncome() {
        return onlineIncome;
    }

    public void setOnlineIncome(Double onlineIncome) {
        this.onlineIncome = onlineIncome;
    }

}
