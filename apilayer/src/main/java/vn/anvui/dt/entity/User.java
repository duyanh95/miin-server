package vn.anvui.dt.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import vn.anvui.dt.enumeration.UserStatus;

@SuppressWarnings("serial")
@Entity
@Table(name = "user")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
    
    @Column(name = "user_name", unique = true, nullable = false, length = 255)
    private String userName;

    @Column(name = "agency_code")
    private String agencyCode;
    
    @Column(name = "avatar")
    private String avatar;
    
    @Column(name = "address")
    private String address;
    
    @Column(name = "province")
    private String province;
    
    @Column(name = "status")
    private Integer status = UserStatus.ACTIVE.getValue();
    
    @Column(name = "email")
    private String email;
    
    @Column(name = "phone_number", length = 20)
    private String phoneNumber;
    
    @Column(name = "citizen_id")
    private String citizenId;
    
    @Column(name = "citizen_id_image_front")
    private String citizenIdImageFront;
    
    @Column(name = "citizen_id_image_back")
    private String citizenIdImageBack;
    
    @Column(name = "bank_account")
    private String bankAccount;
    
    @Column(name = "bank_info")
    private String bankInfo;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    private Date createdDate;
    
    @Column(name = "user_type")
    private String userType;
    
    @JsonIgnore
    @Column(name = "password", nullable = true)
    private String password;

    @Column(name = "parent_id")
    private Integer parentId;
    
    @Column(name = "full_name")
    private String fullName;
    
    @Column(name = "commission")
    private Double commission = 0.0;
    
    @Column(name = "allow_cod")
    private Boolean codAllowed = false;
    
    @JsonIgnore
    @Column(name = "otp_code")
    private String otpCode;
    
    @Column(name = "phone_verified")
    private Boolean phoneVerified = false;
    
    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "otp_expired")
    private Date otpExpiredDate;

    @JsonFormat(pattern = "dd/MM/yyyy", timezone = "GMT+7:00")
    @Column(name = "dob")
    private Date dob;
    
    @Column(name = "is_gateway", nullable = false)
    private Boolean isGateway = false;
    
    @Column(name = "google_id")
    private String googleId;
    
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private List<Role> roles;
    
    @Transient List<User> childUsers;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public Boolean isCodAllowed() {
        return codAllowed;
    }

    public void setCodAllowed(Boolean codAllowed) {
        this.codAllowed = codAllowed;
    }

    public List<User> getChildUsers() {
        return childUsers;
    }

    public void setChildUsers(List<User> childUsers) {
        this.childUsers = childUsers;
    }

    public String getBankInfo() {
        return bankInfo;
    }

    public void setBankInfo(String bankInfo) {
        this.bankInfo = bankInfo;
    }

    public String getOtpCode() {
        return otpCode;
    }

    public void setOtpCode(String otpCode) {
        this.otpCode = otpCode;
    }

    public Date getOtpExpiredDate() {
        return otpExpiredDate;
    }

    public void setOtpExpiredDate(Date otpExpiredDate) {
        this.otpExpiredDate = otpExpiredDate;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getCitizenIdImageFront() {
        return citizenIdImageFront;
    }

    public void setCitizenIdImageFront(String citizenIdImageFront) {
        this.citizenIdImageFront = citizenIdImageFront;
    }

    public String getCitizenIdImageBack() {
        return citizenIdImageBack;
    }

    public void setCitizenIdImageBack(String citizenIdImageBack) {
        this.citizenIdImageBack = citizenIdImageBack;
    }

    public Boolean getIsGateway() {
        return isGateway;
    }

    public void setIsGateway(Boolean isGateway) {
        this.isGateway = isGateway;
    }

    public Boolean isPhoneVerified() {
        return phoneVerified;
    }

    public void setPhoneVerified(Boolean phoneVerified) {
        this.phoneVerified = phoneVerified;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }
}
