package vn.anvui.dt.entity.claim;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.data.annotation.ReadOnlyProperty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "claim_docs")
public class ClaimDocs {
    @JsonIgnore
    @EmbeddedId
    DocsKey key;

    @Column(name = "claim_id", updatable = false, insertable = false)
    @ReadOnlyProperty
    Integer claimId;
    
    @Column(name = "product_claim_id", updatable = false, insertable = false)
    @ReadOnlyProperty
    Integer productClaimId;
    
    @Column(name = "resource_url")
    String resourceUrl;
    
    @Column(name = "accepted", nullable = false)
    Boolean accepted;
    
    @Column(name = "error", nullable = false)
    String error;
    
    @Column(name = "solution", nullable = false)
    String solution;
    
    @Column(name = "note", nullable = false)
    String note;
    
    public ClaimDocs() {
        super();
    }
    
    public ClaimDocs(Integer claimId, Integer productClaimId, List<String> resourceUrls) {
        super();
        this.key = new DocsKey(claimId, productClaimId);
        this.claimId = claimId;
        this.setProductClaimId(productClaimId);
        
        this.resourceUrl = resourceUrls.stream().reduce((res1, res2) -> res1 + "," + res2).get();
        this.setAccepted(false);
    }
    
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public DocsKey getKey() {
        return key;
    }

    public void setKey(DocsKey key) {
        this.key = key;
    }

    public Integer getClaimId() {
        return claimId;
    }

    public void setClaimId(Integer claimId) {
        this.claimId = claimId;
    }

    public Integer getProductClaimId() {
        return productClaimId;
    }

    public void setProductClaimId(Integer productClaimId) {
        this.productClaimId = productClaimId;
    }

    public List<String> getResourceUrl() {
        return Arrays.asList(resourceUrl.split(","));
    }
    
    public void setResourceUrl(List<String> urls) {
        this.resourceUrl = urls.stream().reduce((res1, res2) -> res1 + "," + res2).get();
    }

    public Boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }

    @SuppressWarnings("serial")
    @Embeddable
    public static class DocsKey implements Serializable {
        @Column(name = "claim_id")
        Integer claimId;
        
        @Column(name = "product_claim_id")
        Integer productId;
        
        public DocsKey() {
            super();
        }
        
        public DocsKey(Integer claimId, Integer productId) {
            super();
            this.claimId = claimId;
            this.productId = productId;
        }

        public Integer getClaimId() {
            return claimId;
        }

        public void setClaimId(Integer claimId) {
            this.claimId = claimId;
        }

        public Integer getProductId() {
            return productId;
        }

        public void setProductId(Integer productId) {
            this.productId = productId;
        }
    }
}
