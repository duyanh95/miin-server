package vn.anvui.dt.entity;

import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "notification")
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    Integer id;
    
    @Column(name = "notification_type", nullable = false)
    Integer notificationType;
    
    @Column(name = "content", nullable = false)
    String content;
    
    @Column(name = "title", nullable = false)
    String title;
    
    @Column(name = "read")
    Boolean read = false;
    
    @Column(name = "user_id")
    Integer userId;
    
    @Column(name = "created_date")
    Date createdDate = new Date();
    
    @Column(name = "data")
    String data;
    
    @Transient
    Map<String, String> dataMap;
    
    public Notification() {
        
    }

    public Notification(Integer notificationType, String content, String title, String data, Integer userId) {
        super();
        this.notificationType = notificationType;
        this.content = content;
        this.title = title;
        this.data = data;
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(Integer notificationType) {
        this.notificationType = notificationType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Map<String, String> getDataMap() {
        return dataMap;
    }

    public void setDataMap(Map<String, String> dataMap) {
        this.dataMap = dataMap;
    }
}
