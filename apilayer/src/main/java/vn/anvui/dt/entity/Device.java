package vn.anvui.dt.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import vn.anvui.dt.enumeration.DeviceType;

@Entity
@Table(name = "device")
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private
    Integer id;
    
    @Column(name = "user_id", unique = true, nullable = false)
    Integer userId;
    
    @Column(name = "token", nullable = false)
    String token;
    
    @Column(name = "type")
    Integer type;
    
    public Device() {
        
    }

    public Device(String token, Integer userId, DeviceType type) {
        this.token = token;
        this.userId = userId;
        this.type = type.getValue();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
