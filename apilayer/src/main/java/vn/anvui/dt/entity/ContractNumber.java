package vn.anvui.dt.entity;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import vn.anvui.dt.model.IncomeUser;

@SqlResultSetMapping(name = "IncomeUser",
classes = {
        @ConstructorResult(targetClass = IncomeUser.class,
                columns = {
                        @ColumnResult(name = "onlineContracts", type = Long.class),
                        @ColumnResult(name = "onlineIncome", type = Double.class),
                        @ColumnResult(name = "codContracts", type = Long.class),
                        @ColumnResult(name = "codIncome", type = Double.class),
                        @ColumnResult(name = "totalCommission", type = Double.class)
        })
})
@Entity
@Table(name = "contract_number")
public class ContractNumber {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "number", unique = true, nullable = false)
    private Integer number;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
    
}
