package vn.anvui.dt.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import vn.anvui.dt.enumeration.ContactLogType;

@Entity
@Table(name = "contact_log")
public class ContactLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
    
    @Column(name = "customer_id", nullable = false)
    private Integer customerId;
    
    @Column(name = "phone_number", nullable = false, length = 11)
    private String phoneNumber;
    
    @Column(name = "email", nullable = false)
    private String email;
    
    @Column(name = "customer_name")
    private String customerName;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "customer_dob")
    private Date customerDob;
    
    @Column(name = "customer_citizen_id")
    private String customerCitizenId;
    
    @JsonIgnore
    @Column(name = "partner_id")
    private Integer partnerId;
    
    @JsonIgnore
    @Column(name = "contract_id")
    private Integer contractId;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_time")
    private Date createdTime;
    
    @Column(name = "contact_log_type")
    private Byte contractLogType;
    
    @Column(name = "updated_user_id")
    private Integer updatedUserId;
    
    public ContactLog() {
        
    }
    
    public ContactLog(Customer customer, Customer partner, Contract contract, ContactLogType type, User user) {
        this.customerId = customer.getId();
        this.phoneNumber = customer.getPhoneNumber();
        this.email = customer.getEmail();
        this.customerName = customer.getCustomerName();
        this.customerDob = customer.getDob();
        this.customerCitizenId = customer.getCitizenId();
        
        if (partner != null)
            this.partnerId = partner.getId();
        
        if (contract != null)
            this.contractId = contract.getId();
        
        if (user != null)
            this.updatedUserId = user.getId();
        
        this.contractLogType = (byte) type.getValue();
        this.createdTime = new Date();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Date getCustomerDob() {
        return customerDob;
    }

    public void setCustomerDob(Date customerDob) {
        this.customerDob = customerDob;
    }

    public String getCustomerCitizenId() {
        return customerCitizenId;
    }

    public void setCustomerCitizenId(String customerCitizenId) {
        this.customerCitizenId = customerCitizenId;
    }

    public Integer getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Integer partnerId) {
        this.partnerId = partnerId;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Byte getContractLogType() {
        return contractLogType;
    }

    public void setContractLogType(Byte contractLogType) {
        this.contractLogType = contractLogType;
    }

    public Integer getUpdatedUserId() {
        return updatedUserId;
    }

    public void setUpdatedUserId(Integer updatedUserId) {
        this.updatedUserId = updatedUserId;
    }
}
