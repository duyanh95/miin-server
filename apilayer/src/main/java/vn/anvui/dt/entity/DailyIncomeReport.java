package vn.anvui.dt.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import org.springframework.data.annotation.ReadOnlyProperty;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import vn.anvui.dt.model.MonthsReport;

@SqlResultSetMapping(name = "MonthsReport", classes = { 
    @ConstructorResult(
            targetClass = MonthsReport.class, columns = {
        @ColumnResult(name = "month", type = Integer.class), 
        @ColumnResult(name = "year", type = Integer.class),
        @ColumnResult(name = "totalContracts", type = Integer.class),
        @ColumnResult(name = "totalFee", type = Double.class),
        @ColumnResult(name = "totalIncome", type = Double.class), 
    }) 
})
@Entity
@Table(name = "daily_report")
public class DailyIncomeReport {

    @JsonIgnore
    @EmbeddedId
    ReportKey key;

    @Column(name = "day", nullable = false)
    Integer day;

    @Column(name = "month", nullable = false)
    Integer month;

    @Column(name = "year", nullable = false)
    Integer year;

    @Column(name = "date", nullable = false, insertable = false, updatable = false)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+7:00")
    @ReadOnlyProperty
    Date date;

    @Column(name = "user_id", nullable = false, insertable = false, updatable = false)
    @ReadOnlyProperty
    Integer userId;

    @Column(name = "product_id", nullable = false, insertable = false, updatable = false)
    @ReadOnlyProperty
    Integer productId;

    @Column(name = "total_contracts", nullable = false)
    Integer totalContract;

    @Column(name = "total_income", nullable = false)
    Double totalIncome;

    @Column(name = "total_fee", nullable = false)
    Double totalFee;

    @Column(name = "last_update", nullable = false)
    Date lastUpdate = new Date();

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Date getDate() {
        return this.key.date;
    }

    public void setDate(Date date) {
        if (this.key == null)
            this.key = new ReportKey();

        this.key.date = date;
    }

    public Integer getUserId() {
        return this.key.userId;
    }

    public void setUserId(Integer userId) {
        if (this.key == null)
            this.key = new ReportKey();

        this.key.userId = userId;
    }

    public Integer getProductId() {
        return this.key.productId;
    }

    public void setProductId(Integer productId) {
        if (this.key == null)
            this.key = new ReportKey();

        this.key.productId = productId;
    }

    public Integer getTotalContract() {
        return totalContract;
    }

    public void setTotalContract(Integer totalContract) {
        this.totalContract = totalContract;
    }

    public Double getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(Double totalIncome) {
        this.totalIncome = totalIncome;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Double getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(Double totalFee) {
        this.totalFee = totalFee;
    }

    @SuppressWarnings("serial")
    @Embeddable
    public static class ReportKey implements Serializable {
        @Column(name = "user_id")
        Integer userId;

        @Column(name = "product_id")
        Integer productId;

        @Column(name = "date", nullable = false)
        Date date;

        public ReportKey() {

        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getProductId() {
            return productId;
        }

        public void setProductId(Integer productId) {
            this.productId = productId;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }
    }

    public ReportKey getKey() {
        return key;
    }

    public void setKey(ReportKey key) {
        this.key = key;
    }
}
