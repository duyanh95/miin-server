package vn.anvui.dt.entity.claim;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.model.ClaimInfo;

@Entity
@Table(name = "claim")
@SqlResultSetMappings({
    @SqlResultSetMapping(name = "ClaimInfo", classes = {
        @ConstructorResult(targetClass = ClaimInfo.class,
                columns = {
                        @ColumnResult(name = "id", type = Integer.class),
                        @ColumnResult(name = "createdTime", type = Date.class),
                        @ColumnResult(name = "customerName", type = String.class),
                        @ColumnResult(name = "contractCode", type = String.class),
                        @ColumnResult(name = "phoneNumber", type = String.class),
                        @ColumnResult(name = "claimStatus", type = Integer.class),
                        @ColumnResult(name = "productId", type = Integer.class)
        })
    })
})
public class Claim {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;
    
    @Column(name = "contract_id", nullable = false)
    Integer contractId;
    
    @Column(name = "user_id")
    Integer userId;
    
    @Column(name = "claim_status", nullable = false)
    Integer claimStatus;
    
    @Column(name = "compensation_method")
    Integer composationMethod;
    
    @Column(name = "compensation_note")
    String compensationNote;
    
    @Column(name = "product_id")
    Integer productId;
    
    @JsonFormat(pattern = "HH:mm:ss dd/MM/yyy", timezone = "GMT+7:00")
    @Column(name = "created_time", nullable = false)
    Date createdTime = new Date();
    
    @JsonFormat(pattern = "HH:mm:ss dd/MM/yyy", timezone = "GMT+7:00")
    @Column(name = "update_time")
    Date updateTime;
    
    @JsonFormat(pattern = "dd/MM/yyy", timezone = "GMT+7:00")
    @Column(name = "from_date")
    Date fromDate;
    
    @JsonFormat(pattern = "dd/MM/yyy", timezone = "GMT+7:00")
    @Column(name = "to_date")
    Date toDate;
    
    @Column(name = "claim_type")
    Integer claimType;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "claimId")
    List<ClaimDocs> documents;
    
    @Transient
    Contract contract;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public Integer getClaimStatus() {
        return claimStatus;
    }

    public void setClaimStatus(Integer claimStatus) {
        this.claimStatus = claimStatus;
    }

    public Integer getComposationMethod() {
        return composationMethod;
    }

    public void setComposationMethod(Integer composationMethod) {
        this.composationMethod = composationMethod;
    }

    public String getCompensationNote() {
        return compensationNote;
    }

    public void setCompensationNote(String compensationNote) {
        this.compensationNote = compensationNote;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public List<ClaimDocs> getDocuments() {
        return documents;
    }

    public void setDocuments(List<ClaimDocs> documents) {
        this.documents = documents;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getClaimType() {
        return claimType;
    }

    public void setClaimType(Integer claimType) {
        this.claimType = claimType;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }
}
