package vn.anvui.dt.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import vn.anvui.dt.enumeration.BannerType;

@Entity
@Table(name="banner")
public class Banner {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
    
    @Column(name = "image")
    private String image;
    
    @Column(name = "redirect_url")
    private String redirectURL;
    
    @Column(name = "active")
    private boolean active = true;
    
    @Column(name = "created_time")
    private Date createdTime = new Date();
    
    @Column(name = "description")
    private String description;
    
    @JsonIgnore
    @Column(name = "banner_type")
    private Integer bannerType = BannerType.BANNER.getValue();

    public Banner() {
        super();
    }

    public Banner(String image, String redirectURL, String description, BannerType type) {
        super();
        this.image = image;
        this.redirectURL = redirectURL;
        this.active = true;
        this.createdTime = new Date();
        this.description = description;
        
        if (type != null) {
            this.bannerType = type.getValue();
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRedirectURL() {
        return redirectURL;
    }

    public void setRedirectURL(String redirectURL) {
        this.redirectURL = redirectURL;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getBannerType() {
        return bannerType;
    }

    public void setBannerType(Integer bannerType) {
        this.bannerType = bannerType;
    }
}
