package vn.anvui.dt.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@SuppressWarnings("serial")
@Entity
@Table(name = "package")
public class InsurancePackage implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
    
    @Column(name = "package_name", nullable = false, unique = true)
    private String packageName;
    
    @Column(name = "fee", nullable = false)
    private Double fee;
    
    @Column(name = "insurance", nullable = false)
    private Double insurance;
    
    @Column(name = "description", length = 500)
    private String description;
    
    @Column(name = "active", nullable = false)
    private boolean active = true;
    
    @Column(name = "product_id")
    private Integer productId;
    
    @Column(name = "image")
    private String image;
    
    @Column(name = "additional_info")
    private String additionalInfo;
    
    @Column(name = "has_period_option")
    private Boolean hasPeriod;
    
    @Transient
    private List<PackagePeriod> period;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public Double getInsurance() {
        return insurance;
    }

    public void setInsurance(Double insurance) {
        this.insurance = insurance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public Boolean getHasPeriod() {
        return hasPeriod;
    }

    public void setHasPeriod(Boolean hasPeriod) {
        this.hasPeriod = hasPeriod;
    }

    public List<PackagePeriod> getPeriod() {
        return period;
    }

    public void setPeriod(List<PackagePeriod> period) {
        this.period = period;
    }

}
