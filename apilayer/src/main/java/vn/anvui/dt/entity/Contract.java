package vn.anvui.dt.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import vn.anvui.bsn.dto.user.UserInfo;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.model.DailyReport;

@SqlResultSetMappings({
    @SqlResultSetMapping(name = "DailyReport", classes = {
        @ConstructorResult(targetClass = DailyReport.class,
                columns = {
                        @ColumnResult(name = "codContracts", type = Long.class),
                        @ColumnResult(name = "codIncome", type = Double.class),
                        @ColumnResult(name = "onlineContracts", type = Long.class),
                        @ColumnResult(name = "onlineIncome", type = Double.class),
        })
    }),
    @SqlResultSetMapping(name = "ContractDTO", classes = {
        @ConstructorResult(targetClass = ContractWrapper.class,
        columns = {
                @ColumnResult(name = "uid", type = Integer.class),
                @ColumnResult(name = "user_name", type = String.class),
                @ColumnResult(name = "id", type = Integer.class),
                @ColumnResult(name = "contract_status", type = Integer.class),
                @ColumnResult(name = "package_id", type = Integer.class),
                @ColumnResult(name = "contract_code", type = String.class),
                @ColumnResult(name = "created_date", type = Date.class),
                @ColumnResult(name = "applied_date", type = Date.class),
                @ColumnResult(name = "start_date", type = Date.class),
                @ColumnResult(name = "end_date", type = Date.class),
                @ColumnResult(name = "created_time", type = Date.class),
                @ColumnResult(name = "updated_time", type = Date.class),
                @ColumnResult(name = "cus_id", type = Integer.class),
                @ColumnResult(name = "cus_name", type = String.class),
                @ColumnResult(name = "cus_cid", type = String.class),
                @ColumnResult(name = "cus_gender", type = Integer.class),
                @ColumnResult(name = "cus_dob", type = Date.class),
                @ColumnResult(name = "cus_photo", type = String.class),
                @ColumnResult(name = "cus_email", type = String.class),
                @ColumnResult(name = "cus_phone", type = String.class),
                @ColumnResult(name = "cus_tag", type = Integer.class),
                @ColumnResult(name = "cus_note", type = String.class),
                @ColumnResult(name = "cus_addr", type = String.class),
                @ColumnResult(name = "cus_province", type = String.class),
                @ColumnResult(name = "pa_id", type = Integer.class),
                @ColumnResult(name = "pa_name", type = String.class),
                @ColumnResult(name = "pa_cid", type = String.class),
                @ColumnResult(name = "pa_gender", type = Integer.class),
                @ColumnResult(name = "pa_dob", type = Date.class),
                @ColumnResult(name = "pa_photo", type = String.class),
                @ColumnResult(name = "pa_email", type = String.class),
                @ColumnResult(name = "pa_phone", type = String.class),
                @ColumnResult(name = "pa_tag", type = Integer.class),
                @ColumnResult(name = "pa_note", type = String.class),
                @ColumnResult(name = "pa_addr", type = String.class),
                @ColumnResult(name = "pa_province", type = String.class),
                @ColumnResult(name = "commission", type = Double.class),
                @ColumnResult(name = "fee", type = Double.class),
                @ColumnResult(name = "income", type = Double.class),
                @ColumnResult(name = "product_id", type = Integer.class)
        })
    })
})
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
@Entity
@Table(name = "contract")
public class Contract implements Cloneable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
    
    @Column(name = "contract_status", nullable = false)
    private Integer contractStatus;
    
    @Column(name = "package_id", nullable = false)
    private Integer packageId;
    
    @Column(name = "contract_code", nullable = false)
    private String contractCode;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date", nullable = false)
    private Date createdDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "applied_date", nullable = false)
    private Date appliedDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_date", nullable = false)
    private Date startDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_date", nullable = false)
    private Date endDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "payment_date")
    private Date paymentDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_time")
    private Date createdTime;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_time")
    private Date updatedTime = null;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "inherit_customer_id", nullable = false)
    private Customer inheritCustomer;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "partner_id", nullable = false)
    private Customer partner;
    
    @Column(name = "created_user_id")
    private Integer createdUserId;
    
    @Column(name = "confirm", nullable = false)
    private Boolean confirm = false;
    
    @Column(name = "export_status", nullable = false)
    private byte exportStatus = (byte) 0;
    
    @Column(name = "commission")
    private Double commission = 0.0;
    
    @Column(name = "fee")
    private Double fee = 0.0;
    
    @Column(name = "income")
    private Double income = 0.0;
    
    @Column(name = "product_id")
    private Integer productId;
    
    @Column(name = "buyer_name")
    private String buyerName;
    
    @Column(name = "buyer_phone")
    private String buyerPhone;
    
    @Column(name = "period_type")
    private Integer periodType;
    
    @Column(name = "phase")
    private Integer phase;
    
    @Column(name = "pti_id")
    private Integer ptiId;
    
    @Transient
    private Object info;
    
    @Transient
    private UserInfo createdUser;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(Integer contractStatus) {
        this.contractStatus = contractStatus;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getAppliedDate() {
        return appliedDate;
    }

    public void setAppliedDate(Date appliedDate) {
        this.appliedDate = appliedDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Customer getInheritCustomer() {
        return inheritCustomer;
    }

    public void setInheritCustomer(Customer inheritCustomer) {
        this.inheritCustomer = inheritCustomer;
    }

    public Customer getPartner() {
        return partner;
    }

    public void setPartner(Customer partner) {
        this.partner = partner;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Boolean getConfirm() {
        return confirm;
    }

    public void setConfirm(Boolean confirm) {
        this.confirm = confirm;
    }

    public byte getExportStatus() {
        return exportStatus;
    }

    public void setExportStatus(byte exportStatus) {
        this.exportStatus = exportStatus;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public Double getIncome() {
        return income;
    }

    public void setIncome(Double income) {
        this.income = income;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Object getInfo() {
        return info;
    }

    public void setInfo(Object info) {
        this.info = info;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getBuyerPhone() {
        return buyerPhone;
    }

    public void setBuyerPhone(String buyerPhone) {
        this.buyerPhone = buyerPhone;
    }

    public UserInfo getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(UserInfo createdUser) {
        this.createdUser = createdUser;
    }

    public Integer getPeriodType() {
        return periodType;
    }

    public void setPeriodType(Integer periodType) {
        this.periodType = periodType;
    }

    public Integer getPhase() {
        return phase;
    }

    public void setPhase(Integer phase) {
        this.phase = phase;
    }

    public Integer getPtiId() {
        return ptiId;
    }

    public void setPtiId(Integer ptiId) {
        this.ptiId = ptiId;
    }
}
