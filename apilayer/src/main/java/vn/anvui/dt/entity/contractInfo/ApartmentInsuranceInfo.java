package vn.anvui.dt.entity.contractInfo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import vn.anvui.dt.entity.Contract;

@Entity
@Table(name = "apartment_insurance_info")
public class ApartmentInsuranceInfo extends AbstractContractInfo {
    @Column(name = "apartment_value")
    Double apartmentValue;
    
    @Column(name = "apartment_address")
    String address;
    
    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn(name = "contract_id", referencedColumnName = "id")
    private
    Contract contract;

    public Double getApartmentValue() {
        return apartmentValue;
    }

    public void setApartmentValue(Double apartmentValue) {
        this.apartmentValue = apartmentValue;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
        this.id = contract.getId();
    }
    
    
}
