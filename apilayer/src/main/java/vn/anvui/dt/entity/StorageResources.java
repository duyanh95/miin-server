package vn.anvui.dt.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "resources")
public class StorageResources {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;
    
    @Column(name = "resource_url", nullable = false)
    String resourceUrl;
    
    @Column(name = "user_id", nullable = false)
    Integer userId;
    
    @Column(name = "created_time", nullable = false)
    Date createdTime = new Date();
    
    @Column(name = "storage_url", nullable = false)
    String storageUrl;
    
    public StorageResources() {
        super();
    }
    
    public StorageResources(String resourceUrl, Integer userId, String storageUrl) {
        super();
        this.resourceUrl = resourceUrl;
        this.userId = userId;
        this.storageUrl = storageUrl;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public String getStorageUrl() {
        return storageUrl;
    }

    public void setStorageUrl(String storageUrl) {
        this.storageUrl = storageUrl;
    }
    
}
