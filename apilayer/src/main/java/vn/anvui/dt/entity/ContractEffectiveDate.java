package vn.anvui.dt.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "contract_effective_date")
public class ContractEffectiveDate {
    
    @JsonIgnore
    @EmbeddedId
    EffectiveDateKey key;
    
    @Column(name = "contract_id", insertable = false, updatable = false)
    Integer contractId;
    
    @Column(name = "start_date", insertable = false, updatable = false)
    Date startDate;
    
    @Column(name = "end_date", insertable = false, updatable = false)
    Date endDate;

    public ContractEffectiveDate() {
        super();
    }
    
    public ContractEffectiveDate(Contract contract) {
        super();
        this.key = new EffectiveDateKey(contract.getId(), contract.getStartDate(), contract.getEndDate());
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
    @SuppressWarnings("serial")
    @Embeddable
    public static class EffectiveDateKey implements Serializable {
        @Column(name = "contract_id")
        Integer contractId;
        
        @Column(name = "start_date")
        Date startDate;
        
        @Column(name = "end_date")
        Date endDate;
        
        public EffectiveDateKey() {
            super();
        }

        public EffectiveDateKey(Integer contractId, Date startDate, Date endDate) {
            super();
            this.contractId = contractId;
            this.startDate = startDate;
            this.endDate = endDate;
        }

        public Integer getContractId() {
            return contractId;
        }

        public void setContractId(Integer contractId) {
            this.contractId = contractId;
        }

        public Date getStartDate() {
            return startDate;
        }

        public void setStartDate(Date startDate) {
            this.startDate = startDate;
        }

        public Date getEndDate() {
            return endDate;
        }

        public void setEndDate(Date endDate) {
            this.endDate = endDate;
        }
    }
}
