package vn.anvui.dt.entity.claim;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "claim_type")
public class ClaimType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    Integer id;
    
    @Column(name = "name")
    String name;
    
    @Column(name = "product_id")
    private
    Integer productId;
    
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "claim_type_requirement", 
            joinColumns = @JoinColumn(name = "claim_type_id", referencedColumnName = "id"), 
            inverseJoinColumns = @JoinColumn(name = "product_claim_requirement_id", referencedColumnName = "id"))
    List<ProductClaimRequirement> requirements;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProductClaimRequirement> getRequirements() {
        return requirements;
    }

    public void setRequirements(List<ProductClaimRequirement> requirements) {
        this.requirements = requirements;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }
}
