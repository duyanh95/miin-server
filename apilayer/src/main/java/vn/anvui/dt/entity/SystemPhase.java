package vn.anvui.dt.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "system_phase")
public class SystemPhase {
    @Id
    @Column(name = "id", nullable = false)
    Integer id;
    
    @Column(name = "from_end_date", nullable = false)
    Integer fromEndDate;
    
    @Column(name = "action_types", nullable = false)
    private
    String actionTypes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFromEndDate() {
        return fromEndDate;
    }

    public void setFromEndDate(Integer fromEndDate) {
        this.fromEndDate = fromEndDate;
    }

    public String getActionTypes() {
        return actionTypes;
    }

    public void setActionTypes(String actionTypes) {
        this.actionTypes = actionTypes;
    }
}
