package vn.anvui.dt.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
@Table(name = "access_token")
public class AccessToken implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "token", unique = true, nullable = false)
    private String token; // access token

    @Column(name = "platform_type")
    private Integer platformType;

    @Column(name = "platform_version", length = 250)
    private String platformVersion;

    @Column(name = "device_token")
    private String deviceToken;

    @Column(name = "device_id")
    private String deviceId;
    
    @Column(name = "has_privilege")
    private Boolean hasPrivilege;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "expired_date", nullable = false, length = 19)
    private Date expiredDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date", nullable = false, length = 19)
    private Date createdDate;
    
    public AccessToken() {
        
    }

    public AccessToken(User user, String token, Integer platformType, String platformVersion, String deviceToken,
            String deviceId, Date expiredDate) {
        this(user, token, platformType, platformVersion, deviceToken, deviceId, expiredDate, true);
    }

    public AccessToken(User user, String token, Integer platformType, String platformVersion, String deviceToken,
            String deviceId, Date expiredDate, Boolean hasPrivilege) {
        this.user = user;
        this.token = token;
        this.platformType = platformType;
        this.platformVersion = platformVersion;
        this.deviceToken = deviceToken;
        this.deviceId = deviceId;
        this.expiredDate = expiredDate;
        this.hasPrivilege = hasPrivilege;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getPlatformType() {
        return platformType;
    }

    public void setPlatformType(Integer platformType) {
        this.platformType = platformType;
    }

    public String getPlatformVersion() {
        return platformVersion;
    }

    public void setPlatformVersion(String platformVersion) {
        this.platformVersion = platformVersion;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Boolean getHasPrivilege() {
        return hasPrivilege;
    }

    public void setHasPrivilege(Boolean hasPrivilege) {
        this.hasPrivilege = hasPrivilege;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
