package vn.anvui.dt.common;

public interface ApiConts {
	
	public static final String REST_CONTROLLER_PACKAGE = "vn.anvui.api.rest";

	public static final String BUSINESS_BEAN_PACKAGE = "vn.anvui.bsn.beans";
	
	public static final String REPO_BEAN_PACKAGE = "vn.anvui.dt.repos";
	
	public static final String SPRING_SECURITY_PACKAGE = "vn.anvui.api.security";
	
	public static final String BUSINESS_AUTH_PACKAGE = "vn.anvui.bsn.auth";
	
	public static final String ENTITY_PACKAGE = "vn.anvui.dt.entiy";
	
	public static final String LOGIN_URL = "/login";
	
	public static final String GOOGLE_SIGNIN = "/gg-signin";
	
	public static final String LOGOUT_URL = "/logout";
	
	public static final String PUBLIC_URL = "/public";
	
	public static final String USER_REGISTER_URL = "/registration/user";
	
	public static final String CUSTOMER_URL = "/customers/**";
}
