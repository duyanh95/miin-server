package vn.anvui.dt.common;

import java.util.Random;

public class PincodeHelper {

	private static final int MAX_VALUE_OF_PINCODE = 999999;
	private static final int MIN_VALUE_OF_PINCODE = 100000;

	public static String getRandomPincode() {
		Random rand = new Random();
		
		int randomNum = rand
				.nextInt((MAX_VALUE_OF_PINCODE - MIN_VALUE_OF_PINCODE) + 1)
				+ MIN_VALUE_OF_PINCODE;

		return String.valueOf(randomNum);
	}
}
