package vn.anvui.dt.common;

import java.io.Serializable;
import java.util.Date;

import vn.anvui.dt.entity.AccessToken;
import vn.anvui.dt.entity.User;

public class AuthenticationDetails implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private AccessToken accessToken;
	
	private User user;
	
	private Boolean queueCreated;
	
	public AuthenticationDetails(AccessToken accessToken, User user) {
		this.accessToken = accessToken;
		this.user = user;
	}
	
	public AccessToken getAccessToken() {
		return this.accessToken;
	}
	
	public User getAuthenticatedUser() {
		return this.user;
	}
	
	public Date getExpiredDate() {
		return accessToken.getExpiredDate();
	}
	
	public void setExpiredDate(Date expiredDate) {
		accessToken.setExpiredDate(expiredDate);
	}

	public Boolean getQueueCreated() {
		return queueCreated;
	}

	public void setQueueCreated(Boolean queueCreated) {
		this.queueCreated = queueCreated;
	}
	
	public User getUser() {
	    return this.user;
	}
}
