package vn.anvui.dt.common;

public class StaticValue {
	
	public static final String ANVUI_COMPANY_ID = "TC1694378718189000";
	public static final String DEFAULT_COMPANY_NAME = "Anvui.vn";
	public static final String APP_URL = "";
    
    public static final int TOKEN_EXPIRATION_IN_SECONDS = 30 * 24 * 60 * 60;
}
