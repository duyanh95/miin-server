package vn.anvui.dt.repos.claim;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.claim.ProductClaimRequirement;

public interface ProductClaimRequirementRepository extends CrudRepository<ProductClaimRequirement, Integer>,
        JpaRepository<ProductClaimRequirement, Integer> {
}
