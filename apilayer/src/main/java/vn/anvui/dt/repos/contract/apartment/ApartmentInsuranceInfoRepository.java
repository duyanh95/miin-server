package vn.anvui.dt.repos.contract.apartment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.contractInfo.ApartmentInsuranceInfo;

public interface ApartmentInsuranceInfoRepository extends JpaRepository<ApartmentInsuranceInfo, Integer>,
        CrudRepository<ApartmentInsuranceInfo, Integer> {

}
