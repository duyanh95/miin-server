package vn.anvui.dt.repos.dailyReport;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import vn.anvui.dt.entity.DailyIncomeReport;
import vn.anvui.dt.model.IncomeUser;
import vn.anvui.dt.model.MonthsReport;

public interface DailyIncomeReportRepositoryCustom {
    Collection<DailyIncomeReport> listDailyReport(Date date) throws Exception;

    List<MonthsReport> getMonthReport(List<Date> months, Collection<Integer> userIds, Integer productIds);
    
    List<IncomeUser> getSubAgencyReport(Date fromDate, Date toDate, List<Integer> userIds, Integer productId);
    
    DailyIncomeReport getReportByDay(Date date, Collection<Integer> userIds , Integer productId);
}
