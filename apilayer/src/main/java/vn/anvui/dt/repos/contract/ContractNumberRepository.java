package vn.anvui.dt.repos.contract;

import org.springframework.data.jpa.repository.JpaRepository;

import vn.anvui.dt.entity.ContractNumber;

public interface ContractNumberRepository extends JpaRepository<ContractNumber, Integer> {

}
