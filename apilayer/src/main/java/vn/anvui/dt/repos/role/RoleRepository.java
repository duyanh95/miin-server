package vn.anvui.dt.repos.role;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import vn.anvui.dt.entity.Privilege;
import vn.anvui.dt.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Integer>, 
        CrudRepository<Role, Integer> {
    List<Role> findAllByNameIn(List<String> roles);
    
    @Query("SELECT r.privileges FROM Role r WHERE r.id = :id")
    List<Privilege> getPrivilegesById(@Param("id") Integer id);
}
