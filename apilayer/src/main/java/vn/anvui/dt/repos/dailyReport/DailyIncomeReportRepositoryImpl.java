package vn.anvui.dt.repos.dailyReport;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import vn.anvui.dt.entity.DailyIncomeReport;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.model.IncomeUser;
import vn.anvui.dt.model.MonthsReport;
import vn.anvui.dt.utils.DateTimeHelper;

public class DailyIncomeReportRepositoryImpl implements DailyIncomeReportRepositoryCustom {
    final static Logger log = Logger.getLogger(DailyIncomeReportRepositoryImpl.class.getSimpleName());
    
    @PersistenceContext
    EntityManager em;
    
    @Override
    public Collection<DailyIncomeReport> listDailyReport(Date date) throws Exception {
        String sql = "select \n" + 
                "    c.product_id as productId, c.created_user_id as userId, \n" + 
                "    day(:date) as day, month(:date) as month, year(:date) as year, date(:date) as date, \n" + 
                "    count(1) as totalContracts, sum(c.fee) as totalFee, sum(c.income) as totalIncome\n" + 
                "from contract c\n" + 
                "where c.created_time between :date and :toDate and c.contract_status in :status\n" +
                "group by product_id, created_user_id";
        
        Date toDate = new Date(date.getTime() + DateTimeHelper.DAY_IN_MS);
        
        List<Integer> status = new ArrayList<>(Arrays.asList(
                ContractStatus.AGENCY_PAID.getValue(), ContractStatus.AGENCY_COD_DONE.getValue(),
                ContractStatus.AGENCY_TRANSFER_DONE.getValue()));
        
        Query query = em.createNativeQuery(sql);
        
        query.setParameter("date", date);
        query.setParameter("toDate", toDate);
        query.setParameter("status", status);
                
        return mapResultIntoEntity(query.getResultList());
    }
    
    protected Collection<DailyIncomeReport> mapResultIntoEntity(List<Object[]> rsList) {
        List<DailyIncomeReport> reportList = new LinkedList<>();
        
        for (Object[] ele : rsList) {
            DailyIncomeReport report = new DailyIncomeReport();
            
            report.setProductId((Integer) ele[0]);
            report.setUserId((Integer) ele[1]);
            report.setDay(((BigInteger) ele[2]).intValue());
            report.setMonth(((BigInteger) ele[3]).intValue());
            report.setYear(((BigInteger) ele[4]).intValue());
            report.setDate((Date) ele[5]);
            report.setTotalContract(((BigInteger) ele[6]).intValue());
            report.setTotalFee((Double) ele[7]);
            report.setTotalIncome(((Double) ele[8]));
            
            reportList.add(report);
        }
        
        return reportList;
    }
    
    @Override
    public List<MonthsReport> getMonthReport(List<Date> months, Collection<Integer> userIds, 
            Integer productId) {
        String sql = "select mon.month, mon.year, coalesce(sum(total_contracts), 0) as totalContracts,\n" + 
                "coalesce(sum(total_fee), 0) as totalFee, coalesce(sum(total_income), 0) as totalIncome\n" +
                "from daily_report r\n" + 
                "right join \n" + 
                "(%s) as mon\n" +
                "    on mon.month = r.month and mon.year = r.year\n" + 
                "    and user_id in :userIds\n" +
                "    %s\n" +
                "group by mon.year, mon.month";
        
        StringBuilder monthTable = new StringBuilder();
        for (int i = 0; i < months.size(); i++) {
            Date month = months.get(i);
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+7:00"));
            cal.setTime(month);
            monthTable.append(String.format(" select %d as month, %d as year\n",
                    cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR)));
            
            if (i != months.size() - 1) {
                monthTable.append("union all");
            }
        }
        
        String productsSql = "";
        if (productId != null) {
            productsSql = "and product_id = :productId";
        }
        
        sql = String.format(sql, monthTable.toString(), productsSql);
        
        log.info(sql);
        
        Query query = em.createNativeQuery(sql, "MonthsReport");
        
        query.setParameter("userIds", userIds);
        
        if (productId != null) {
            query.setParameter("productId", productId);
        }
        
        return query.getResultList();
    }

    @Override
    public List<IncomeUser> getSubAgencyReport(Date fromDate, Date toDate, 
            List<Integer> userIds, Integer productId) {
        String sql = "select u.id, u.user_name, u.full_name, u.user_type, coalesce(sum(r.total_contracts), 0),\n" + 
                "    coalesce(sum(r.total_fee), 0), coalesce(sum(r.total_income), 0) as totalIncome from user u \n" + 
                "left join daily_report r on r.user_id = u.id and r.date between :fromDate and :toDate\n" + 
                "%s\n" +
                "where u.id in :userIds group by u.id order by totalIncome desc";
        
        
        String querySql = "";
        if (productId != null) {
            querySql = "and product_id = :productId";
        }
        
        sql = String.format(sql, querySql);
        Query query = em.createNativeQuery(sql);
        
        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);
        query.setParameter("userIds", userIds);
        
        if (productId != null) {
            query.setParameter("productId", productId);
        }
        
        return mapResultToIncomeUser(query.getResultList());
    }
    
    protected List<IncomeUser> mapResultToIncomeUser(List<Object[]> rsList) {
        List<IncomeUser> reportList = new LinkedList<>();
        
        for (Object[] ele : rsList) {
            IncomeUser report = new IncomeUser();
            
            report.setUserId((Integer) ele[0]);
            report.setUserName((String) ele[1]);
            report.setFullName((String) ele[2]);
            report.setUserType((String) ele[3]);
            report.setTotalContracts(((BigDecimal) ele[4]).longValue());
            report.setTotalFee((Double) ele[5]);
            report.setTotalCommission((Double) ele[6]);
            
            reportList.add(report);
        }
        
        return reportList; 
    }

    @Override
    public DailyIncomeReport getReportByDay(Date date, Collection<Integer> userIds, Integer productId) {
        String sql = "SELECT \n" + 
                "    COALESCE(SUM(dr.total_contracts), 0) AS totalContract,\n" + 
                "    COALESCE(SUM(dr.total_fee), 0) AS totalFee,\n" + 
                "    COALESCE(SUM(dr.total_income), 0) AS totalIncome\n" + 
                "FROM\n" + 
                "    daily_report dr\n" + 
                "WHERE\n" + 
                "    dr.date = :date\n" +
                "%s" +
                ";";
        
        String condition = "";
        if (userIds != null && !userIds.isEmpty()) {
            condition += "    AND dr.user_id in :userIds\n";
        }
        
        if (productId != null) {
            condition += "    AND dr.product_id = :productId";
        }
        
        sql = String.format(sql, condition);
        
        Query query = em.createNativeQuery(sql);
        
        query.setParameter("date", date);
        
        if (userIds != null && !userIds.isEmpty()) {
            query.setParameter("userIds", userIds);
        }
        
        if (productId != null) {
            query.setParameter("productId", productId);
        }
        
        List<Object[]> rs = query.getResultList();
        DailyIncomeReport rep = new DailyIncomeReport();
        for (Object[] result : rs) {
            rep.setDate(date);
            rep.setTotalContract(((BigDecimal) result[0]).intValue());
            rep.setTotalFee((Double) result[1]);
        }
        
        return rep;
    }
}
