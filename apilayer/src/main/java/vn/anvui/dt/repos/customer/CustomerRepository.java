package vn.anvui.dt.repos.customer;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import vn.anvui.dt.entity.Customer;

@Repository
public interface CustomerRepository
        extends JpaRepository<Customer, Integer>, CrudRepository<Customer, Integer>, CustomerRepositoryCustom {
    List<Customer> findByContractId(Integer contractId);

    List<ContractIdOnly> findAllByPhoneNumber(String phoneNumber);

    List<ContractIdOnly> findAllByCitizenId(String citizenId);

    List<CitizenAndContractIdOnly> findAllByContractIdIn(List<Integer> contractIdsList);

    @Query(value = "select * from customer c1\n"
            + "where c1.contract_id is null and c1.tag = 0 and c1.assigned_user_id is null\n"
            + "    order by c1.created_date asc limit ?1", nativeQuery = true)
    List<Customer> findPotentialCustomer(Integer size);
    
    @Query(value = "select count(1) from customer c1\n"
            + "where c1.contract_id is null and c1.tag = 0 and c1.assigned_user_id is null\n", 
            nativeQuery = true)
    Integer countPotentialCustomer();

    @Query(value = "select c.* from customer c inner join "
            + "contract co on co.contract_status = 1 and co.inherit_customer_id = c.id\n"
            + "    and c.tag = 0 and c.assigned_user_id is null\n"
            + "    order by c.created_date asc limit ?1", nativeQuery = true)
    List<Customer> findContractFilledCustomer(Integer size);
    
    @Query(value = "select count(1) from customer c inner join "
            + "contract co on co.contract_status = 1 and co.inherit_customer_id = c.id\n"
            + "    and c.tag = 0 and c.assigned_user_id is null\n", 
            nativeQuery = true)
    Integer countContractFilledCustomer();

    Customer findFirstByPhoneNumberAndTagInAndContractIdIsNull(String phoneNumber, List<Integer> tags);

    @Query(value = "select c1.* from customer c1 inner join contract co\n"
            + "        on co.inherit_customer_id = c1.id and co.contract_status = 1 and c1.tag in ?1\n"
            + "            and c1.phone_number = ?2 limit 1", nativeQuery = true)
    Customer findContractCreatedContactByPhoneNumberAndTag(List<Integer> tags, String phoneNumber);
    
    @Query(value = "select count(1) from (\n" + 
            "select c.id from customer c where c.contract_id is null and c.tag = 0 and c.assigned_user_id is null\n" + 
            "union\n" + 
            "select c1.id from customer c1 inner join contract co \n" + 
            "        on co.inherit_customer_id = c1.id and co.contract_status = 1\n" + 
            "                and c1.tag = 0 and c1.assigned_user_id is null) as jt", nativeQuery = true)
    Integer countUnassignedContacts();
    
    public interface CitizenAndContractIdOnly {
        public String getCitizenId();

        public Integer getContractId();
    }

    public interface ContractIdOnly {
        public Integer getContractId();
    }
}
