package vn.anvui.dt.repos.onlineTransaction;

import java.util.Collection;
import java.util.Date;

import vn.anvui.dt.model.TransactionReportDetail;

public interface OnlineTransactionRepositoryCustom {
    Collection<TransactionReportDetail> findTransactionDetail(Integer paymentType, Integer status,
            Date fromDate, Date toDate);

    TransactionReportDetail findTransactionDetail(String paymentId);
}
