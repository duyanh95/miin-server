package vn.anvui.dt.repos.privilege;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import vn.anvui.dt.entity.Privilege;

@Repository
public interface PrivilegeRepository extends JpaRepository<Privilege, Integer>, 
		JpaSpecificationExecutor<Privilege> {
    List<Privilege> findAllByNameIn(List<String> privileges);

}
