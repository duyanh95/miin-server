package vn.anvui.dt.repos.customer;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.model.ContactCrmInfo;
import vn.anvui.dt.model.ContactReport;

public class CustomerRepositoryImpl implements CustomerRepositoryCustom {
    static final Logger log = Logger.getLogger(CustomerRepositoryImpl.class.getName());

    @PersistenceContext
    EntityManager em;

    @Override
    public List<Customer> getPotentialCustomer(Integer size, Integer offset, String phoneNumber, String email,
            String customerName, Date fromDate, Date toDate, Integer tag, Integer userId) throws Exception {
        // String sql = "select c1.* from customer c1 inner join \n" +
        // "(select min(id) as id from customer c where c.contract_id is null group by
        // c.phone_number) as jt\n" +
        // " on c1.id = jt.id \n" +
        // " %s";
        String sql = "select c1.* from customer c1\n" + "    %s";

        Query sqlQuery = buildPotentialCustomerQuery(sql, phoneNumber, email, customerName, size, offset, fromDate,
                toDate, tag, false, userId);

        return sqlQuery.getResultList();
    }

    public Query buildPotentialCustomerQuery(String queryStr, String phoneNumber, String email, String name,
            Integer size, Integer offset, Date fromDate, Date toDate, Integer tag, boolean onContract, Integer userId) {

        String format = "    where true";

        if (userId != null) {
            format += " and c1.assigned_user_id = :userId";
        }

        if (!onContract) {
            format += " and c1.contract_id is null";
        }

        if (!StringUtils.isEmpty(phoneNumber)) {
            format += " and c1.phone_number like :phoneNumber";
        }

        if (!StringUtils.isEmpty(email)) {
            format += " and c1.email like :email";
        }

        if (!StringUtils.isEmpty(name)) {
            format += " and c1.customer_name like :name";
        }

        if (tag != null) {
            format += " and tag = :tag";
        } else {
            format += " and tag <> 2";
        }

        if (fromDate != null && toDate != null) {
            format += " and c1.created_date between :fromDate and :toDate";
        }

        format += "\n        order by c1.created_date desc";
        if (offset != null)
            format += " limit :limit offset :offset";

        queryStr = String.format(queryStr, format);

        Query query = em.createNativeQuery(queryStr, Customer.class);

        if (userId != null) {
            query.setParameter("userId", userId);
        }

        if (!StringUtils.isEmpty(phoneNumber)) {
            query.setParameter("phoneNumber", "%" + phoneNumber + "%");
        }

        if (!StringUtils.isEmpty(email)) {
            query.setParameter("email", email + "%");
        }

        if (!StringUtils.isEmpty(name)) {
            query.setParameter("name", "%" + name + "%");
        }

        if (tag != null) {
            query.setParameter("tag", tag);
        }

        if (fromDate != null && toDate != null) {
            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
        }

        if (offset != null) {
            query.setParameter("limit", size);
            query.setParameter("offset", offset);
        }
        return query;
    }

    @Override
    public List<Customer> getPotentialCustomerOnContract(Integer size, Integer offset, String phoneNumber, String email,
            String customerName, Date fromDate, Date toDate, Integer tag, Integer userId) throws Exception {
        // String sql = "select c1.* from customer c1 inner join\n" +
        // " (select min(c2.id) as id from customer c2 inner join contract co\n" +
        // " on co.inherit_customer_id = c2.id and co.contract_status = 1 group by
        // c2.phone_number) as cj\n" +
        // " on cj.id = c1.id\n" +
        // " %s";
        String sql = "select c1.* from customer c1 inner join\n"
                + "    contract co on co.inherit_customer_id = c1.id and c1.contract_id = co.id\n"
                + "and co.contract_status = 1\n" + "    %s";

        Query sqlQuery = buildPotentialCustomerQuery(sql, phoneNumber, email, customerName, size, offset, fromDate,
                toDate, tag, true, userId);

        return sqlQuery.getResultList();
    }

    @Override
    public List<ContactReport> getContactReportForSale(Integer userId, Date fromDate, Date toDate) throws Exception {
        Integer[] tagList = { 0, 1, 2, 3, 5 };
        List<ContactReport> repList = new ArrayList<>();
        
        for (Integer tag : tagList) {
            ContactReport rep;
            if (userId == null && tag != 3 && tag != 5) {
                rep = new ContactReport();
                rep.setTag(tag);
                rep.setCount(0);
            } else {
                rep = getContactReportOnTag(userId, fromDate, toDate, tag);
            }
            
            repList.add(rep);
        }
        
        return repList;
    }

    private ContactReport getContactReportOnTag(Integer userId, Date fromDate, Date toDate, Integer tag) {
        String sql = "select count(c.id) from customer c \n" + 
                "        %s";

        String format = "where true";

        // if query only sale
        if (userId != null) {
            format += " and c.assigned_user_id = :userId";
        
            if (tag == 3 || tag == 5) {
                format = "inner join contract co on c.id = co.inherit_customer_id\n" + format;
                format += " and c.tag = :tag";
                if (fromDate != null && toDate != null) {
                    format += " and co.created_time between :fromDate and :toDate";
                }
            } else {
                format = "inner join contact_assignment ca on ca.id = c.assignment_id\n" + format;
                if (fromDate != null && toDate != null) {
                    format += " and ca.created_time between :fromDate and :toDate";
                }
    
                format += " and c.tag = :tag";
            }
        } else {
            format += " and c.assigned_user_id is null";
            
            format = "inner join contract co on c.id = co.inherit_customer_id"
                    + " and co.created_user_id is null\n" + format;
            format += " and c.tag = :tag";
            if (fromDate != null && toDate != null) {
                format += " and co.created_time between :fromDate and :toDate";
            }
        }
        
        sql = String.format(sql, format);

        Query sqlQuery = em.createNativeQuery(sql);

        if (userId != null) {
            sqlQuery.setParameter("userId", userId);
        }

        if (fromDate != null && toDate != null) {
            sqlQuery.setParameter("fromDate", fromDate);
            sqlQuery.setParameter("toDate", toDate);
        }
        
        if (tag != null) {
            sqlQuery.setParameter("tag", tag);
        }

        Integer rs = ((BigInteger) sqlQuery.getSingleResult()).intValue();

        ContactReport rep = new ContactReport();
        rep.setTag(tag);

        if (rs != null)
            rep.setCount(rs);

        rep.setUserId(userId);

        return rep;
    }
    
    @Transactional(rollbackFor=Exception.class)
    public List<ContactCrmInfo> getFullContactInformation(Date fromDate, Date toDate, boolean notExportedOnly) {
        String sql = "select cu.customer_name as customerName, cu.citizen_id as citizenId,\n" + 
                "cu.dob as dob, cu.gender as gender,\n" + 
                "pa.customer_name as partnerName, pa.citizen_id as partnerCitizenId,\n" + 
                "pa.dob as partnerDob, pa.gender as partnerGender,\n" + 
                "cu.phone_number as phoneNumber, cu.email as email, cu.created_date as createdDate,\n" + 
                "c.fee as fee, t.team as team, t.agent as agent, t.campaign as campaign,\n" + 
                "t.medium as medium, t.source as source, t.channel as channel\n" + 
                "        from customer cu\n" + 
                "        left join contract c on cu.contract_id = c.id \n" + 
                "    left join customer pa on c.partner_id = pa.id\n" + 
                "    left join tracking t on t.customer_id = cu.id\n" + 
                "where %s cu.created_date between :fromDate and :toDate \n" + 
                "        and (c.partner_id != cu.id or cu.contract_id is null)\n" + 
                "        and c.product_id = 1\n" +
                "order by cu.created_date";
        String updateSql = "update customer set export_status = 1 where id in (\n" + 
                "        select id from \n" + 
                "                (select cu.id as id\n" + 
                "                                from customer cu\n" + 
                "                                left join contract c on cu.contract_id = c.id \n" + 
                "                                left join customer pa on c.partner_id = pa.id\n" + 
                "                                left join package p on p.id = c.package_id\n" + 
                "                                left join tracking t on t.customer_id = cu.id\n" + 
                "                        where %s cu.created_date between :fromDate and :toDate\n" + 
                "                                and (c.partner_id != cu.id or cu.contract_id is null)) as x\n" + 
                "                                and c.product_id = 1\n" +
                ")";
        
        String statusQuery;
        if (notExportedOnly) {
            statusQuery = "cu.export_status = 0 and";
        } else {
            statusQuery = "cu.export_status = 1 and";
        }
        
        sql = String.format(sql, statusQuery);
        updateSql = String.format(updateSql, statusQuery);
        
        Query query = em.createNativeQuery(sql, "ContactCrmInfo");
        Query updateQuery = em.createNativeQuery(updateSql);
        
        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);
        updateQuery.setParameter("fromDate", fromDate);
        updateQuery.setParameter("toDate", toDate);
        
        List<ContactCrmInfo> contactInfo = query.getResultList();
        updateQuery.executeUpdate();
        
        return contactInfo;
    }
}
