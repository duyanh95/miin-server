package vn.anvui.dt.repos.userClosure;

import java.util.Collection;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import vn.anvui.dt.entity.UserClosure;
import vn.anvui.dt.entity.UserClosure.UserClosureKey;

public interface UserClosureRepository extends JpaRepository<UserClosure, UserClosureKey> {
    
    @Query(value = "select u.descendance_id as descendance_id from user_closure u where u.ascendance_id = ?1", 
            nativeQuery = true)
    Collection<Integer> findDescendanceIdFromId(Integer userId);
    
    @Query(value = "select u.descendance_id as descendance_id from user_closure u where u.ascendance_id in ?1", 
            nativeQuery = true)
    Collection<Integer> findDescendanceIdFromId(Collection<Integer> userIds);
    
    @Query(value = "select u.descendance_id as descendance_id from user_closure u where u.ascendance_id = ?1"
            + " limit ?2 offset ?3", 
            nativeQuery = true)
    Collection<Integer> findDescendanceIdFromId(Integer userId, Integer count, Integer offset);
    
    @Query(value = "select * from user_closure u where u.ascendance_id = ?1", 
            nativeQuery = true)
    Collection<UserClosure> findDescendanceFromId(Integer userId);
    
    @Query(value = "select * from user_closure u where u.descendance_id = ?1", 
            nativeQuery = true)
    Collection<UserClosure> findAscendanceFromId(Integer userId);
    
    @Query(value = "select u.ascendance_id from user_closure u where u.descendance_id = ?1", 
            nativeQuery = true)
    Collection<Integer> findAscendanceIdFromId(Integer userId);
    
    @Query(value = "select u.ascendance_id from user_closure u where u.descendance_id in ?1", 
            nativeQuery = true)
    Collection<Integer> findAscendanceIdFromIdIn(Collection<Integer> userIds);
    
    @Query(value = "select u.depth from user_closure u where u.descendance_id = ?1 and u.ascendance_id = ?1"
            + " limit 1",
            nativeQuery = true)
    Integer findDepthFromId(Integer parentId);
    
    @Query(value = "select u.ascendance_id from user_closure u where u.ascendance_id = u.descendance_id"
            + " and u.depth = 1", nativeQuery = true)
    Set<Integer> findRootUsers();
    
    @Query(value = "select * from user_closure u where u.descendance_id in ?1 and u.depth = 1", nativeQuery = true)
    Collection<UserClosure> findRootUsersClosureByDescendanceIds(Collection<Integer> descIds);
    
    
    Boolean existsByKey(UserClosureKey key);
}
