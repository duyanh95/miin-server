package vn.anvui.dt.repos.SystemPhase;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import vn.anvui.dt.entity.SystemPhase;

@Repository
public interface SystemPhaseRepository extends JpaRepository<SystemPhase, Integer>, 
        CrudRepository<SystemPhase, Integer> {

}
