package vn.anvui.dt.repos.onlineTransaction;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import vn.anvui.dt.model.TransactionReportDetail;

public class OnlineTransactionRepositoryImpl implements OnlineTransactionRepositoryCustom {
    
    @PersistenceContext
    EntityManager em;
    
    @Override
    public Collection<TransactionReportDetail> findTransactionDetail(Integer paymentType, Integer status,
            Date fromDate, Date toDate) {
        String sqlString = "select ot.created_date as createdDate, ot.amount as amount,\n" + 
                "    ot.payment_id as paymentId, ot.payment_type as paymentType, ot.merchant_id as merchantId,\n" + 
                "    ot.status as status, co.contract_code as contractCode,\n" + 
                "    c.email as email, c.customer_name as customerName, p.customer_name as partnerName\n" + 
                "from online_transaction ot\n" + 
                "inner join contract co on co.id = ot.contract_id\n" + 
                "inner join customer c on c.id = co.inherit_customer_id\n" +
                "inner join customer p on p.id = co.partner_id\n" +
                "where ot.payment_type = :paymentType %s\n" + 
                "    and ot.created_date between :fromDate and :toDate\n" + 
                "    order by ot.created_date desc";
        if (status != null)
            sqlString = String.format(sqlString, "and ot.status = :status");
        else
            sqlString = String.format(sqlString, "");
            
        Query query = em.createNativeQuery(sqlString, "TransactionReportDetail");
        
        
        if (status != null)
            query.setParameter("status", status);
        
        query.setParameter("paymentType", paymentType);
        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);
        
        Collection<TransactionReportDetail> rs = query.getResultList();
        return rs;
    }
    
    @Override
    public TransactionReportDetail findTransactionDetail(String paymentId) {
        String sqlString = "select ot.created_date as createdDate, ot.amount as amount,\n" + 
                "    ot.payment_id as paymentId, ot.payment_type as paymentType, ot.merchant_id as merchantId,\n" + 
                "    ot.status as status, co.contract_code as contractCode,\n" + 
                "    c.email as email, c.customer_name as customerName, p.customer_name as partnerName\n" + 
                "from online_transaction ot\n" + 
                "inner join contract co on co.id = ot.contract_id\n" + 
                "inner join customer c on c.id = co.inherit_customer_id\n" +
                "inner join customer p on p.id = co.partner_id\n" +
                "where ot.payment_id = :paymentId\n" + 
                "    order by ot.created_date desc limit 1";
            
        Query query = em.createNativeQuery(sqlString, "TransactionReportDetail");
        
        query.setParameter("paymentId", paymentId);
        
        List<TransactionReportDetail> rs = query.getResultList();
        if (rs.isEmpty())
            return null;
        else 
            return rs.get(0);
    }
}
