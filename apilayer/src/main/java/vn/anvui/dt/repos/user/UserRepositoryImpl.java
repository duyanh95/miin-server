package vn.anvui.dt.repos.user;

import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.context.annotation.PropertySource;
import org.springframework.util.StringUtils;

import vn.anvui.dt.entity.User;

@PropertySource({" classpath:${SYSTEM_ENVIRONMENT_VARIABLE}_mysql.properties "})
public class UserRepositoryImpl implements UserRepositoryCustom {
    
    private static final Logger log = Logger.getLogger(UserRepositoryImpl.class.getName());
	
	@PersistenceContext
	private EntityManager entityManager;

    @Override
    public List<User> getListRequestedAgency(Integer page, Integer count, String citizenId, String phoneNumber) {
        String sql = "select * from user where status = 2\n" +
                "    %s";
        
        Query query = buildUserQuery(sql, page, count, citizenId, phoneNumber, null, null, null);
        return query.getResultList();
    }
    
    @Override
    public List<User> findUsersBy(String userName, String citizenId, String phoneNumber, String email,
            Collection<Integer> ids) {
        String sql = "select * from user where status = 1\n" +
                "    %s";

        Query query = buildUserQuery(sql, null, null, citizenId, phoneNumber, ids, email, userName);
        return query.getResultList();
    }
    
    Query buildUserQuery(String template, Integer page, Integer count, String citizenId, String phoneNumber,
            Collection<Integer> userIds, String email, String userName) {
        String query = "";
        
        if (!StringUtils.isEmpty(citizenId)) {
            query += " and citizen_id = :citizenId";
        }
        
        if (!StringUtils.isEmpty(phoneNumber)) {
            query += " and phone_number = :phoneNumber";
        }
        
        if (!StringUtils.isEmpty(email)) {
            query += " and email = :email";
        }
        
        if (!StringUtils.isEmpty(userName)) {
            query += " and user_name = :userName";
        }
        
        if (userIds != null && !userIds.isEmpty()) {
            query += " and id in :userIds";
        } 
        
        query += " order by id desc";
        
        if (page != null && count != null) {
            query += " limit :count offset :offset";
        }
        
        String sql = String.format(template, query);
        
        log.info(sql);
        
        Query sqlQuery = entityManager.createNativeQuery(sql, User.class);
        
        if (!StringUtils.isEmpty(citizenId)) {
            sqlQuery.setParameter("citizenId", citizenId);
        }
        
        if (!StringUtils.isEmpty(phoneNumber)) {
            sqlQuery.setParameter("phoneNumber", phoneNumber);
        }
        
        if (!StringUtils.isEmpty(email)) {
            sqlQuery.setParameter("email", email);
        }
        
        if (!StringUtils.isEmpty(userName)) {
            sqlQuery.setParameter("userName", userName);
        }
        
        
        if (userIds != null && !userIds.isEmpty()) {
            sqlQuery.setParameter("userIds", userIds);
        } 
        
        if (page != null && count != null) {
            sqlQuery.setParameter("count", count);
            sqlQuery.setParameter("offset", count * page);
        }
        
        return sqlQuery;
    }
}	
