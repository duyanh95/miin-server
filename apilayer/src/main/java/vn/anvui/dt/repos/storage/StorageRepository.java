package vn.anvui.dt.repos.storage;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.StorageResources;

public interface StorageRepository extends JpaRepository<StorageResources, Integer>,
        CrudRepository<StorageResources, Integer> {
    StorageResources findOneByResourceUrl(String resourceUrl);
    
}
