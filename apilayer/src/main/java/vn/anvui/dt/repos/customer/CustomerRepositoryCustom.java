package vn.anvui.dt.repos.customer;

import java.util.Date;
import java.util.List;

import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.model.ContactCrmInfo;
import vn.anvui.dt.model.ContactReport;

public interface CustomerRepositoryCustom {

    List<Customer> getPotentialCustomerOnContract(Integer size, Integer offset, String phoneNumber, String email,
            String customerName, Date fromDate, Date toDate, Integer tag, Integer userId) throws Exception;

    List<Customer> getPotentialCustomer(Integer size, Integer offset, String phoneNumber, String email,
            String customerName, Date fromDate, Date toDate, Integer tag, Integer userId) throws Exception;
    
    List<ContactReport> getContactReportForSale(Integer userId, Date fromDate, Date toDate) throws Exception;
    
    public List<ContactCrmInfo> getFullContactInformation(Date fromDate, Date toDate, boolean notExportedOnly);
}
