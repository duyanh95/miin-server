package vn.anvui.dt.repos.accesstoken;

import java.util.Date;
import java.util.List;

import org.hibernate.annotations.Cache;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import vn.anvui.dt.entity.AccessToken;

@Repository
public interface AccessTokenRepository
        extends JpaRepository<AccessToken, Integer>, JpaSpecificationExecutor<AccessToken> {

    @Cacheable(cacheNames = "token")
    AccessToken findFirstByTokenOrderByCreatedDateDesc(String token);
    
    @CacheEvict(cacheNames = "token")
    List<AccessToken> findAllByExpiredDateLessThan(Date date);

    List<AccessToken> findByDeviceToken(String deviceToken);
    
    List<AccessToken> findAllByUserId(Integer userId);
}
