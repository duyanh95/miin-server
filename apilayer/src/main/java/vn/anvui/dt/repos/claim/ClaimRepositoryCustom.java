package vn.anvui.dt.repos.claim;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import vn.anvui.dt.model.ClaimInfo;

public interface ClaimRepositoryCustom {
    List<ClaimInfo> queryClaim(Integer offset, Integer size, Date fromDate, Date toDate, String contractCode,
            String citizenId, String phoneNumber, Integer productId, Collection<Integer> claimStatuses,
            Collection<Integer> userIds);
}
