package vn.anvui.dt.repos.sms;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.Sms;

public interface SmsRepository extends JpaRepository<Sms, Integer>,
        CrudRepository<Sms, Integer> {
    
}
