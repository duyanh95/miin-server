package vn.anvui.dt.repos.user;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import vn.anvui.dt.entity.Role;
import vn.anvui.dt.entity.User;

@Repository
public interface UserRepository
        extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User>, UserRepositoryCustom {
    Set<IdOnly> findAllBy();
    
    Long countByAgencyCode(String agencyCode);
    
    User findFirstByUserName(String userName);

    User findFirstByPhoneNumber(String phoneNumber);

    User findFirstByPhoneNumberAndUserType(String phoneNumber, Integer userType);
    
    @Query("SELECT u.roles FROM User u WHERE u.id = :id")
    List<Role> getRolesByUserId(@Param("id") Integer id);
    
    List<User> getAllByParentId(Integer id);
    Set<IdOnly> findAllByParentId(Integer id);
    
    List<IdOnly> findAllByUserTypeIn(List<String> userType);
    List<User> getAllByUserTypeIn(List<String> userType);
    
    List<User> findByUserTypeAndStatusOrderByFullNameAsc(String userType, Integer status);
    
    Boolean existsByUserName(String userName);
    
    Boolean existsByEmail(String email);
    
    List<UserIdNameOnly> findAllByIdIn(Collection<Integer> ids);
    
    public interface IdOnly {
        Integer getId();
    }
    
    public interface UserIdNameOnly {
        public Integer getId();
        public String getUserName();
    }
    
    User findFirstByGoogleId(String googleId);
}
