package vn.anvui.dt.repos.claim;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.claim.ClaimType;

public interface ClaimTypeRepository extends JpaRepository<ClaimType, Integer>,
        CrudRepository<ClaimType, Integer> {
    List<ClaimType> findAllByProductId(Integer productId);
}
