package vn.anvui.dt.repos.claim;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.claim.Claim;

public interface ClaimRepository extends CrudRepository<Claim, Integer>, JpaRepository<Claim, Integer>,
        ClaimRepositoryCustom {

}
