package vn.anvui.dt.repos.contract;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.ContractEffectiveDate;

public interface ContractEffectiveDateRepository extends CrudRepository<ContractEffectiveDate, Integer>,
        JpaRepository<ContractEffectiveDate, Integer> {
    
}
