package vn.anvui.dt.repos.claim;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import vn.anvui.dt.model.ClaimInfo;
import vn.anvui.dt.repos.contract.ContractRepositoryImpl;

@Repository
public class ClaimRepositoryImpl implements ClaimRepositoryCustom {
    static final Logger log = Logger.getLogger(ContractRepositoryImpl.class.getSimpleName());

    @PersistenceContext
    EntityManager em;
    
    @Override
    public List<ClaimInfo> queryClaim(Integer offset, Integer size, Date fromDate, Date toDate, String contractCode,
            String citizenId, String phoneNumber, Integer productId, Collection<Integer> claimStatuses,
            Collection<Integer> userIds) {
        String sql = "select cl.id as id, cl.created_time as createdTime, c.contract_code as contractCode, cu.customer_name as customerName, " +
                "cu.phone_number as phoneNumber, cl.claim_status as claimStatus, cl.product_id as productId from claim cl\n" + 
                "inner join claim_docs cld on cl.id = cld.claim_id\n" + 
                "inner join contract c on cl.contract_id = c.id \n" + 
                "inner join customer cu on c.inherit_customer_id = cu.id \n" + 
                "where true";
        
        String sqlPredicate = "";
        if (claimStatuses != null && (!claimStatuses.isEmpty())) {
            sqlPredicate += " and cl.claim_status in :claimStatuses";
        }
        if (productId != null) {
            sqlPredicate += " and cl.product_id = :productId"; 
        }
        if (fromDate != null) {
            sqlPredicate += " and cl.create_time >= :fromDate";
        }
        if (toDate != null) {
            sqlPredicate += " and cl.create_time < :toDate";
        }
        if (!StringUtils.isEmpty(contractCode)) {
            sqlPredicate += " and c.contract_code like %:contractCode";
        }
        if (!StringUtils.isEmpty(citizenId)) {
            sqlPredicate += " and cu.citizen_id like %:citizenId";
        }
        if (!StringUtils.isEmpty(phoneNumber)) {
            sqlPredicate += " and cu.phone_number like %:phoneNumber";
        }
        if (userIds != null && (!userIds.isEmpty())) {
            sqlPredicate += " and cl.user_id in :userIds";
        }
        
        sqlPredicate += " group by id order by id desc";
        
        if (size != null) {
            sqlPredicate += " limit :size";
        }
        if (offset != null) {
            sqlPredicate += " offset :offset";
        }
        sql += sqlPredicate;
        
        log.fine(sql);
        
        Query query = em.createNativeQuery(sql, "ClaimInfo");
        
        if (claimStatuses != null && (!claimStatuses.isEmpty())) {
            query.setParameter("claimStatuses", claimStatuses);
        }
        if (productId != null) {
            query.setParameter("productId", productId);
        }
        if (fromDate != null) {
            query.setParameter("fromDate", fromDate);
        }
        if (toDate != null) {
            query.setParameter("toDate", toDate);
        }
        if (!StringUtils.isEmpty(contractCode)) {
            query.setParameter("contractCode", contractCode);
        }
        if (!StringUtils.isEmpty(citizenId)) {
            query.setParameter("citizenId", citizenId);
        }
        if (!StringUtils.isEmpty(phoneNumber)) {
            query.setParameter("phoneNumber", phoneNumber);
        }
        if (userIds != null && (!userIds.isEmpty())) {
            query.setParameter("userIds", userIds);
        }
        if (size != null) {
            query.setParameter("size", size);
        }
        if (offset != null) {
            query.setParameter("offset", offset);
        }
        
        return query.getResultList();
    }

}
