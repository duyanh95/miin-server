package vn.anvui.dt.repos.claim;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.claim.ClaimDocs;

public interface ClaimDocsRepository extends CrudRepository<ClaimDocs, Integer>, 
        JpaRepository<ClaimDocs, Integer> {
    ClaimDocs findFirstByClaimIdAndProductClaimId(Integer claimId, Integer productClaimId);
}
