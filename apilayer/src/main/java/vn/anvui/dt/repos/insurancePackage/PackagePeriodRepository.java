package vn.anvui.dt.repos.insurancePackage;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.PackagePeriod;

public interface PackagePeriodRepository extends JpaRepository<PackagePeriod, Integer>,
        CrudRepository<PackagePeriod, Integer> {
    List<PackagePeriod> findAllByPackageId(Integer packageId);
    
    PackagePeriod findFirstByPackageIdAndPeriodType(Integer pkgId, Integer periodType);
}
