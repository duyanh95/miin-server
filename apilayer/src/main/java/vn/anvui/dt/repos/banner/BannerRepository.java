package vn.anvui.dt.repos.banner;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import vn.anvui.dt.entity.Banner;

public interface BannerRepository extends JpaRepository<Banner, Integer>, 
        CrudRepository<Banner, Integer> {
    
    List<Banner> findAllByActiveAndBannerTypeOrderByIdDesc(boolean active, Integer bannerType);
}
