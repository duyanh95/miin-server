package vn.anvui.dt.utils;

public class EnvironmentHelper {

	public static final String ENVIRONMENT_DEVELOPMENT = "dev";

	public static final String ENVIRONMENT_TEST = "test";
	
	public static final String ENVIRONMENT_PRODUCTION = "pro";

	public static final String SYSTEM_ENVIRONMENT_VARIABLE = "SYSTEM_ENVIRONMENT_VARIABLE";

	public static boolean isProduction() {
		return ENVIRONMENT_PRODUCTION.equalsIgnoreCase(System.getenv(SYSTEM_ENVIRONMENT_VARIABLE));
	}

	public static boolean isDevelopment() {
		return ENVIRONMENT_DEVELOPMENT.equalsIgnoreCase(System.getenv(SYSTEM_ENVIRONMENT_VARIABLE));
	}

	public static boolean isTest() {
		return ENVIRONMENT_TEST.equalsIgnoreCase(System.getenv(SYSTEM_ENVIRONMENT_VARIABLE));
	}
}
