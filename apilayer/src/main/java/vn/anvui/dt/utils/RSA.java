package vn.anvui.dt.utils;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.InputStream;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.xml.bind.DatatypeConverter;

import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;


public class RSA {
    static String private_key = "MIICXQIBAAKBgQCQ+zkQkbH602Jsax0dG5Iam7NwQ0XBdxjMUK1dF5d9RBr8ZQj1P2ZTvublRiJ7xVhv/HYzCxKpbsaJvUqW7LZVMcfajGC2EGiY1wxhttH4X1i4F0YgrI1iy7NVoKLsTQ+tYDwNRx42rjCXZMAYwO1MLF5LYiMPXIIMiYh5taA9kQIDAQABAoGAQjEAd6kdCfgyam9o5l4tEUre3LVKBq76OmXnEKZxunY6n9r9AQNGE5wHzMt9eOa7nr0ztdArYAG8USyD/m3qPEA/03fGR36sP6Ehsw8S3TWk0BCtXMBRCQazC15vT3KyXWH2vFTv6ok79K4hOFehCk6Mw81fe+Q8Cts7FxyfzTUCQQDWuFkNh3f9H+UEK5F1mvdqIVgQbncKXNT4Z/GtmWEhcNk+0Na/K+flGRIje55xLm6JOfTEaxsfWIciQGjSG2xbAkEArNqdrGKUMvXCYfAeSUIONOFbYG+PTSVtruoTUJb/rO9zKgQ02Sx7y+/QQUKBgh6FWCHPtL+eK1l1dibtGh1RgwJBANNhrJm7EY7pxUWztoF2y7d5l/6lIR/cf/UEL1Jdutd3BwfmBGMzUBmj8s1nt3Vo5nzFx4dOgeb7+hzoF8kwIh0CQBX1uC5cX8ORoV5b6R0ZfUCOmNNJmQUOCj88kbOdW2IXEaR24Ffu5NNl5ilET7u+MOafdQnbhjavqNKX42t76g0CQQDEbd0qnOl5BLFALG4OJ7sjcsiJtm7EkpQwduINcROsPM1ZUpP5V8QSelnqRN9u6FaMToGA4KrhAVO+T8rSWwfc";
    static String public_key = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCQ+zkQkbH602Jsax0dG5Iam7NwQ0XBdxjMUK1dF5d9RBr8ZQj1P2ZTvublRiJ7xVhv/HYzCxKpbsaJvUqW7LZVMcfajGC2EGiY1wxhttH4X1i4F0YgrI1iy7NVoKLsTQ+tYDwNRx42rjCXZMAYwO1MLF5LYiMPXIIMiYh5taA9kQIDAQAB";

    public static KeyPair generateKeyPair() throws Exception {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(2048, new SecureRandom());
        KeyPair pair = generator.generateKeyPair();

        return pair;
    }

    public static KeyPair getKeyPairFromKeyStore() throws Exception {
        // Generated with:
        // keytool -genkeypair -alias mykey -storepass s3cr3t -keypass s3cr3t
        // -keyalg RSA -keystore keystore.jks

        InputStream ins = RSA.class.getResourceAsStream("/keystore.jks");

        KeyStore keyStore = KeyStore.getInstance("JCEKS");
        keyStore.load(ins, "s3cr3t".toCharArray()); // Keystore password
        KeyStore.PasswordProtection keyPassword = // Key password
                new KeyStore.PasswordProtection("s3cr3t".toCharArray());

        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry("mykey", keyPassword);

        java.security.cert.Certificate cert = keyStore.getCertificate("mykey");
        PublicKey publicKey = cert.getPublicKey();
        PrivateKey privateKey = privateKeyEntry.getPrivateKey();

        return new KeyPair(publicKey, privateKey);
    }

    public static String encrypt(String plainText, PublicKey publicKey) throws Exception {
        byte[] privateKeyBytes = DatatypeConverter.parseBase64Binary(private_key);

        @SuppressWarnings("unused")
        PrivateKey privateKey = KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));
        Cipher encryptCipher = Cipher.getInstance("RSA");
        encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);

        byte[] cipherText = encryptCipher.doFinal(plainText.getBytes(UTF_8));

        return DatatypeConverter.printHexBinary(cipherText);
    }

    public static String decrypt(String cipherText, PrivateKey privateKey) throws Exception {

        byte[] bytes = DatatypeConverter.parseHexBinary(cipherText);

        Cipher decriptCipher = Cipher.getInstance("RSA");
        decriptCipher.init(Cipher.DECRYPT_MODE, privateKey);

        return new String(decriptCipher.doFinal(bytes), UTF_8);
    }

    public static String sign(String plainText) throws Exception {
        byte[] data = DatatypeConverter.parseBase64Binary(private_key);
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(new ASN1Integer(0));
        ASN1EncodableVector v2 = new ASN1EncodableVector();
        v2.add(new ASN1ObjectIdentifier(PKCSObjectIdentifiers.rsaEncryption.getId()));
        v2.add(DERNull.INSTANCE);
        v.add(new DERSequence(v2));
        v.add(new DEROctetString(data));
        ASN1Sequence seq = new DERSequence(v);
        byte[] privKey = seq.getEncoded("DER");
        PrivateKey privateKey = KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(privKey));
        Signature privateSignature = Signature.getInstance("SHA256withRSA");
        privateSignature.initSign(privateKey);
        privateSignature.update(plainText.getBytes(UTF_8));

        byte[] signature = privateSignature.sign();

        return DatatypeConverter.printHexBinary(signature);
    }

    public static boolean verify(String plainText, String signature) throws Exception {
        byte[] publicKeyBytes = DatatypeConverter.parseBase64Binary(public_key);
        PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(publicKeyBytes));
        Signature publicSignature = Signature.getInstance("SHA256withRSA");
        publicSignature.initVerify(publicKey);
        publicSignature.update(plainText.getBytes(UTF_8));

        byte[] signatureBytes = DatatypeConverter.parseHexBinary(signature);

        return publicSignature.verify(signatureBytes);
    }

    // public static void main(String... argv) throws Exception {
    // //First generate a public/private key pair
    // KeyPair pair = generateKeyPair();
    // //KeyPair pair = getKeyPairFromKeyStore();
    //
    // //Our secret message
    // String message = "the answer to life the universe and everything";
    //
    // //Encrypt the message
    // String cipherText = encrypt(message, pair.getPublic());
    //
    // //Now decrypt it
    // String decipheredMessage = decrypt(cipherText, pair.getPrivate());
    //
    // System.out.println(decipheredMessage);
    //
    // //Let's sign our message
    // String signature = sign("foobar", pair.getPrivate());
    //
    // //Let's check the signature
    // boolean isCorrect = verify("foobar", signature, pair.getPublic());
    // System.out.println("Signature correct: " + isCorrect);
    // }
    public static void main(String[] args) throws Exception {

        String data = "194341b3eaef1558810223c4185a9e8e100005tulala01@mailina.com20150904162600";
        // RSASign.initializeKeys();

        String signature = RSA.sign(data);
        // generatePublicKeyPemFromKeyData(public_key);
        System.out.println("SIGNATURE=" + signature);
        System.out.println(signature.length());
        System.out.println(RSA.verify(data, signature));
    }
}