package vn.anvui.dt.utils;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

public final class StringHelper {

	public static String generateUniqueString() {
		return UUID.randomUUID().toString().replace("-", "");
	}
	
	public static String formatDouble(double d) {
//	    String numberStr;
//	    if(d == (long) d)
//	        numberStr = String.format("%d",(long)d);
//	    else
//	        numberStr = String.format("%s",d);
	    
	    DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRENCH);
	    otherSymbols.setDecimalSeparator(',');
	    otherSymbols.setGroupingSeparator('.'); 
	    DecimalFormat formatter = new DecimalFormat("#,###", otherSymbols);
	    return formatter.format(d);
	}
	
	public static String replaceTemplate(String template, String fieldName, String value) {
        return template.replaceAll("\\$\\{" + fieldName + "\\}", value);
    }
}
