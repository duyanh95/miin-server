package vn.anvui.bsn.beans.miinContract.health;

import vn.anvui.bsn.beans.miinContract.MiinContractService;
import vn.anvui.bsn.beans.miinContract.MiinPeriodContractService;
import vn.anvui.bsn.dto.miinContract.HealthContractRequest;

public interface HealthContractService extends MiinContractService<HealthContractRequest>,
        MiinPeriodContractService {
}
