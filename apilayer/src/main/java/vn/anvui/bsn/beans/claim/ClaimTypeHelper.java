package vn.anvui.bsn.beans.claim;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.anvui.dt.entity.claim.Claim;
import vn.anvui.dt.entity.claim.ClaimDocs;
import vn.anvui.dt.entity.claim.ClaimType;
import vn.anvui.dt.entity.claim.ProductClaimRequirement;
import vn.anvui.dt.repos.claim.ClaimTypeRepository;

@Service
public class ClaimTypeHelper {
    
    @Autowired
    ClaimTypeRepository typeRepo;
    
    public List<ClaimType> getProductClaimTypes(Integer productId) {
        return typeRepo.findAllByProductId(productId);
    }
}
