package vn.anvui.bsn.beans.miinContract.general;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.dt.common.AuthenticationDetails;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.entity.UserClosure.UserClosureKey;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.repos.contract.ContractRepository;
import vn.anvui.dt.repos.userClosure.UserClosureRepository;

@Service
public class MiinGeneralContractServiceImpl implements MiinGeneralContractService {
    final static Logger log = Logger.getLogger(MiinGeneralContractServiceImpl.class.getSimpleName());
    
    @Autowired
    ContractRepository contractRepo;
    
    @Autowired
    UserClosureRepository closureRepo;

    @Override
    public List<Contract> listCustomerContract(String customerName, String citizenId, String email, String phoneNumber,
            Long fromDate, Long toDate, Integer page, Integer size, Integer productId, Boolean isCompleted) {
        User user = null;
        try {
            AuthenticationDetails userDetail = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();

            user = userDetail.getAuthenticatedUser();

        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }
        
        List<Integer> contractStatus = new LinkedList<>();
        contractStatus.add(ContractStatus.CUSTOMER_PAID.getValue());
        if (!isCompleted) {
            contractStatus.add(ContractStatus.CUSTOMER_CREATED.getValue());
        }
        
        List<Integer> userIds = new ArrayList<>(Arrays.asList(user.getId()));
        
        try {
            return new ArrayList<Contract>(contractRepo.miinListContract(customerName, citizenId, email, phoneNumber, fromDate, toDate, 
                    userIds, false, contractStatus, productId, page, size));
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }
        
    }

    @Override
    public List<Contract> listAgencyContract(String customerName, String citizenId, String email, String phoneNumber,
            Long fromDate, Long toDate, Integer page, Integer size, Integer productId, Boolean isCompleted,
            Integer txnType, Integer agencyId) {
        User user = null;
        try {
            AuthenticationDetails userDetail = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();

            user = userDetail.getAuthenticatedUser();

        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }
        
        List<Integer> userIds;
        if (agencyId != null && !closureRepo.existsByKey(new UserClosureKey(user.getId(), agencyId))) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        } else if (agencyId != null) {
            userIds = new ArrayList<>(closureRepo.findDescendanceIdFromId(agencyId));
        } else {
            userIds = new ArrayList<>(closureRepo.findDescendanceIdFromId(user.getId())); 
        }
        
        List<Integer> statuses = listStatuses(isCompleted, txnType);
        
        try {
            return new ArrayList<Contract>(contractRepo.miinListContract(customerName, citizenId, email, phoneNumber, 
                    fromDate, toDate, userIds, false, statuses, productId, page, size));
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }
    }
    
    protected List<Integer> listStatuses(boolean isCompleted, Integer txnType) {
        
        if (txnType == null) {
            return new ArrayList<Integer>(Arrays.asList(
                    ContractStatus.AGENCY_PAID.getValue(),
                    ContractStatus.AGENCY_CREATED.getValue(),
                    ContractStatus.AGENCY_COD_DONE.getValue(),
                    ContractStatus.AGENCY_COD_CREATED.getValue(),
                    ContractStatus.AGENCY_TRANSFER.getValue(),
                    ContractStatus.AGENCY_TRANSFER_DONE.getValue()));
        }
        
        List<Integer> statuses = new ArrayList<>(2);
        switch (txnType) {
        case 0: 
            statuses.add(ContractStatus.AGENCY_PAID.getValue());
            if (isCompleted) {
                statuses.add(ContractStatus.AGENCY_CREATED.getValue());
            }
            break;
            
        case 1:
            statuses.add(ContractStatus.AGENCY_COD_DONE.getValue());
            if (isCompleted) {
                statuses.add(ContractStatus.AGENCY_COD_CREATED.getValue());
            }
            break;
            
        case 2:
            statuses.add(ContractStatus.AGENCY_TRANSFER.getValue());
            if (isCompleted) {
                statuses.add(ContractStatus.AGENCY_TRANSFER_DONE.getValue());
            }
            break;
            
        default:
            throw new AnvuiBaseException(ErrorInfo.BAD_REQUEST.addInfo("field", "txnType")
                    .addInfo("error", "txnType must be in range 0 to 2"));
        }
        
        return statuses;
    }
}
