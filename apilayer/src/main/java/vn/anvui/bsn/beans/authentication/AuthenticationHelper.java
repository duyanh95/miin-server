package vn.anvui.bsn.beans.authentication;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import vn.anvui.dt.common.AuthenticationDetails;
import vn.anvui.dt.entity.User;

@Service
public class AuthenticationHelper implements AuthenticationHelperInterface {
    
    @Override
    public User getUserFromAuthentication() throws Exception {
        
        AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                .getAuthentication().getDetails();
        return userDetails.getAuthenticatedUser();
    }
}
