package vn.anvui.bsn.beans.sms;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.sms.SendToSpeedSmsRequest;
import vn.anvui.dt.entity.Sms;
import vn.anvui.dt.repos.sms.SmsRepository;

@Service
public class SpeedSmsService implements SmsService {
    protected final Logger log = Logger.getLogger(this.getClass().getSimpleName());
    
    @Value("${speedsms.token}")
    private String mAccessToken;
    
    @Autowired
    ObjectMapper mapper;
    
    @Autowired
    SmsRepository smsRepo;
    
    private final String API_URL = "http://api.speedsms.vn/index.php";

    @Override
    public void sendSms(List<String> receivers, String message) throws AnvuiBaseException {
        SendToSpeedSmsRequest req = new SendToSpeedSmsRequest(receivers, message, "2", "");
        String json;
        try {
            json = new String(mapper.writeValueAsBytes(req), "UTF-8");
            
        } catch (JsonProcessingException | UnsupportedEncodingException e1) {
            throw new AnvuiBaseException(ErrorInfo.BAD_REQUEST);
        }
        log.log(Level.WARNING, json);

        try {

            // String basicAuth = "Basic " +
            // Base64.encode(mAccessToken.getBytes(), mAccessToken.length());
            String basicAuth = "Basic " + mAccessToken;
            URL url = new URL(API_URL + "/sms/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", basicAuth);
            conn.setRequestProperty("Content-Type", "application/json");

            conn.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(json);
            wr.flush();
            wr.close();
            
            BufferedReader in;
            try {
                in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            } catch (IOException e) {
                in = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
            }
            String inputLine = "";
            StringBuilder buffer = new StringBuilder();
            
            while ((inputLine = in.readLine()) != null) {
                buffer.append(inputLine);
            }
            in.close();
            String result = buffer.toString();
            
            try {
                SpeedSmsResponse res = mapper.readValue(result, SpeedSmsResponse.class);
                if (res.getStatus().equals("success")) {
                    Sms sms = new Sms();
                    sms.setContent(message);
                    sms.setReceiver(mapper.writeValueAsString(receivers));
                    sms.setSmsId(res.getData().getTranId().toString());
                    
                    smsRepo.save(sms);
                }
            } catch (Exception e) {
                log.log(Level.WARNING, e.getMessage(), e);
            }
            
            log.log(Level.INFO, "result send SMS: " + result);

        } catch (Exception e) {
            log.log(Level.INFO, "SEND_SMS_VERIFY_FAIL: " + e, e);
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }
    }
    
    public static class SpeedSmsResponse {
        private String status;
        
        private Data data;

        public static class Data {
            private Long tranId;
            
            public Long getTranId() {
                return tranId;
            }

            public void setTranId(Long tranId) {
                this.tranId = tranId;
            }
            
            public Data() {
                
            }
        }
        
        public SpeedSmsResponse() {
            
        }
        
        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }
    }
}
