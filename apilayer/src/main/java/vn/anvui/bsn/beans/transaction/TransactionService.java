package vn.anvui.bsn.beans.transaction;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.transaction.TransactionRequest;
import vn.anvui.bsn.dto.transaction.TransactionStatusRequest;
import vn.anvui.dt.model.ContractWrapper;

public interface TransactionService<T extends TransactionRequest, U extends TransactionStatusRequest> {
    
    public ContractWrapper updateTransactionStatus(U req) throws AnvuiBaseException;

    String createTransaction(ContractWrapper contract, T req, boolean isExtension)
            throws AnvuiBaseException;
    
}
