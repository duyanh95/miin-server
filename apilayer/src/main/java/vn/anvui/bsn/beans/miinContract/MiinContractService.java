package vn.anvui.bsn.beans.miinContract;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.miinContract.MiinContractRequest;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.model.ContractWrapper;

public interface MiinContractService<T extends MiinContractRequest> {
    public Contract createCustomerContract(T request) throws AnvuiBaseException;
    
    public Contract createAgencyContract(T request) throws AnvuiBaseException;

    public void sendSuccessMessage(ContractWrapper contract) throws AnvuiBaseException;
    
    public Contract updateContractAfterTransaction(Contract contract);
    
    public void sendStatusNotify(Contract contract, boolean success);
}
