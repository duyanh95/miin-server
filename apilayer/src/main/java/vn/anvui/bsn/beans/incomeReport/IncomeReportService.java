package vn.anvui.bsn.beans.incomeReport;

import java.util.Date;
import java.util.List;

import vn.anvui.dt.entity.DailyIncomeReport;
import vn.anvui.dt.model.IncomeUser;
import vn.anvui.dt.model.MonthsReport;

public interface IncomeReportService {
    public void createIncomeReportByDay(Integer noDay, Date lockDate);

    public List<MonthsReport> getMonthsReport(Integer productId);

    public List<IncomeUser> getIncomeReportByAgency(Date fromDate, Date toDate, 
            Integer userId, Integer productId, Integer page, Integer count);

    public List<DailyIncomeReport> getDailyIncomeReport(Date fromDate, Date toDate, Integer productId);
}
