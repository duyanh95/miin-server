package vn.anvui.bsn.beans.misc;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.dt.entity.Province;
import vn.anvui.dt.repos.Province.ProvinceRepository;

@Service
public class MiscellaneousService {
    @Autowired
    ProvinceRepository provinceRepo;
    
    public List<String> listProvince() throws AnvuiBaseException {
        List<Province> list = provinceRepo.findAll();
        List<String> provinces = new LinkedList<>();
        
        list.forEach(province -> provinces.add(province.getProvince()));
        
        return provinces;
    }
}
