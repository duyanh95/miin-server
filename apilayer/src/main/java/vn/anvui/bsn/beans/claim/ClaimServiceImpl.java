package vn.anvui.bsn.beans.claim;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import vn.anvui.bsn.auth.AuthorizationHelperService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.claim.ClaimCreationRequest;
import vn.anvui.bsn.dto.claim.ClaimDocCreationRequest;
import vn.anvui.bsn.dto.claim.ClaimDocUpdateRequest;
import vn.anvui.dt.common.AuthenticationDetails;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.entity.claim.Claim;
import vn.anvui.dt.entity.claim.ClaimDocs;
import vn.anvui.dt.entity.claim.ProductClaimRequirement;
import vn.anvui.dt.enumeration.ClaimStatus;
import vn.anvui.dt.enumeration.PTIPrivileges;
import vn.anvui.dt.enumeration.UserType;
import vn.anvui.dt.model.ClaimInfo;
import vn.anvui.dt.repos.claim.ClaimDocsRepository;
import vn.anvui.dt.repos.claim.ClaimRepository;
import vn.anvui.dt.repos.claim.ClaimTypeRepository;
import vn.anvui.dt.repos.claim.ProductClaimRequirementRepository;
import vn.anvui.dt.repos.contract.ContractRepository;
import vn.anvui.dt.repos.userClosure.UserClosureRepository;

@Service
public class ClaimServiceImpl implements ClaimServiceInterface {
    private static Logger log = Logger.getLogger("ClaimService");

    @Autowired
    ContractRepository contractRepo;

    @Autowired
    UserClosureRepository closureRepo;

    @Autowired
    ClaimDocsRepository docsRepo;

    @Autowired
    ProductClaimRequirementRepository requirementRepo;
    
    @Autowired
    ClaimRepository claimRepo;
    
    @Autowired
    ClaimTypeRepository claimTypeRepo;
    
    @Autowired
    ClaimTypeHelper claimTypeService;
    
    @Autowired
    AuthorizationHelperService authService;

    @Override
    public Claim createClaim(ClaimCreationRequest req) {
        User loggedUser = authService.getLoggedUser();
        if (loggedUser == null)
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);

        Contract contract = contractRepo.findOne(req.getContractId());
        List<ProductClaimRequirement> requirements = claimTypeRepo.findOne(req.getClaimType()).getRequirements();

        checkValidClaimData(req, contract, loggedUser, requirements);

        return saveClaimFromRequest(req, contract, loggedUser);

    }

    @Transactional
    private Claim saveClaimFromRequest(ClaimCreationRequest req, Contract contract, User loggedUser) {
        log.info("contractId: " + contract.getId());
        Claim claim = req.createClaimFromRequest(contract, loggedUser);

        claim = claimRepo.save(claim);
        
        log.info("saved claim: " + claim.getId());
        Integer claimId = claim.getId();
        List<ClaimDocs> docs = req.getDocuments().stream()
                .map((docReq) -> new ClaimDocs(claimId, docReq.getClaimDocId(), docReq.getResourceURL()))
                .collect(Collectors.toList());
        
        docsRepo.save(docs);

        claim.setDocuments(docs);

        return claim;
    }

    private void checkValidClaimData(ClaimCreationRequest req, Contract contract, User loggedUser,
            List<ProductClaimRequirement> requirements) throws AnvuiBaseException {
        if (contract == null)
            throw new AnvuiBaseException(ErrorInfo.DATA_NOT_EXIST);
        if (closureRepo.findAscendanceIdFromId(loggedUser.getId()).contains(contract.getId()))
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);

        if (req.getFromDate().after(req.getToDate())) {
            log.warning("date invalid");
            throw new AnvuiBaseException(ErrorInfo.BAD_REQUEST.addInfo("error", "date invalid"));
        }
        
        Map<Integer, ProductClaimRequirement> requiredMap = requirements.stream()
                .collect(Collectors.toMap(ProductClaimRequirement::getId, require -> require));
        
        for (ClaimDocCreationRequest docReq : req.getDocuments()) {
            if (!requiredMap.containsKey(docReq.getClaimDocId())) {
                log.warning("claimDocId invalid");
                throw new AnvuiBaseException(ErrorInfo.BAD_REQUEST.addInfo("error", "claimDocId invalid"));
            } else if (requiredMap.get(docReq.getClaimDocId()).getNumberOfImages() < 
                    docReq.getResourceURL().size()) {
                log.warning("number of image invalid");
                throw new AnvuiBaseException(ErrorInfo.BAD_REQUEST.addInfo("error", "invalid number of image"));
            }
        }
    }

    @Override
    public ClaimDocs updateClaimDocuments(Integer claimId, ClaimDocCreationRequest req) {

        return updateDocs(claimId, req);
    }

    @Override
    public Claim updateClaimStatus(Integer claimId, Integer claimStatus) {
        Claim claim = claimRepo.findOne(claimId);
        if (claim == null)
            throw new AnvuiBaseException(ErrorInfo.DATA_NOT_EXIST);
        
        if (claimStatus.equals(ClaimStatus.PAPER_REQUIRED.getValue())) {
            if (!claimRequirementsIsSufficient(claim))
                throw new AnvuiBaseException(ErrorInfo.CLAIM_DOCS_MISSING);
        } else if (claimStatus.equals(ClaimStatus.DONE.getValue())) {
            if (! (claimRequirementsIsSufficient(claim) && claimRequirementAccepted(claim)))
                throw new AnvuiBaseException(ErrorInfo.CLAIM_DOCS_MISSING);
        } else if (!claimStatus.equals(ClaimStatus.REJECTED.getValue())) {
            throw new AnvuiBaseException(ErrorInfo.BAD_REQUEST.addInfo("error", "status is invalid"));
        }
        
        claim.setClaimStatus(claimStatus);
        
        return claim;
    }

    @Override
    public List<ClaimInfo> getListClaim(Integer page, Integer count, Date fromDate, Date toDate, String phoneNumber,
            String citizenId, String contractCode, Integer productId, List<Integer> claimStatuses) {
        User loggedUser = authService.getLoggedUser();
        if (loggedUser == null)
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        
        Collection<Integer> descIds = null;
        if (authService.hasPrivilege(PTIPrivileges.READ_ADMIN)) {
            descIds = closureRepo.findDescendanceIdFromId(loggedUser.getId());
        }
        
        Integer offset = (page != null && count != null) ? page * count : null;
        return claimRepo.queryClaim(offset, count, fromDate, toDate, contractCode, citizenId, 
                phoneNumber, productId, claimStatuses, descIds);
    }

    @Override
    public Claim updateClaimInfo(Integer claimId, ClaimCreationRequest req) {
        User loggedUser = authService.getLoggedUser();
        if (loggedUser == null)
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        
        Claim claim = claimRepo.findOne(claimId);
        if (claim == null)
            throw new AnvuiBaseException(ErrorInfo.DATA_NOT_EXIST);
        
        Collection<Integer> descIds = closureRepo.findDescendanceIdFromId(loggedUser.getId());
        if (!descIds.contains(claim.getUserId())) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        if (!loggedUser.getUserType().equals(UserType.ADMIN.getValue()) 
                && !ClaimStatus.UPDATE_ALLOWED_STATUS.contains(claim.getClaimStatus()))
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        
        claim = req.updateClaimWithRequest(claim, loggedUser);
        
        List<ClaimDocs> docs = req.getDocuments().stream()
                .map((docReq) -> new ClaimDocs(claimId, docReq.getClaimDocId(), docReq.getResourceURL()))
                .collect(Collectors.toList());
        
        docsRepo.save(docs);

        claim.setDocuments(docs);
        
        return claimRepo.save(claim);
    }

    protected boolean claimRequirementsIsSufficient(Claim claim) {
        List<ProductClaimRequirement> require = claimTypeRepo.findOne(claim.getClaimType()).getRequirements();
        Map<Integer, ClaimDocs> docs = claim.getDocuments().stream()
                .collect(Collectors.toMap(ClaimDocs::getProductClaimId, doc -> doc));
        
        for (ProductClaimRequirement req : require) {
            if (docs.containsKey(req.getId())) {
                return false;
            } else if (!req.getNumberOfImages().equals(docs.get(req.getId()).getResourceUrl().size())) {
                return false;
            }
        }
        
        return true;
    }
    
    protected boolean claimRequirementAccepted(Claim claim) {
        return claim.getDocuments().stream().map(ClaimDocs::getAccepted).reduce((x, y) -> x && y).get();
    }
    
    protected ClaimDocs updateDocs(Integer claimId, ClaimDocCreationRequest req) {
        Claim claim = claimRepo.findOne(claimId);
        ProductClaimRequirement requirement = requirementRepo.findOne(req.getClaimDocId());
        
        if (claim == null || requirement == null)
            throw new AnvuiBaseException(ErrorInfo.DATA_NOT_EXIST);
        
        ClaimDocs doc = docsRepo.findFirstByClaimIdAndProductClaimId(claimId, req.getClaimDocId());
        if (doc == null)
            doc = new ClaimDocs(claimId, req.getClaimDocId(), req.getResourceURL());
        
        if (doc.getAccepted())
            throw new AnvuiBaseException(ErrorInfo.BAD_REQUEST);
        
        doc.setResourceUrl(req.getResourceURL());
        
        if (req instanceof ClaimDocUpdateRequest) {
            ClaimDocUpdateRequest request = (ClaimDocUpdateRequest) req;
            
            doc.setAccepted(request.getAccepted());
            
            if (!doc.getAccepted()) {
                doc.setError(request.getError());
                doc.setSolution(request.getSolution());
                doc.setNote(request.getNote());
                
                claim.setClaimStatus(ClaimStatus.REVISED_REQUIRED.getValue());
                claimRepo.save(claim);
            }
        }
        
        return docsRepo.save(doc);
    }

    @Override
    public ClaimDocs updateClaimDocsRequest(Integer claimId, ClaimDocUpdateRequest req) {
        return updateDocs(claimId, req);
    }

    @Override
    public Claim getClaimInfo(Integer id) {
        User loggedUser = authService.getLoggedUser();
        if (loggedUser == null)
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        
        Collection<Integer> descId = closureRepo.findDescendanceIdFromId(loggedUser.getId());        
        Claim claim = claimRepo.findOne(id);
        
        if (descId.contains(claim.getUserId()) || authService.hasPrivilege(PTIPrivileges.READ_ADMIN)) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        Contract contract = contractRepo.findOne(claim.getContractId());
        claim.setContract(contract);
        
        return claim;
    }
}
