package vn.anvui.bsn.beans.miinContract.health;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;

import vn.anvui.api.amqp.MessageProducer;
import vn.anvui.bsn.beans.mailing.MailingService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.contract.ContractNotifyData;
import vn.anvui.bsn.dto.mailing.AttachmentFile;
import vn.anvui.bsn.dto.mailing.AttachmentFile.FileType;
import vn.anvui.bsn.dto.notification.NotificationDto;
import vn.anvui.bsn.dto.sms.SmsQueueDto;
import vn.anvui.dt.common.StaticValue;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.InsurancePackage;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.enumeration.NotificationTarget;
import vn.anvui.dt.enumeration.NotificationType;
import vn.anvui.dt.enumeration.PeriodType;
import vn.anvui.dt.enumeration.PhaseActionType;
import vn.anvui.dt.enumeration.ProductType;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.repos.insurancePackage.InsurancePackageRepository;
import vn.anvui.dt.utils.StringHelper;

@Service
public class HealthContractNotification {
    private final Logger log = Logger.getLogger(HealthContractNotification.class.getName());
    
    private static final Integer PRODUCT_ID = ProductType.HEALTH.getValue();
    
    private static final String EMAIL_HTML = "Email/BHNV.html";
    private static final String WARNING_EMAIL = "Email/BHNV/BHNV-Warn.html";
    private static final String TERMINATE_EMAIL = "Email/BHNV/BHNV-Expired.html";
    
    DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    
    @Value("${health.success.sms}")
    String SUCCESS_SMS;
    @Value("${health.warning.sms}")
    String WARNING_SMS;
    @Value("${health.terminate.sms}")
    String TERMINATE_SMS;
    @Value("${health.success.noti}")
    String SUCCESS_NOTI;
    @Value("${health.warning.noti}")
    String WARNING_NOTI;
    @Value("${health.terminate.noti}")
    String TERMINATE_NOTI;
    
    @Autowired
    InsurancePackageRepository pkgRepo;
    
    @Autowired
    MessageProducer msgProducer;

    @Autowired
    HealthContractPdf pdfService;
    
    @Autowired
    MailingService mailService;
    
    public HealthContractNotification() {
        super();
        format.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
    }
    
    public void sendSuccessMessage(ContractWrapper wrapper) throws AnvuiBaseException {
        if (wrapper.getInsurancePackage() == null) {
            InsurancePackage pkg = pkgRepo.findOne(wrapper.getContract().getPackageId());
            
            wrapper.setInsurancePackage(pkg);
        }
        
        sendSuccessEmail(wrapper);
        sendSuccessSMS(wrapper);
    }
    
    public void sendPhaseNotification(ContractNotifyData contractData) {
        //handling different action types
        PhaseActionType action = contractData.getAction();
        log.info(action.name());
        
        switch (action) {
        case WARNING_NOTIFY:
            sendWarningNotification(contractData.getContractWrapper());
            break;
            
        case WARNING_SMS:
            sendWarningSMS(contractData.getContractWrapper());
            break;
            
        case WARNING_EMAIL:
            sendWarningEmail(contractData.getContractWrapper());
            break;
            
        case TERMINATE_NOTIFY:
            sendTerminateNotification(contractData.getContractWrapper());
            break;
            
        case TERMINATE_EMAIL:
            sendTerminateEmail(contractData.getContractWrapper());
            break;
            
        case TERMINATE_SMS:
            sendTerminateSMS(contractData.getContractWrapper());
            break;
            
        //TODO: extends contract notify
        default:
            break;
        }
        
    }
    
    protected void sendSuccessSMS(ContractWrapper wrapper) {
        Contract contract = wrapper.getContract();
        
        String content = SUCCESS_SMS;
        content = StringHelper.replaceTemplate(content, "contractCode", contract.getContractCode());
        content = StringHelper.replaceTemplate(content, "startDate", format.format(contract.getStartDate()));
        content = StringHelper.replaceTemplate(content, "endDate", format.format(contract.getEndDate()));
        
        SmsQueueDto dto = new SmsQueueDto(
                Arrays.asList(contract.getInheritCustomer().getPhoneNumber()), content);
        
        msgProducer.sendSms(dto);
    }
    
    protected void sendSuccessEmail(ContractWrapper wrapper) {
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        
        ClassPathResource classPath = new ClassPathResource(EMAIL_HTML);
        
        String html;
        try {
            html = new String(FileCopyUtils.copyToByteArray(classPath.getInputStream()), "UTF-8");
        } catch (IOException e) {
            log.log(Level.WARNING, e.getMessage(), e);
            return;
        }
        
        Contract contract = wrapper.getContract();
        
        html = StringHelper.replaceTemplate(html, "contractCode", contract.getContractCode());
        html = StringHelper.replaceTemplate(html, "startDate", format.format(contract.getStartDate()));
        html = StringHelper.replaceTemplate(html, "endDate", format.format(contract.getEndDate()));
        html = StringHelper.replaceTemplate(html, "period", PeriodType.getPeriodTypeStr(contract.getPeriodType()));
        
        if (!ContractStatus.COMPLETED_SET.contains(contract.getContractStatus())) {
            log.warning("transaction failed, mail not sent");
            return;
        }   
        
        if (!StringUtils.isEmpty(contract.getInheritCustomer().getEmail())) {
            List<String> listTo = new ArrayList<>(Arrays.asList(contract.getInheritCustomer().getEmail()));
           
            try {
                String pdfFile = "/tmp/" + System.currentTimeMillis();
                
                AttachmentFile attach = 
                        new AttachmentFile(pdfService.createPdfFile(pdfFile, wrapper), "CNBH.pdf", FileType.PDF);
                
                List<AttachmentFile> files = new ArrayList<>(Arrays.asList(attach));
                
                mailService.sendEmailWithAttachment(listTo, null, null, "Bảo hiểm nằm viện", html, null, files);
            } catch (Exception e) {
                log.log(Level.WARNING, e.getMessage(), e);
                throw new AnvuiBaseException(ErrorInfo.MAIL_SEND_FAIL);
            }
        } else {
            log.warning("no email address provided");
        }
    }
    
    protected void sendWarningNotification(ContractWrapper wrapper) {
        Contract contract = wrapper.getContract();
        
        String content = WARNING_NOTI;
        content = StringHelper.replaceTemplate(content, "contractCode", contract.getContractCode());
        content = StringHelper.replaceTemplate(content, "startDate", format.format(contract.getStartDate()));
        content = StringHelper.replaceTemplate(content, "currentDate", format.format(new Date()));
        
        NotificationDto dto = new NotificationDto();
        dto.setContent(content);
        dto.setTitle("Cảnh báo");
        dto.setNotificationType(NotificationTarget.CUSTOMER.getValue());
        dto.setUserId(contract.getCreatedUserId());
        
        Map<String, String> data = new HashMap<>();
        data.put("type", String.valueOf(NotificationType.WARNING.getValue()));
        data.put("productId", String.valueOf(PRODUCT_ID));
        data.put("contractId", String.valueOf(wrapper.getContract().getId()));
        dto.setData(data);
        
        msgProducer.sendPushServiceQueue(dto);
    }
    
    protected void sendWarningSMS(ContractWrapper wrapper) {
        String currentDate = format.format(new Date());
        String endDate = format.format(wrapper.getContract().getEndDate());
        
        String content = WARNING_SMS;
        content = StringHelper.replaceTemplate(content, "contractCode", wrapper.getContract().getContractCode());
        content = StringHelper.replaceTemplate(content, "endDate", endDate);
        content = StringHelper.replaceTemplate(content, "currentDate", currentDate);
        
        List<String> receivers = 
                new ArrayList<>(Arrays.asList(wrapper.getContract().getInheritCustomer().getPhoneNumber()));
        
        SmsQueueDto sms = new SmsQueueDto(receivers, content);
        
        msgProducer.sendSms(sms);
    }
    
    protected void sendWarningEmail(ContractWrapper wrapper) {
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        
        ClassPathResource classPath = new ClassPathResource(WARNING_EMAIL);
        
        String html;
        try {
            html = new String(FileCopyUtils.copyToByteArray(classPath.getInputStream()), "UTF-8");
        } catch (IOException e) {
            log.log(Level.WARNING, e.getMessage(), e);
            return;
        }
        
        Contract contract = wrapper.getContract();
        html = StringHelper.replaceTemplate(html, "contractCode", contract.getContractCode());
        html = StringHelper.replaceTemplate(html, "endDate", format.format(contract.getEndDate()));
        html = StringHelper.replaceTemplate(html, "currentDate", format.format(new Date()));
        html = StringHelper.replaceTemplate(html, "appURL", StaticValue.APP_URL);
        
        if (!StringUtils.isEmpty(contract.getInheritCustomer().getEmail())) {
            List<String> listTo = new ArrayList<>(Arrays.asList(contract.getInheritCustomer().getEmail()));
           
            try {
                mailService.sendEmail(listTo, null, null, "Bảo hiểm nằm viện", html, null);
            } catch (Exception e) {
                log.log(Level.WARNING, e.getMessage(), e);
                throw new AnvuiBaseException(ErrorInfo.MAIL_SEND_FAIL);
            }
        } else {
            log.warning("no email address provided");
        }
    }
    
    protected void sendTerminateNotification(ContractWrapper wrapper) {
        
        String content = TERMINATE_NOTI;
        content = StringHelper.replaceTemplate(content, "contractCode", wrapper.getContract().getContractCode());
        
        NotificationDto dto = new NotificationDto();
        dto.setContent(content);
        dto.setTitle("Hết hiệu lực");
        dto.setNotificationType(NotificationTarget.CUSTOMER.getValue());
        dto.setUserId(wrapper.getContract().getCreatedUserId());
        
        Map<String, String> data = new HashMap<>();
        data.put("type", String.valueOf(NotificationType.TERMINATE.getValue()));
        data.put("productId", String.valueOf(PRODUCT_ID));
        data.put("contractId", String.valueOf(wrapper.getContract().getId()));
        dto.setData(data);
        
        msgProducer.sendPushServiceQueue(dto);
    }
    
    protected void sendTerminateEmail(ContractWrapper wrapper) {
        ClassPathResource classPath = new ClassPathResource(TERMINATE_EMAIL);
        
        String html;
        try {
            html = new String(FileCopyUtils.copyToByteArray(classPath.getInputStream()), "UTF-8");
        } catch (IOException e) {
            log.log(Level.WARNING, e.getMessage(), e);
            return;
        }
        
        Contract contract = wrapper.getContract();
        html = StringHelper.replaceTemplate(html, "contractCode", contract.getContractCode());
        html = StringHelper.replaceTemplate(html, "appURL", StaticValue.APP_URL);
        
        if (!StringUtils.isEmpty(contract.getInheritCustomer().getEmail())) {
            List<String> listTo = new ArrayList<>(Arrays.asList(contract.getInheritCustomer().getEmail()));
           
            try {
                mailService.sendEmail(listTo, null, null, "Bảo hiểm nằm viện", html, null);
            } catch (Exception e) {
                log.log(Level.WARNING, e.getMessage(), e);
                throw new AnvuiBaseException(ErrorInfo.MAIL_SEND_FAIL);
            }
        } else {
            log.warning("no email address provided");
        }
    }
    
    protected void sendTerminateSMS(ContractWrapper wrapper) {
        String content = TERMINATE_SMS;
        content = StringHelper.replaceTemplate(content, "contractCode", wrapper.getContract().getContractCode());
        
        List<String> receivers = 
                new ArrayList<>(Arrays.asList(wrapper.getContract().getInheritCustomer().getPhoneNumber()));
        
        SmsQueueDto sms = new SmsQueueDto(receivers, content);
        msgProducer.sendSms(sms);
    }

    protected void sendStatusNotification(Contract contract, boolean success) {
        NotificationDto dto = new NotificationDto();
        
        String content, title;
        Map<String, String> data = new HashMap<>();
        data.put("productId", String.valueOf(PRODUCT_ID));
        data.put("contractId", String.valueOf(contract.getId()));
        
        if (success) {
            content = SUCCESS_NOTI;
            content = StringHelper.replaceTemplate(content, "contractCode", contract.getContractCode());
            content = StringHelper.replaceTemplate(content, "startDate", format.format(contract.getStartDate()));
            content = StringHelper.replaceTemplate(content, "endDate", format.format(contract.getEndDate()));
            
            title = "Thành công";
            
            data.put("type", String.valueOf(NotificationType.TRANSACTION_SUCCESS.getValue()));
        } else {
            content = "Thanh toán thất bại.";
            title = "Thất bại";
            
            data.put("type", String.valueOf(NotificationType.TRANSACTION_FAIL.getValue()));
        }
        
        dto.setContent(content);
        dto.setTitle(title);
        dto.setNotificationType(NotificationTarget.CUSTOMER.getValue());
        dto.setUserId(contract.getCreatedUserId());
        dto.setData(data);
        
        msgProducer.sendPushServiceQueue(dto);
    }
}
