package vn.anvui.bsn.beans.user;

import java.util.List;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.account.LoginRequest;
import vn.anvui.bsn.dto.account.LoginResponse;
import vn.anvui.bsn.dto.account.OTPResetRequest;
import vn.anvui.bsn.dto.user.ActiveUserRequest;
import vn.anvui.bsn.dto.user.ActiveUserResponse;
import vn.anvui.bsn.dto.user.ChangePasswordRequest;
import vn.anvui.bsn.dto.user.PhoneLoginDto;
import vn.anvui.bsn.dto.user.UserRequest;
import vn.anvui.dt.entity.AccessToken;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.enumeration.UserStatus;
import vn.anvui.dt.enumeration.UserType;

public interface UserService {
    ActiveUserResponse activeUser(ActiveUserRequest request) throws AnvuiBaseException;
    
    User createUser(UserRequest request, UserType type) throws AnvuiBaseException;

    boolean changePassword(ChangePasswordRequest req) throws AnvuiBaseException;

    List<User> findByUserType(String userType);

    List<User> getAllRootAgencies(String userName, String citizenId, String phoneNumber, String email) 
            throws AnvuiBaseException;

    User listAgencyUserByRootId(Integer agencyId, String userName, 
            String citizenId, String phoneNumber, String email) throws AnvuiBaseException;

    User updateUser(UserRequest req, Integer id) throws AnvuiBaseException;

    void allowAgencyCod(Integer userId, Boolean enable) throws AnvuiBaseException;

    void updateUserStatus(Integer id, UserType type, UserStatus status) throws AnvuiBaseException;

    void resetUserPassword(Integer id, String email) throws AnvuiBaseException;

    PhoneLoginDto sendResetOtp(String account) throws AnvuiBaseException;

    void resetByOtp(OTPResetRequest req) throws AnvuiBaseException;

    AccessToken createAPIKey(Integer id) throws AnvuiBaseException;

    User getUserInfo() throws AnvuiBaseException;

    User registerAgency(UserRequest req) throws AnvuiBaseException;

    List<User> listPendingAgencyRequestUsers(Integer page, Integer count, String citizenId, String phoneNumber)
            throws AnvuiBaseException;

    User confirmAgentAsAgency(UserRequest req, Integer uid) throws AnvuiBaseException;
    
    void updateDeviceToken(String token) throws AnvuiBaseException;

    LoginResponse registerMiinUser(LoginRequest req);

    void sendOTP();

    User verifyPhoneNumber(String otp);

    User createUserFromGooglePayload(Payload payload);
}
