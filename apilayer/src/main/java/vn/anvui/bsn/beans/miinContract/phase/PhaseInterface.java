package vn.anvui.bsn.beans.miinContract.phase;

import java.util.Date;

public interface PhaseInterface {
    void updateContractPhase(Date today);
}
