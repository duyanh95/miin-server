package vn.anvui.bsn.beans.sms;

import java.util.List;

import vn.anvui.bsn.common.AnvuiBaseException;

public interface SmsService {
    public void sendSms(List<String> receivers, String content) throws AnvuiBaseException;
}
