package vn.anvui.bsn.beans.transaction;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import vn.anvui.bsn.beans.mailing.MailingService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.transaction.TransactionRequest;
import vn.anvui.bsn.dto.transaction.payoo.PayooTransactionRequest;
import vn.anvui.bsn.dto.transaction.payoo.PayooTransactionResponse;
import vn.anvui.bsn.dto.transaction.payoo.PayooTransactionStatusRequest;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.InsurancePackage;
import vn.anvui.dt.entity.OnlineTransaction;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.enumeration.CustomerTag;
import vn.anvui.dt.enumeration.PaymentType;
import vn.anvui.dt.enumeration.TransactionStatus;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.repos.contract.ContractRepository;
import vn.anvui.dt.repos.insurancePackage.InsurancePackageRepository;
import vn.anvui.dt.repos.onlineTransaction.OnlineTransactionRepository;
import vn.anvui.dt.repos.user.UserRepository;
import vn.anvui.dt.utils.EncryptionHelper;

@Service
@Qualifier("PayooTransactionService")
public class PayooTransactionService extends TransactionAbstractService
        implements TransactionService<PayooTransactionRequest, PayooTransactionStatusRequest>,
        PayooMailService {
    
    public final Logger log = Logger.getLogger(PayooTransactionService.class.getName());
    public final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    private final static String TITLE = "Hướng dẫn thanh toán trên Payoo";
    private final static String HTML_FILE = "bhty.html";
    private final static long PAYOO_EXPIRED_TIME = 7 * 86400000;
    
    // secret key
    @Value("${pti.payoo.secret}")
    private String SECRET_KEY;

    @Autowired
    OnlineTransactionRepository txnRepo;

    @Autowired
    ContractRepository contractRepo;
    
    @Autowired
    UserRepository userRepo;

    @Autowired
    InsurancePackageRepository pkgRepo;
    
    @Autowired
    MailingService mailService;

    @Override
    public String createTransaction(ContractWrapper contract, PayooTransactionRequest req, boolean isExtension) throws AnvuiBaseException {
        if (!checkContractValidForTransaction(contract, isExtension)) {
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_COMPLETED);
        }
        
        return createNewTransaction(contract, req);
    }

    @Override
    public ContractWrapper updateTransactionStatus(PayooTransactionStatusRequest req) throws AnvuiBaseException {
        Long currentTime = System.currentTimeMillis();
        Date date = req.getTransactionTime();
        
        if (date == null) {
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_BAD_REQUEST);
        }
        log.info ("time: " + date.getTime());

        if (currentTime < req.getTransactionTime().getTime()) {
            log.info("time invalid");
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_BAD_REQUEST);
        }
        
        if (StringUtils.isEmpty(req.getPaymentId())) {
            log.info("paymentId missing");
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_DATA_MISSING);
        }

        OnlineTransaction txn = txnRepo.findFirstByPaymentId(req.getPaymentId());
        if (txn == null) {
            log.info("transaction invalid");
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_BAD_REQUEST);
        }
        
        if (!txn.getStatus().equals(TransactionStatus.PENDING.getValue())) {
            log.info("transaction already completed");
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_ALREADY_COMPLETE);
        }
        
        
        log.info(txn.getId().toString());
        
        if (currentTime > txn.getExpiredDate().getTime()) {
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_EXPIRED);
        }
        
        if (!txn.getContractId().equals(req.getContractId())) {
            log.info("contractId invalid");
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_BAD_REQUEST);
        }

        Contract contract = contractRepo.findOne(txn.getContractId());
        if (contract == null) {
            log.info("contract invalid");
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_BAD_REQUEST);
        }
        
        if (!contract.getContractStatus().equals(ContractStatus.CREATED.getValue())) {
            log.info("contract completed");
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_CONTRACT_ALREADY_COMPLETED);
        }

        if (!txn.getAmount().equals(req.getAmount())) {
            log.info("amount invalid");
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_BAD_REQUEST);
        }

        if (!contract.getInheritCustomer().getCustomerName().equals(req.getCustomerName())) {
            log.info("customerName invalid");
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_BAD_REQUEST);
        }
        
        String transactionTime = format.format(req.getTransactionTime());
        
        String data = req.getPaymentId() + req.getStatus() + req.getCustomerName() + req.getAmount()
        + req.getContractId() + transactionTime + SECRET_KEY;
        log.info(data);
        String hash = EncryptionHelper.md5Encrypt(data);
        
        if (StringUtils.isEmpty(req.getChecksum())) {
            log.info("checksum missing");
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_DATA_MISSING);
        }
        if (!req.getChecksum().equals(hash)) {
            log.info("checksum invalid");
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_BAD_REQUEST);
        }

        switch (req.getStatus()) {
        // Pending
        case 0:
        // Fail
        case 2:
            txn.setStatus(req.getStatus());
            break;
           
        //SUCCESS
        case 1:
            txn.setStatus(req.getStatus());
            
            contract.setContractStatus(ContractStatus.PAID.getValue());
            contract.getInheritCustomer().setTag(CustomerTag.ONLINE.getValue());
            contract.getPartner().setTag(CustomerTag.ONLINE.getValue());
            
            contract = contractRepo.save(contract);
            
            break;
        
        default:
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_BAD_REQUEST);
        }
        
        ContractWrapper res = new ContractWrapper(contract);
        
        InsurancePackage pkg = pkgRepo.findOne(contract.getPackageId());
        
        res.setInsurancePackage(pkg);
        res.setTxn(txn);
        
        sendCustomer(res);

        return res;
    }
    
    public PayooTransactionResponse getPayooTransaction(String paymentId) throws AnvuiBaseException {
        OnlineTransaction txn = txnRepo.findFirstByPaymentId(paymentId);
        
        if (txn == null) {
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_NULL);
        }
        if (txn.getPaymentType() != PaymentType.PAYOO.getValue()) {
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_NULL);
        }
        
        Contract contract = contractRepo.findOne(txn.getContractId());
        if (contract == null) {
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }
        
        if (txn.getExpiredDate().getTime() < System.currentTimeMillis()) {
            txn.setStatus(TransactionStatus.EXPIRED.getValue());
            txn = txnRepo.save(txn);
        }
        
        PayooTransactionResponse res = new PayooTransactionResponse();
        
        String createdTime = format.format(txn.getCreatedDate());
        String expiredTime = format.format(txn.getExpiredDate());
        
        res.setPaymentId(txn.getPaymentId());
        res.setContractId(txn.getContractId());
        res.setCustomerName(contract.getInheritCustomer().getCustomerName());
        res.setContractId(contract.getId());
        res.setStatus(txn.getStatus());
        res.setAmount(pkgRepo.findFirstById(contract.getPackageId()).getFee().longValue());
        res.setCreatedTime(createdTime);
        res.setExpiredTime(expiredTime);
        
        String data = res.getPaymentId() + res.getStatus() + res.getCustomerName() + res.getAmount()
            + res.getContractId() + createdTime + expiredTime + SECRET_KEY;
        String hash = EncryptionHelper.md5Encrypt(data);
        
        res.setChecksum(hash);
        
        return res;
    }
    
    public boolean checkTxnSuccess(Integer txnId) {
        OnlineTransaction txn = txnRepo.findOne(txnId);
        
        if (txn.getStatus().equals(TransactionStatus.SUCCESS.getValue()))
            return true;
        else
            return false;
    }
    
    public void sendGuideMail(String toEmail, String customerName, String paymentId) throws IOException {
        List<String> toMailList = new ArrayList<>(Arrays.asList(toEmail));
        
        ClassPathResource classPath = new ClassPathResource(HTML_FILE);
        byte[] bytes = FileCopyUtils.copyToByteArray(classPath.getInputStream());
        String html = new String(bytes, "UTF-8");
        html = html.replace("${paymentId}", paymentId).replace("${customerName}", customerName);
        
        mailService.sendEmail(toMailList, null, null, TITLE, html, "text/html");
    }

    @Override
    protected String createNewTransaction(ContractWrapper contract, TransactionRequest req) {
        OnlineTransaction txn = new OnlineTransaction();
        
        long amount = getAmount(contract.getContract());

        Long currentTime = System.currentTimeMillis();
        txn.setCreatedDate(new Date(currentTime));
        txn.setExpiredDate(new Date(currentTime + PAYOO_EXPIRED_TIME));

        txn.setContractId(contract.getContract().getId());
        txn.setAmount(amount);
        txn.setPaymentType(PaymentType.PAYOO.getValue());
        txn.setStatus(TransactionStatus.PENDING.getValue());

        txn = txnRepo.save(txn);

        SimpleDateFormat format = new SimpleDateFormat("MMdd");
        String date = format.format(new Date());
        date += "%06d";
        date = String.format(date, txn.getId());
        txn.setPaymentId(date);

        txnRepo.save(txn);

        return txn.getPaymentId();
    }
}
