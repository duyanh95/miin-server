package vn.anvui.bsn.beans.claim;

import java.util.Date;
import java.util.List;

import vn.anvui.bsn.dto.claim.ClaimCreationRequest;
import vn.anvui.bsn.dto.claim.ClaimDocCreationRequest;
import vn.anvui.bsn.dto.claim.ClaimDocUpdateRequest;
import vn.anvui.dt.entity.claim.Claim;
import vn.anvui.dt.entity.claim.ClaimDocs;
import vn.anvui.dt.model.ClaimInfo;

public interface ClaimServiceInterface {
    Claim createClaim(ClaimCreationRequest req);
    
    Claim getClaimInfo(Integer id);
    
    ClaimDocs updateClaimDocuments(Integer claimId, ClaimDocCreationRequest req);
    
    ClaimDocs updateClaimDocsRequest(Integer claimId, ClaimDocUpdateRequest req);
    
    Claim updateClaimInfo(Integer claimId, ClaimCreationRequest req);
    
    List<ClaimInfo> getListClaim(Integer page, Integer count, Date fromDate, Date toDate, String phoneNumber,
            String citizenId, String contractCode, Integer productId, List<Integer> claimStatuses);

    Claim updateClaimStatus(Integer claimId, Integer claimStatus);
}
