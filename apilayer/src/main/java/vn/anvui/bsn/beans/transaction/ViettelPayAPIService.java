package vn.anvui.bsn.beans.transaction;

import vn.anvui.bsn.dto.transaction.viettelpay.ViettelPayCheckTransactionRequest;
import vn.anvui.bsn.dto.transaction.viettelpay.ViettelPayCheckTransactionResponse;
import vn.anvui.bsn.dto.transaction.viettelpay.ViettelPayTransactionStatusRequest;
import vn.anvui.bsn.dto.transaction.viettelpay.ViettelPayTransactionStatusResponse;
import vn.anvui.dt.model.ContractWrapper;

public interface ViettelPayAPIService {
    ViettelPayCheckTransactionResponse checkViettelPayTransaction(ViettelPayCheckTransactionRequest req);
    
    ViettelPayTransactionStatusResponse getTransactionStatusResponse(ContractWrapper contract, String errorCode);

    ViettelPayTransactionStatusResponse getTransactionStatusResponse(ViettelPayTransactionStatusRequest req,
            String errorCode);
}
