package vn.anvui.bsn.beans.contract;

import java.util.Date;
import java.util.List;
import java.util.Map;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.dto.customer.CoupleRequest;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.model.ContractInfo;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.model.DailyReport;
import vn.anvui.dt.model.IncomeUser;
import vn.anvui.dt.model.RejectedContract;

public interface ContractService {
    ContractWrapper getContractById(Integer id) throws AnvuiBaseException;

    void sendMailToCustomer(ContractWrapper contract) throws AnvuiBaseException;

    List<ContractWrapper> listContract(Integer page, Integer size, String phoneNumber) throws AnvuiBaseException;

    List<ContractWrapper> listContractByCitizenId(Integer page, Integer size, String citizenId)
            throws AnvuiBaseException;

    ContractInfo listContractPTI(String customerName, String citizenId, String email, String phoneNumber, Long fromDate,
            Long toDate, Integer page, Integer size, List<Integer> userIds, String source, String medium,
            String campaign) throws AnvuiBaseException;

    ContractWrapper updateContract(Integer id, CoupleRequest req) throws AnvuiBaseException;

    Map<Object, Object> getListContract(String citizenId, String email, String phoneNumber, Date fromDate, Date toDate,
            Integer createdUserId, Boolean includeNullUser, Integer page, Integer size);

    ContractInfo listContractForAgency(String customerName, String citizenId, String email, String phoneNumber,
            Boolean isCompleted, Long fromDate, Long toDate, Integer page, Integer size, List<Integer> userIds,
            Integer txnType) throws AnvuiBaseException;

    List<IncomeUser> getIncomeReportByPTI(Long fromDate, Long toDate) throws AnvuiBaseException;

    ContractInfo getSaleContract(String citizenId, String customerName, String email, String phoneNumber, Long fromDate,
            Long toDate, Integer page, Integer size) throws AnvuiBaseException;

    Contract completeCodContract(Integer id) throws AnvuiBaseException;

    Contract cancelContract(Integer id) throws AnvuiBaseException;

    ContractWrapper confirmContract(Integer id) throws AnvuiBaseException;

    List<ContractWrapper> getNotExportedContract(Boolean includeExported, Long fromDate, Long toDate)
            throws AnvuiBaseException;

    void exportContracts(List<Integer> ids) throws AnvuiBaseException;

    List<RejectedContract> listRejectedContract(Long fromDate, Long toDate) throws AnvuiBaseException;

    Contract cancelCodContract(Integer id) throws AnvuiBaseException;

    List<DailyReport> getDailyReportForAgency(Date fromDate, Date toDate) throws AnvuiBaseException;

    List<IncomeUser> getIncomeReportByAgency(Date fromDate, Date toDate, Integer userId) throws AnvuiBaseException;

    Contract completeAccountTransferContract(Integer contractId) throws AnvuiBaseException;
}
