package vn.anvui.bsn.beans.storage;

import vn.anvui.bsn.dto.file.FileObject;
import vn.anvui.dt.entity.StorageResources;

public interface StorageService {
    StorageResources uploadFile(byte[] file, String fileName);
    
    FileObject getFile(String fileName);
}
