package vn.anvui.bsn.beans.miinContract.health;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import vn.anvui.dt.enumeration.PeriodType;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.utils.StringHelper;

@Service
public class HealthContractPdf {
    public final String CERT_PDF = "Certificate/BHNV.pdf";
    public final String FONT = "utm-aptima.ttf";
    public final BaseColor MIIN_GREEN = new BaseColor(37, 143, 69);
    
    private DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    private DateFormat timeFormat = new SimpleDateFormat("HH 'giờ' mm dd/MM/yyyy");
    
    public HealthContractPdf() {
        super();
        format.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
        timeFormat.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
    }
    
    private enum TextPosition {
        CONTRACT_CODE(397, 567.5f), CREATED_TIME(716, 567.5f), 
        BUYER_NAME(302, 452f), BUYER_PHONE(302, 428f), 
        CUSTOMER_NAME(665, 453), CUSTOMER_CID(665, 428.5f), CUSTOMER_DOB(720, 405), 
        PERIOD_TYPE(361, 343.5f), START_DATE(550, 343.5f), END_DATE(750, 343.5f),
        INSURANCE_1(642, 281), INSURANCE_2(880, 260), INSURANCE_3(880, 239),
        FEE(550, 202.5f)
        ;

        final float x;
        final float y;

        private TextPosition(float x, float y) {
            this.x = x;
            this.y = y;
        }

        public float getX() {
            return this.x;
        }

        public float getY() {
            return this.y;
        }
    }
    
    public File createPdfFile(String pdfFile, ContractWrapper wrapper) throws FileNotFoundException, DocumentException, IOException {
        
        ClassPathResource pdfClassPath;
        pdfClassPath = new ClassPathResource(CERT_PDF);
        byte[] bytes = FileCopyUtils.copyToByteArray(pdfClassPath.getInputStream());
        PdfReader reader = new PdfReader(bytes);
        File pdf = new File(pdfFile);
        pdf.createNewFile();
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(pdfFile));

        PdfContentByte canvas = stamper.getOverContent(1);

        ClassPathResource fontClassPath = new ClassPathResource(FONT);
        byte[] fontBytes = FileCopyUtils.copyToByteArray(fontClassPath.getInputStream());
        BaseFont aptima = BaseFont.createFont(FONT, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, true, fontBytes, null);
        Font font = new Font(aptima, 15.5f);
        
        Phrase phrase = new Phrase(wrapper.getContract().getContractCode(), font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.CONTRACT_CODE.getX(),
                TextPosition.CONTRACT_CODE.getY(), 0);
        
        String createdTime = format.format(wrapper.getContract().getCreatedTime());
        phrase = new Phrase(createdTime, font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.CREATED_TIME.getX(),
                TextPosition.CREATED_TIME.getY(), 0);
        
        String customerName = wrapper.getContract().getInheritCustomer().getCustomerName();
        phrase = new Phrase(customerName, font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.BUYER_NAME.getX(),
                TextPosition.BUYER_NAME.getY(), 0);
        
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.CUSTOMER_NAME.getX(),
                TextPosition.CUSTOMER_NAME.getY(), 0);
        
        phrase = new Phrase(wrapper.getContract().getInheritCustomer().getPhoneNumber(), 
                font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.BUYER_PHONE.getX(),
                TextPosition.BUYER_PHONE.getY(), 0);
        
        String dob = format.format(wrapper.getContract().getInheritCustomer().getDob());
        phrase = new Phrase(dob , font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.CUSTOMER_DOB.getX(),
                TextPosition.CUSTOMER_DOB.getY(), 0);
        
        phrase = new Phrase(wrapper.getContract().getInheritCustomer().getCitizenId(), 
                font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.CUSTOMER_CID.getX(),
                TextPosition.CUSTOMER_CID.getY(), 0);
        
        font.setColor(MIIN_GREEN);
        phrase = new Phrase(PeriodType.getPeriodTypeStr(wrapper.getContract().getPeriodType()), 
                font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.PERIOD_TYPE.getX(),
                TextPosition.PERIOD_TYPE.getY(), 0);
        
        String startDate = timeFormat.format(wrapper.getContract().getStartDate());
        font.setStyle(1);
        phrase = new Phrase(startDate, font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.START_DATE.getX(),
                TextPosition.START_DATE.getY(), 0);
        
        String endDate = timeFormat.format(wrapper.getContract().getEndDate());
        phrase = new Phrase(endDate, font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.END_DATE.getX(),
                TextPosition.END_DATE.getY(), 0);
        
        font.setColor(BaseColor.BLACK);
        String[] insurance = wrapper.getInsurancePackage().getAdditionalInfo().split(",");
        phrase = new Phrase(StringHelper.formatDouble(Double.parseDouble(insurance[0])) + " VNĐ", font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.INSURANCE_1.getX(),
                TextPosition.INSURANCE_1.getY(), 0);
        
        phrase = new Phrase(StringHelper.formatDouble(Double.parseDouble(insurance[1])) + " VNĐ", font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.INSURANCE_2.getX(),
                TextPosition.INSURANCE_2.getY(), 0);
        
        phrase = new Phrase(StringHelper.formatDouble(Double.parseDouble(insurance[2])) + " VNĐ", font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.INSURANCE_3.getX(),
                TextPosition.INSURANCE_3.getY(), 0);
        
        font.setColor(MIIN_GREEN);
        String fee = StringHelper.formatDouble(wrapper.getContract().getFee()) + " VNĐ";
        phrase = new Phrase(fee, font);
        ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, TextPosition.FEE.getX(),
                TextPosition.FEE.getY(), 0);
        
        stamper.close();
        
        return pdf;
    }
}
