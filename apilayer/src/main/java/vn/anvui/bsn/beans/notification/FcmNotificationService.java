package vn.anvui.bsn.beans.notification;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Message.Builder;
import com.google.firebase.messaging.TopicManagementResponse;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.notification.NotificationDto;
import vn.anvui.dt.common.AuthenticationDetails;
import vn.anvui.dt.entity.Device;
import vn.anvui.dt.entity.Notification;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.enumeration.DeviceType;
import vn.anvui.dt.repos.fcmNotification.DeviceRepository;
import vn.anvui.dt.repos.notification.NotificationRepository;

@Service
public class FcmNotificationService implements NotificationService {
    final static Logger log = Logger.getLogger(FcmNotificationService.class.getName());
    
    @Autowired
    NotificationRepository notiRepo;
    
    @Autowired
    DeviceRepository deviceRepo;
    
    @Autowired
    ObjectMapper mapper;
    
    public final static String SYS_TOPIC = "system";
    
    @PostConstruct
    public void initFireBase() throws IOException {
        try {
            FileInputStream serviceAccount = new FileInputStream(System.getenv("FIREBASE_CREDENTIAL"));
    
            FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://miin-6a54d.firebaseio.com/")
                .build();
    
            FirebaseApp.initializeApp(options);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    @Override
    public void createAndSendNotification(NotificationDto dto) {
        String data;
        try {
            data = mapper.writeValueAsString(dto.getData());
        } catch (JsonProcessingException e) {
            log.warning("can't parse data");
            
            return;
        }
        
        Notification noti = new Notification(dto.getNotificationType(), dto.getContent(), dto.getTitle(), data,
                dto.getUserId());
 
        notiRepo.save(noti);
        
        sendNotification(noti);
    }
    
    private void sendNotification(Notification noti) {
        
//        try {
//            initFireBase();
//        } catch (IOException e1) {
//            e1.printStackTrace();
//        }
        
        com.google.firebase.messaging.Notification notification = 
                new com.google.firebase.messaging.Notification(noti.getTitle(), noti.getContent());
        
        Message message;
        String topic;
        if (noti.getUserId() != null) {
            topic = "user" + noti.getUserId();
        } else {
            topic = SYS_TOPIC;
        }
        
        log.info("topic: " + topic);
        
        try {
            Builder msgBuilder = Message.builder()
                    .setNotification(notification)
                    .setTopic(topic);
            
            if (noti.getDataMap() != null)
                msgBuilder.putAllData(noti.getDataMap());
            
            log.info("ready to send");
            String response = FirebaseMessaging.getInstance().send(msgBuilder.build());
            log.info(response);
        } catch (Exception e) {
            log.log(Level.SEVERE , e.getMessage(), e);
        }
    }

    @Override
    public List<Notification> listNotification(Integer page, Integer count, boolean isAgency) {
        
        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getAuthenticatedUser();

            if (user == null)
                throw new Exception();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        
        Pageable pageable = new PageRequest(page, count, sort);
        
        Page<Notification> notis = notiRepo.findAllByUserId(user.getId(), pageable);
        
        return notis.getContent();
    }

    @Override
    public void markAsRead(List<Integer> notiIds) {
        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getAuthenticatedUser();

            if (user == null)
                throw new Exception();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        List<Notification> notiList = notiRepo.findAll(notiIds);
        log.info(notiList.toString());
        
        for (Notification noti : notiList) {
            if (!noti.getUserId().equals(user.getId()))
                throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
            
            noti.setRead(true);
        }
        
        notiRepo.save(notiList);
    }

    @Override
    @Transactional
    public void subscribeSystemTopic(List<String> tokens, Integer userId) {
        
        List<Device> device = deviceRepo.findAllByTokenIn(tokens);
        
        if (device == null) {
            log.warning("device token not existed");
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }
        
        List<String> deviceTokens = device.stream()
                .map(Device::getToken)
                .collect(Collectors.toList());
        
        try {
            TopicManagementResponse resp = FirebaseMessaging.getInstance()
                    .subscribeToTopic(deviceTokens, SYS_TOPIC);
            
            if (resp.getFailureCount() > 0) {
                resp.getErrors().forEach(err -> log.warning(err.getReason()));
                throw new Exception();
            }
            
            resp = FirebaseMessaging.getInstance()
                    .subscribeToTopic(deviceTokens, "user" + userId);
            
            if (resp.getFailureCount() > 0) {
                resp.getErrors().forEach(err -> log.warning(err.getReason()));
                throw new Exception();
            }
            
        } catch (Exception e) {
            log.warning("topic subcription failed");
        }
    }
    
    @Override
    public void addUserToken(String token, Integer userId, DeviceType type) {
        Device device = deviceRepo.findFirstByToken(token);
        
        if (device == null)
            device = new Device(token, userId, type);
        
        deviceRepo.save(device);
    }

}