package vn.anvui.bsn.beans.transaction;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.transaction.TransactionRequest;
import vn.anvui.bsn.dto.transaction.vtcpay.VTCPayTransactionRequest;
import vn.anvui.bsn.dto.transaction.vtcpay.VTCPayTransactionStatusRequest;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.ContractNumber;
import vn.anvui.dt.entity.OnlineTransaction;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.enumeration.CustomerTag;
import vn.anvui.dt.enumeration.PaymentType;
import vn.anvui.dt.enumeration.TransactionStatus;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.repos.contract.ContractNumberRepository;
import vn.anvui.dt.repos.contract.ContractRepository;
import vn.anvui.dt.repos.insurancePackage.InsurancePackageRepository;
import vn.anvui.dt.repos.onlineTransaction.OnlineTransactionRepository;
import vn.anvui.dt.repos.user.UserRepository;
import vn.anvui.dt.utils.EncryptionHelper;

@Service
@Qualifier("VTCPayTransactionService")
public class VTCPayTransactionService  extends TransactionAbstractService
        implements TransactionService<VTCPayTransactionRequest, VTCPayTransactionStatusRequest> {
    
    private final static Logger log = Logger.getLogger(VTCPayTransactionService.class.getName());
    
    @Autowired
    OnlineTransactionRepository txnRepo;
    
    @Autowired
    ContractRepository contractRepo;
    
    @Autowired
    InsurancePackageRepository pkgRepo;
    
    @Autowired
    UserRepository userRepo;
    
    @Autowired
    ContractNumberRepository noRepo;
    
    public static final String VTC_URL = "https://vtcpay.vn/bank-gateway/checkout.html";
    public static final String URL_RETURN = "https://baohiemtinhyeu.vn";
    public static final String WEBSITE_ID = "6588";
    public static final String RECEIVER_ACCOUNT = "0963424567";
    public static final String CURRENCY = "VND";
    public static final String SECRET = "aD@L$J?5M*?!Ngw^";

    @Override
    public String createTransaction(ContractWrapper contract, VTCPayTransactionRequest req, boolean isExtension) 
            throws AnvuiBaseException {
        if (!checkContractValidForTransaction(contract, isExtension)) {
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_COMPLETED);
        }
        
        return createNewTransaction(contract, req); 
    }
    
    @Override
    protected String createNewTransaction(ContractWrapper contract, TransactionRequest req) {
        
        OnlineTransaction txn = new OnlineTransaction();
        
        long amount = getAmount(contract.getContract());
        
        txn.setAmount(amount);
        txn.setContractId(contract.getContract().getId());
        txn.setStatus(TransactionStatus.PENDING.getValue());
        txn.setPaymentType(PaymentType.VTCPAY.getValue());
        txn = txnRepo.save(txn);
        
        String email = contract.getContract().getInheritCustomer().getEmail();
        String phoneNumber = contract.getContract().getInheritCustomer().getPhoneNumber();
        String currency = CURRENCY;
        String receiverAccount = RECEIVER_ACCOUNT;
        String referenceNumber = String.valueOf(txn.getId());
        String urlReturn = URL_RETURN;
        String websiteId = WEBSITE_ID;
        String secret = SECRET;
        
        String signature = "%d|%s|%s|%s|%s|%s|%s|%s|%s";
        signature = String.format(signature, amount, email, phoneNumber, currency,
                receiverAccount, referenceNumber, urlReturn, websiteId, secret);
        signature = EncryptionHelper.sha256Encrypt(signature);
        
        String data = VTC_URL + "?amount=%d&bill_to_email=%s&bill_to_phone=%s&currency=%s"
                + "&receiver_account=%s&reference_number"
                + "=%s&url_return=%s&website_id=%s&signature=%s";
        data = String.format(data, amount, email, phoneNumber, currency, receiverAccount,
                referenceNumber, urlReturn, websiteId, signature);
        
        return data;
    }

    @Override
    public ContractWrapper updateTransactionStatus(VTCPayTransactionStatusRequest req) throws AnvuiBaseException {
        String[] data = req.getData().split("\\|");

        String signature;
        try {
            signature = URLDecoder.decode(req.getData(), StandardCharsets.UTF_8.toString()) + "|" + SECRET;
        } catch (UnsupportedEncodingException e) {
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }
        
        log.info(signature);
        signature = EncryptionHelper.sha256Encrypt(signature).toUpperCase();
        log.info(signature);
        if (!signature.equals(req.getSignature())) {
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_BAD_REQUEST);
        }
        
        Integer reference_number = Integer.valueOf(data[3]);
        Integer status = Integer.valueOf(data[4]);
        String trans_ref_no = data[5];
        log.info("id: " + reference_number);
        OnlineTransaction txn = txnRepo.findOne(reference_number);
        if (txn == null) {
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_NULL);
        }
        
        
        txn.setPaymentId(trans_ref_no);
        txn.setReturnCode(status);
        
        Contract contract = contractRepo.findOne(txn.getContractId());
        
        if (status == 1) {
            txn.setStatus(TransactionStatus.SUCCESS.getValue());
            contract.getInheritCustomer().setTag(CustomerTag.ONLINE.getValue());;
            contract.getPartner().setTag(CustomerTag.ONLINE.getValue());
            
            contract.setPaymentDate(new Date());
            
            if (contract.getContractStatus().equals(ContractStatus.CREATED.getValue())) {
                contract.setContractStatus(ContractStatus.PAID.getValue());
            } else if (contract.getContractStatus().equals(ContractStatus.AGENCY_CREATED.getValue())) {
                contract.setContractStatus(ContractStatus.AGENCY_PAID.getValue());
            }
            
            if (contract.getContractCode().length() < 15) {
                ContractNumber num = noRepo.save(new ContractNumber());
                String contractCode = contract.getContractCode() + "%07d/HD/045-HN.01/PHH.TN.15.2018";
                contractCode = String.format(contractCode, num.getNumber());
                contract.setContractCode(contractCode);
                log.info("code: " + contract.getContractCode().split("/")[0]);
            }
            
            contractRepo.save(contract);
        } else if (status < 0) {
          txn.setStatus(TransactionStatus.FAIL.getValue());
        }
        txnRepo.save(txn);
        
        ContractWrapper conRes = new ContractWrapper(contract);
        conRes.setInsurancePackage(pkgRepo.findOne(contract.getPackageId()));
        conRes.setTxn(txn);
        
        sendCustomer(conRes);
        
        return conRes;
    }

}
