package vn.anvui.bsn.beans.miinContract;

import vn.anvui.bsn.dto.contract.ContractNotifyData;

public interface MiinPeriodContractService {

    void sendPhaseNotification(ContractNotifyData wrapper);
}
