package vn.anvui.bsn.beans.transaction;

import java.io.IOException;

public interface PayooMailService {
    public void sendGuideMail(String toEmail, String customerName, String paymentId) throws IOException;
}
