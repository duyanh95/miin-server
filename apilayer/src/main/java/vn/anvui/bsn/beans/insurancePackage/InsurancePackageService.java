package vn.anvui.bsn.beans.insurancePackage;

import java.util.List;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.dt.entity.InsurancePackage;

public interface InsurancePackageService {
    List<InsurancePackage> getAllPackages() throws AnvuiBaseException;

    List<InsurancePackage> getAllPackages(Integer productId) throws AnvuiBaseException;
}
