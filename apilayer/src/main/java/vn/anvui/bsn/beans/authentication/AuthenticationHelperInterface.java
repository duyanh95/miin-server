package vn.anvui.bsn.beans.authentication;

import vn.anvui.dt.entity.User;

public interface AuthenticationHelperInterface {

    User getUserFromAuthentication() throws Exception;

}
