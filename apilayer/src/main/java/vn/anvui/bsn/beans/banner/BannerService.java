package vn.anvui.bsn.beans.banner;

import java.util.List;

import vn.anvui.bsn.dto.banner.BannerRequest;
import vn.anvui.dt.entity.Banner;
import vn.anvui.dt.enumeration.BannerType;

public interface BannerService {

    List<Banner> listBanner(boolean deleted, BannerType type);

    Banner createBanner(BannerRequest req, BannerType type);

    Banner updateBanner(Integer id, BannerRequest req, BannerType type);

    void updateActiveBanner(Integer id, boolean active, BannerType type);

}
