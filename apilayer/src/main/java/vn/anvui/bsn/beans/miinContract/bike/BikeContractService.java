package vn.anvui.bsn.beans.miinContract.bike;

import vn.anvui.bsn.beans.miinContract.MiinContractService;
import vn.anvui.bsn.dto.miinContract.BikeContractRequest;

public interface BikeContractService extends MiinContractService<BikeContractRequest> {

}
