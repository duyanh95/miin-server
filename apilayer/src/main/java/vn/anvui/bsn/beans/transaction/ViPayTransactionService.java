package vn.anvui.bsn.beans.transaction;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.transaction.TransactionRequest;
import vn.anvui.bsn.dto.transaction.vipay.ViPayTransactionRequest;
import vn.anvui.bsn.dto.transaction.vipay.ViPayTransactionStatusRequest;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.OnlineTransaction;
import vn.anvui.dt.enumeration.PaymentType;
import vn.anvui.dt.enumeration.TransactionStatus;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.repos.insurancePackage.InsurancePackageRepository;
import vn.anvui.dt.utils.EncryptionHelper;

@Service
@Qualifier("ViPayTransactionService")
public class ViPayTransactionService extends TransactionAbstractService
        implements TransactionService<ViPayTransactionRequest, ViPayTransactionStatusRequest> {
    static final Logger log = Logger.getLogger(ViPayTransactionService.class.getSimpleName());

    @Autowired
    InsurancePackageRepository pkgRepo;
    
    public static final String MERCHANT = "PTI";
    public static final String MERCHANT_SITE_ID = "4274";
    public static final String ACCESS_CODE = "RF184730181";
    public static final String SECRET_KEY = "KD455987070184937751";
    public static final String ACCOUNT  = "0100234876201";
    public static final String RETURN_URL = "https://baohiemtinhyeu.vn:8443/pti/contract/transaction/vipay/status";
    public static final String CANCEL_URL = "https://baohiemtinhyeu.vn/thatbai.html";
    public static final String VIPAY_URL = "http://sandbox.viviet.vn/vipay/ecommerce?";

    @Override
    public String createTransaction(ContractWrapper contract, ViPayTransactionRequest req, boolean isExtension) 
            throws AnvuiBaseException {
        if (!checkContractValidForTransaction(contract, isExtension)) {
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_COMPLETED);
        }
        
        return createNewTransaction(contract, req); 
    }

    @Override
    public ContractWrapper updateTransactionStatus(ViPayTransactionStatusRequest req) throws AnvuiBaseException {
        
        Map<String, String> params = new TreeMap<>(req.getParams().entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue()[0])));
        
        if (!params.containsKey("secure_hash")) {
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_DATA_MISSING);
        }
        
        params.remove("secure_hash");
        
        Iterator<String> iter = params.keySet().iterator();
        StringBuilder dataParams = new StringBuilder(SECRET_KEY);
        try {
            while (iter.hasNext()) {
                String paramKey = iter.next();
                dataParams.append("|" + params.get(paramKey));
            }
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }
        log.info(dataParams.toString());

//        if (!EncryptionHelper.sha256Encrypt(dataParams.toString()).equals(hash)) {
//            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_BAD_REQUEST);
//        }
            
        OnlineTransaction txn = txnRepo.findFirstByPaymentId(params.get("order_no"));
        Contract contract = contractRepo.findOne(txn.getContractId());
        
        if (!txn.getStatus().equals(TransactionStatus.PENDING.getValue())) {
            throw new AnvuiBaseException(ErrorInfo.TRANSACTION_ALREADY_COMPLETE);
        }
        
        if (params.get("response_code").equals("0")) {
            log.info("transsaction success");
            txn.setStatus(TransactionStatus.SUCCESS.getValue());
            contract = updateContractStatus(contract);
        } else {
            log.info("transsaction failed");
            txn.setStatus(TransactionStatus.FAIL.getValue());
        }
        
        txnRepo.save(txn);
        
        ContractWrapper conRes = new ContractWrapper(contract);
        conRes.setInsurancePackage(pkgRepo.findOne(contract.getPackageId()));
        conRes.setTxn(txn);
        
        sendCustomer(conRes);
        
        return conRes;
    }

    @Override
    protected String createNewTransaction(ContractWrapper contract, TransactionRequest req) {
        OnlineTransaction txn = new OnlineTransaction();
        
        long amount = getAmount(contract.getContract());
        
        txn.setStatus(TransactionStatus.PENDING.getValue());
        txn.setContractId(contract.getContract().getId());
        txn.setMerchantId(MERCHANT);
        txn.setAmount(amount);
        txn.setPaymentType(PaymentType.VIPAY.getValue());
        
        txn = txnRepo.save(txn);
        
        txn.setPaymentId("PTIVV" + txn.getId());
        
        Map<String, String> params = new TreeMap<>();
        
        params.put("version", "1.0");
//        params.put("locale", "vn");
        params.put("merchant_site", MERCHANT_SITE_ID);
        params.put("access_code", ACCESS_CODE);
        params.put("return_url", RETURN_URL);
//        params.put("cancel_url", CANCEL_URL);
        params.put("merch_txn_ref", txn.getPaymentId());
        params.put("order_no", txn.getPaymentId());
        params.put("order_desc", "thanh toan hop dong bao hiem so " + contract.getContract().getId());
//        params.put("order_info", "Merchant transaction");
//        params.put("shipping_fee", "0");
//        params.put("tax_fee", "0");
//        params.put("currency", "VND");
        params.put("total_amount", String.valueOf(amount * 100));
        
        Iterator<String> iter = params.keySet().iterator();
        StringBuilder dataParams = new StringBuilder(SECRET_KEY);
        StringBuilder pathParams = new StringBuilder();
        try {
            while (iter.hasNext()) {
                String paramKey = iter.next();
                dataParams.append("|" + params.get(paramKey));
                pathParams.append(paramKey + "=" + 
                        params.get(paramKey).replaceAll(" ", "%20") + "&");
            }
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }
        
        log.info(dataParams.toString());
        String hash = EncryptionHelper.sha256Encrypt(dataParams.toString());
        
        pathParams.append("secure_hash=" + hash);
        
        log.info("params: " + pathParams);
        txnRepo.save(txn);
        
        return VIPAY_URL + pathParams.toString();
    }


}
