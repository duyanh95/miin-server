package vn.anvui.bsn.beans.transaction;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import vn.anvui.api.amqp.MessageProducer;
import vn.anvui.bsn.beans.miinContract.MiinContractService;
import vn.anvui.bsn.beans.miinContract.MiinContractServiceDispenser;
import vn.anvui.bsn.beans.miinContract.general.MiinGeneralContractService;
import vn.anvui.bsn.dto.notification.NotificationDto;
import vn.anvui.bsn.dto.transaction.TransactionRequest;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.OnlineTransaction;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.enumeration.NotificationTypeEnum;
import vn.anvui.dt.enumeration.ProductType;
import vn.anvui.dt.enumeration.TransactionSource;
import vn.anvui.dt.enumeration.TransactionStatus;
import vn.anvui.dt.enumeration.NotificationTarget;
import vn.anvui.dt.enumeration.UserType;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.repos.contract.ContractNumberRepository;
import vn.anvui.dt.repos.contract.ContractRepository;
import vn.anvui.dt.repos.onlineTransaction.OnlineTransactionRepository;
import vn.anvui.dt.repos.user.UserRepository;

public abstract class TransactionAbstractService {
    private final Logger log = Logger.getLogger(TransactionAbstractService.class.getName());
    
    static final String BHTY_RETURN_URL = "https://baohiemtinhyeu.vn/";
    static final String MIIN_ANDROID_URL = "";
    static final String MIIN_IOS_URL = "";
    static final String AGENCY_URL = "https://daily.baohiemtinhyeu.vn/";
    
    @Autowired
    MessageProducer msgProducer;
    
    @Autowired
    ContractNumberRepository noRepo;
    
    @Autowired
    ContractRepository contractRepo;
    
    @Autowired
    UserRepository userRepo;
    
    @Autowired
    OnlineTransactionRepository txnRepo;
    
    @Autowired
    MiinGeneralContractService miinService;
    
    @Autowired
    MiinContractServiceDispenser contractServiceDispenser;
    
    protected void sendCustomer(ContractWrapper wrapper) {
        notify(wrapper);
        
        msgProducer.sendMailQueue(wrapper);
    }
    
    protected void notify(ContractWrapper wrapper) {
        MiinContractService<?> miinContractService = 
                contractServiceDispenser.getServiceByProduct(
                        ProductType.fromValue(wrapper.getContract().getProductId()));
        
        if (wrapper.getTxn() != null || wrapper.getTxn().getStatus().equals(TransactionStatus.SUCCESS.getValue()))
            miinContractService.sendStatusNotify(wrapper.getContract(), true);
    }
    
    long getAmount(Contract contract) {
        User createdUser = null;
        if (contract.getCreatedUserId() != null) {
            createdUser = userRepo.findOne(contract.getCreatedUserId());
        }
        
        long amount = contract.getIncome().longValue();
        if (createdUser != null && createdUser.getUserType().equals(UserType.AGENCY.getValue())) {
            amount = contract.getFee().longValue();
        }
        
        return amount;
    }
    
    Contract updateContractStatus(Contract contract) {
        MiinContractService<?> miinContractService = 
                contractServiceDispenser.getServiceByProduct(
                        ProductType.fromValue(contract.getProductId()));
        
        contract = miinContractService.updateContractAfterTransaction(contract);
        
        return contract;
    }
    
    String getReturnURL(TransactionRequest req) {
        if (StringUtils.isNotEmpty(req.getReturnURL())) {
            return req.getReturnURL();
        } else if (req.getSource().equals(TransactionSource.BHTY.getValue())) {
            return BHTY_RETURN_URL;
        } else if (req.getSource().equals(TransactionSource.MIIN_ANDROID.getValue())) {
            return MIIN_ANDROID_URL;
        } else if (req.getSource().equals(TransactionSource.MIIN_IOS.getValue())) {
            return MIIN_IOS_URL;
        } else  {
            return AGENCY_URL;
        } 
    }
    
    boolean checkContractValidForTransaction(ContractWrapper wrapper, boolean isExtension) {
        boolean validStatus = wrapper.getContract().getContractStatus().equals(ContractStatus.CUSTOMER_CREATED.getValue())
                || wrapper.getContract().getContractStatus().equals(ContractStatus.AGENCY_CREATED.getValue()) 
                || wrapper.getContract().getContractStatus().equals(ContractStatus.CREATED.getValue());
        
        boolean extensionValid = !isExtension || wrapper.getContract().getPeriodType() != null;
        
        return validStatus && extensionValid;
    }
    
    protected abstract String createNewTransaction(ContractWrapper contract, TransactionRequest req);
}
