package vn.anvui.bsn.beans.txnUtils;

import java.util.Date;
import java.util.List;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.dt.model.TransactionReport;
import vn.anvui.dt.model.TransactionReportDetail;

public interface TransactionUtilsService {
    List<TransactionReport> getTransactionReport(Date fromDate, Date toDate, String txnType, boolean isSuccess) 
            throws AnvuiBaseException;
    
    List<String> getPaymentType() throws AnvuiBaseException;

    TransactionReportDetail getTransaction(String paymentId) throws AnvuiBaseException;
}
