package vn.anvui.bsn.beans.miinContract.phase;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.anvui.bsn.beans.miinContract.MiinContractServiceDispenser;
import vn.anvui.bsn.beans.miinContract.MiinPeriodContractService;
import vn.anvui.bsn.dto.contract.ContractNotifyData;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.InsurancePackage;
import vn.anvui.dt.entity.SystemPhase;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.enumeration.PhaseActionType;
import vn.anvui.dt.enumeration.ProductType;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.repos.SystemPhase.SystemPhaseRepository;
import vn.anvui.dt.repos.contract.ContractRepository;
import vn.anvui.dt.repos.insurancePackage.InsurancePackageRepository;
import vn.anvui.dt.utils.DateTimeHelper;

@Service
public class PhaseService implements PhaseInterface {
    private static final Logger log = Logger.getLogger(PhaseService.class.getSimpleName());

    @Autowired
    ContractRepository contractRepo;

    @Autowired
    InsurancePackageRepository pkgRepo;

    @Autowired
    SystemPhaseRepository phaseRepo;

    @Autowired
    MiinContractServiceDispenser serviceDispenser;

    @Override
    public void updateContractPhase(Date today) {
        Map<Integer, InsurancePackage> availPackages = pkgRepo.findAllByHasPeriod(true).stream()
                .collect(Collectors.toMap(InsurancePackage::getId, pkg -> pkg));

        Map<Integer, SystemPhase> phases = phaseRepo.findAll().stream()
                .collect(Collectors.toMap(SystemPhase::getFromEndDate, phase -> phase));

        List<Contract> phaseList = contractRepo
                .findAllByPeriodTypeNotNullAndContractStatusInAndPhaseGreaterThanAndEndDateLessThanEqual(
                        ContractStatus.COMPLETED_SET, -1, DateTimeHelper.addDay(today, DateTimeHelper.DAY_OF_YEAR, 1));

        // List<Contract> phaseList = contractRepo.findAll(Arrays.asList(1298));

        for (Contract contract : phaseList) {
            log.info("contract Id: " + contract.getId());
            try {
                Integer dayDiff = DateTimeHelper.getNumberOfDayBetween(today, contract.getEndDate());
                log.info("at day: " + dayDiff);

                MiinPeriodContractService periodService = serviceDispenser
                        .getPeriodServiceByProduct(ProductType.fromValue(contract.getProductId()));

                if (phases.containsKey(dayDiff)) {
                    log.info("phase existed");
                    String[] actions = phases.get(dayDiff).getActionTypes().split(",");

                    for (String action : actions) {
                        ContractWrapper wrapper = new ContractWrapper(contract,
                                availPackages.get(contract.getPackageId()), null);

                        ContractNotifyData data = new ContractNotifyData(wrapper,
                                PhaseActionType.fromValue(Integer.parseInt(action)));

                        periodService.sendPhaseNotification(data);
                    }

                    updatePhase(contract, dayDiff);
                }
            } catch (Exception e) {
                log.log(Level.WARNING, e.getMessage(), e);
            }
        }
    }

    private Contract updatePhase(Contract contract, Integer dayDifference) {
        if (dayDifference >= 3)
            contract.setPhase(-1);
        else 
            contract.setPhase(contract.getPhase() + 1);

        return contractRepo.save(contract);
    }
}
