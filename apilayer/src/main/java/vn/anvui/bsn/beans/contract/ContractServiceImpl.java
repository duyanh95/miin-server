package vn.anvui.bsn.beans.contract;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import vn.anvui.bsn.beans.customer.CustomerService;
import vn.anvui.bsn.beans.mailing.MailingService;
import vn.anvui.bsn.beans.miinContract.MiinContractService;
import vn.anvui.bsn.beans.miinContract.MiinContractServiceDispenser;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.customer.CoupleRequest;
import vn.anvui.bsn.dto.user.UserInfo;
import vn.anvui.dt.common.AuthenticationDetails;
import vn.anvui.dt.entity.ContactLog;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.entity.InsurancePackage;
import vn.anvui.dt.entity.PackagePeriod;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.entity.UserClosure;
import vn.anvui.dt.enumeration.ContactLogType;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.enumeration.CustomerTag;
import vn.anvui.dt.enumeration.ExportStatus;
import vn.anvui.dt.enumeration.ProductType;
import vn.anvui.dt.enumeration.UserType;
import vn.anvui.dt.model.ContractInfo;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.model.DailyReport;
import vn.anvui.dt.model.IncomeUser;
import vn.anvui.dt.model.RejectedContract;
import vn.anvui.dt.repos.contactLog.ContactLogRepository;
import vn.anvui.dt.repos.contract.ContractRepository;
import vn.anvui.dt.repos.customer.CustomerRepository;
import vn.anvui.dt.repos.customer.CustomerRepository.ContractIdOnly;
import vn.anvui.dt.repos.insurancePackage.InsurancePackageRepository;
import vn.anvui.dt.repos.insurancePackage.PackagePeriodRepository;
import vn.anvui.dt.repos.onlineTransaction.OnlineTransactionRepository;
import vn.anvui.dt.repos.user.UserRepository;
import vn.anvui.dt.repos.user.UserRepository.IdOnly;
import vn.anvui.dt.repos.userClosure.UserClosureRepository;
import vn.anvui.dt.utils.DateTimeHelper;

@Service
public class ContractServiceImpl implements ContractService {
    public final static String BCC = "chamsockhachhang@epti.vn";
    public final static String BHNV_HTML = "Email/BHNV.html";
            public final static String BHXM_HTML = "Email/BHXM.html";
    static final Logger log = Logger.getLogger(ContractServiceImpl.class.getSimpleName());

    @Autowired
    OnlineTransactionRepository txnRepo;

    @Autowired
    CustomerService customerService;

    @Autowired
    CustomerRepository customerRepo;

    @Autowired
    ContractRepository contractRepo;

    @Autowired
    InsurancePackageRepository packageRepo;
    
    @Autowired
    PackagePeriodRepository periodRepo;

    @Autowired
    MailingService mailService;

    @Autowired
    UserRepository userRepo;

    @Autowired
    ContactLogRepository logRepo;

    @Autowired
    UserClosureRepository uClosureRepo;
    
    @Autowired
    MiinContractServiceDispenser contractDispenser;

    @Override
    public ContractWrapper getContractById(Integer id) throws AnvuiBaseException {
        log.info("get contract");
        Contract contract = contractRepo.getOne(id);
        contract.getInheritCustomer();
        contract.getPartner();

        InsurancePackage insurancePackage = packageRepo.findOne(contract.getPackageId());
        PackagePeriod period = periodRepo.findFirstByPackageIdAndPeriodType(contract.getPackageId(),
                contract.getPeriodType());
        
        log.info(insurancePackage.getAdditionalInfo());

        ContractWrapper res = new ContractWrapper(contract);
        res.setInsurancePackage(insurancePackage);
        res.setPeriod(period);
        
        return res;
    }
    

    @Override
    public List<ContractWrapper> listContract(Integer page, Integer size, String phoneNumber)
            throws AnvuiBaseException {
        Pageable contractPage = new PageRequest(page, size);

        List<Contract> contracts;

        if (StringUtils.isEmpty(phoneNumber)) {
            contracts = contractRepo.findAllByOrderByCreatedDateDesc(contractPage).getContent();
        } else {
            List<ContractIdOnly> customerList = customerRepo.findAllByPhoneNumber(phoneNumber);
            if (customerList.isEmpty())
                throw new AnvuiBaseException(ErrorInfo.EMPTY);
            Set<Integer> ids = new HashSet<>();
            customerList.forEach((customer) -> {
                ids.add(customer.getContractId());
            });

            contracts = contractRepo.findAll(ids);
        }

        if (contracts.isEmpty())
            throw new AnvuiBaseException(ErrorInfo.EMPTY);

        List<InsurancePackage> packageList = packageRepo.findAll();
        Map<Integer, InsurancePackage> packageMap = packageList.stream()
                .collect(Collectors.toMap(InsurancePackage::getId, Function.identity()));

        List<ContractWrapper> res = new ArrayList<>();
        contracts.forEach(contract -> {
            ContractWrapper contractRes = new ContractWrapper(contract);
            contractRes.setInsurancePackage(packageMap.get(contract.getPackageId()));
            res.add(contractRes);
        });
        return res;
    }

    @Override
    public void sendMailToCustomer(ContractWrapper contract) throws AnvuiBaseException {
        MiinContractService<?> service = 
                contractDispenser.getServiceByProduct(
                        ProductType.fromValue(contract.getContract().getProductId()));
        
        service.sendSuccessMessage(contract);
    }

    @Override
    public List<ContractWrapper> listContractByCitizenId(Integer page, Integer size, String citizenId)
            throws AnvuiBaseException {
        Pageable contractPage = new PageRequest(page, size);

        List<Contract> contracts;

        if (StringUtils.isEmpty(citizenId)) {
            contracts = contractRepo.findAllByOrderByCreatedDateDesc(contractPage).getContent();
        } else {
            List<ContractIdOnly> customerList = customerRepo.findAllByCitizenId(citizenId);
            if (customerList.isEmpty())
                throw new AnvuiBaseException(ErrorInfo.EMPTY);
            Set<Integer> ids = new HashSet<>();
            customerList.forEach((customer) -> {
                ids.add(customer.getContractId());
            });

            contracts = contractRepo.findAll(ids);
        }

        if (contracts.isEmpty())
            throw new AnvuiBaseException(ErrorInfo.EMPTY);

        List<InsurancePackage> packageList = packageRepo.findAll();
        Map<Integer, InsurancePackage> packageMap = packageList.stream()
                .collect(Collectors.toMap(InsurancePackage::getId, Function.identity()));

        List<ContractWrapper> res = new ArrayList<>();
        contracts.forEach(contract -> {
            ContractWrapper contractRes = new ContractWrapper(contract);
            contractRes.setInsurancePackage(packageMap.get(contract.getPackageId()));
            res.add(contractRes);
        });
        return res;
    }

    @Override
    public ContractWrapper updateContract(Integer id, CoupleRequest req) throws AnvuiBaseException {
        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getAuthenticatedUser();

            if (user == null)
                throw new Exception();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }

        Contract contract = contractRepo.findOne(id);
        if (contract == null)
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_NOT_EXISTED);

        if (user.getUserType().equals(UserType.AGENCY_USER.getValue())) {
            if (contract.getCreatedUserId().equals(user.getId()))
                throw new AnvuiBaseException(ErrorInfo.CONTRACT_UPDATE_NOT_ALLOWED);
        } else if (user.getUserType().equals(UserType.AGENCY.getValue())) {
            List<Integer> userIds = new ArrayList<>();
            Set<IdOnly> userIdOnly = userRepo.findAllByParentId(user.getId());
            userIdOnly.forEach((idOnly) -> {
                userIds.add(idOnly.getId());
            });
            userIds.add(user.getId());

            if (userIds.contains(contract.getCreatedUserId()))
                throw new AnvuiBaseException(ErrorInfo.CONTRACT_UPDATE_NOT_ALLOWED);

            if (contract.getContractStatus().equals(ContractStatus.AGENCY_PAID.getValue()))
                throw new AnvuiBaseException(ErrorInfo.CONTRACT_UPDATE_NOT_ALLOWED);
        }

        if (!(contract.getUpdatedTime() == null || user.getUserType().equals(UserType.ADMIN.getValue())))
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_UPDATE_NOT_ALLOWED);

        if (System.currentTimeMillis() - contract.getAppliedDate().getTime() > 86400000) {
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_UPDATE_NOT_ALLOWED);
        }

        Customer inheritance = contract.getInheritCustomer();
        Customer partner = contract.getPartner();

        if (!StringUtils.isEmpty(req.getCitizenId())) {
            inheritance.setCitizenId(req.getCitizenId());
        }

        if (!StringUtils.isEmpty(req.getCustomerName())) {
            inheritance.setCustomerName(req.getCustomerName());
        }

        if (req.getDob() != null) {
            inheritance.setDob(new Date(req.getDob()));
        }

        if (!StringUtils.isEmpty(req.getPartnerCitizenId())) {
            partner.setCitizenId(req.getPartnerCitizenId());
        }

        if (!StringUtils.isEmpty(req.getPartnerName())) {
            partner.setCustomerName(req.getPartnerName());
        }

        if (req.getPartnerDob() != null) {
            partner.setDob(new Date(req.getPartnerDob()));
        }

        if (!StringUtils.isEmpty(req.getEmail())) {
            inheritance.setEmail(req.getEmail());
            partner.setEmail(req.getEmail());
        }
        
        if (req.getGender() != null) {
            log.info("request gender: " + req.getGender().toString());
            inheritance.setGender(req.getGender());
        }
        
        if (req.getPartnerGender() != null) {
            partner.setGender(req.getPartnerGender());
        }
        
        if (inheritance.getGender().equals(partner.getGender())) {
            throw new AnvuiBaseException(ErrorInfo.CUSTOMER_SAME_GENDER);
        }

        if (req.getStartDate() != null) {
            if (req.getStartDate().getTime() < (System.currentTimeMillis() - 86400000)) {
                throw new AnvuiBaseException(ErrorInfo.CONTRACT_DATE_INVALID);
            } else {
                contract.setCreatedDate(DateTimeHelper.getFirstTimeOfDay(req.getStartDate()));
                contract.setAppliedDate(DateTimeHelper.addDay(req.getStartDate(), "month", 1));
                contract.setStartDate(DateTimeHelper.addDay(contract.getCreatedDate(), "year", 3));
                contract.setEndDate(DateTimeHelper.addDay(contract.getStartDate(), "year", 5));
            }
        }

        if (!StringUtils.isEmpty(req.getNote())) {
            inheritance.setNote(req.getNote());
        }

        if (customerService.contractIsExisted(id, inheritance.getCitizenId(), partner.getCitizenId()))
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_EXISTED);

        customerRepo.save(inheritance);
        customerRepo.save(partner);

        Date currentDate = new Date();

        contract.setExportStatus((byte) ExportStatus.UPDATED.getValue());
        contract.setUpdatedTime(currentDate);

        InsurancePackage pack;
        if (req.getPackageId() != null && !contract.getContractStatus().equals(ContractStatus.PAID.getValue())) {
            pack = packageRepo.findOne(req.getPackageId());
            contract.setPackageId(req.getPackageId());
        } else {
            pack = packageRepo.findOne(contract.getPackageId());
        }

        contractRepo.save(contract);

        ContactLog ctsLog = new ContactLog(contract.getInheritCustomer(), contract.getPartner(), contract,
                ContactLogType.UPDATED, user);
        logRepo.save(ctsLog);

        ContractWrapper conRes = new ContractWrapper(contract);
        conRes.setInsurancePackage(pack);

        return conRes;
    }

    @Override
    public ContractInfo listContractForAgency(String customerName, String citizenId, String email, String phoneNumber,
            Boolean isCompleted, Long fromDate, Long toDate, Integer page, Integer size, List<Integer> userIds,
            Integer txnType) throws AnvuiBaseException {
        User user;

        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getUser();

        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }

        if (fromDate == null ^ toDate == null)
            throw new AnvuiBaseException(ErrorInfo.BAD_REQUEST);

        List<Integer> searchUserIds = null;
        if (user.getUserType().equals(UserType.AGENCY.getValue())) {
            if (userIds == null || userIds.isEmpty()) {
                searchUserIds = new ArrayList<>(uClosureRepo.findDescendanceIdFromId(user.getId()));
            } else {
                boolean validAgencyRole = true;
                Set<Integer> descIds =  new HashSet<>(uClosureRepo.findDescendanceIdFromId(user.getId()));
                for (Integer id : userIds) {
                    if(!descIds.contains(id)) {
                        validAgencyRole = false;
                        break;
                    }
                }
               
                if ((!user.getUserType().equals(UserType.ADMIN.getValue())) && (!validAgencyRole))
                    throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);

                searchUserIds = new ArrayList<>(uClosureRepo.findDescendanceIdFromId(userIds));
            }
        } 

        ContractInfo rsList;
        try {
            List<Integer> contractStatus = new LinkedList<>();
            if (txnType != null) {
                switch (txnType) {
                case 0:
                    if (isCompleted) {
                        contractStatus.add(ContractStatus.AGENCY_PAID.getValue());
                    } else {
                        contractStatus.add(ContractStatus.AGENCY_CREATED.getValue());
                    }
                    break;

                case 1:
                    if (isCompleted) {
                        contractStatus.add(ContractStatus.AGENCY_COD_DONE.getValue());
                    } else {
                        contractStatus.add(ContractStatus.AGENCY_COD_CREATED.getValue());
                    }
                    break;

                case 2:
                    if (isCompleted) {
                        contractStatus.add(ContractStatus.AGENCY_TRANSFER_DONE.getValue());
                    } else {
                        contractStatus.add(ContractStatus.AGENCY_TRANSFER.getValue());
                    }
                    break;

                default:
                    throw new AnvuiBaseException(ErrorInfo.BAD_REQUEST);
                }
            } else {
                if (isCompleted) {
                    contractStatus.add(ContractStatus.CUSTOMER_PAID.getValue());
                    contractStatus.add(ContractStatus.AGENCY_COD_DONE.getValue());
                    contractStatus.add(ContractStatus.AGENCY_PAID.getValue());
                    contractStatus.add(ContractStatus.AGENCY_TRANSFER_DONE.getValue());
                } else {
                    contractStatus.add(ContractStatus.CUSTOMER_CREATED.getValue());
                    contractStatus.add(ContractStatus.AGENCY_TRANSFER.getValue());
                    contractStatus.add(ContractStatus.AGENCY_CREATED.getValue());
                }
            }

            rsList = contractRepo.listContractBy(customerName, citizenId, email, phoneNumber, fromDate, toDate,
                    searchUserIds, false, contractStatus, page, size, null, null, null);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }

        Set<Integer> createdUserIds = new HashSet<>(25);
        for (Contract contract : rsList.getContractList()) {
            if (contract.getCreatedUserId() != null) {
                createdUserIds.add(contract.getCreatedUserId());
            }
        }
        if (createdUserIds.isEmpty())
            return rsList;

        log.warning(createdUserIds.toString());

        Map<Integer, Integer> closureMap = uClosureRepo.findRootUsersClosureByDescendanceIds(createdUserIds).stream()
                .collect(Collectors.toMap(UserClosure::getDescendanceId, UserClosure::getAscendanceId));

        Map<Integer, UserInfo> createdUser = userRepo.findAllByIdIn(closureMap.values()).stream()
                .collect(Collectors.toMap(cUser -> cUser.getId(), cUser -> new UserInfo(cUser)));

        Map<Integer, UserInfo> rootUserMaps = closureMap.keySet().stream().collect(
                Collectors.toMap(key -> key, key -> createdUser.getOrDefault(closureMap.get(key), new UserInfo())));

        for (Contract contract : rsList.getContractList()) {
            if (contract.getCreatedUserId() != null) {
                contract.setCreatedUser(rootUserMaps.getOrDefault(contract.getCreatedUserId(), null));
            }
        }

        return rsList;
    }

    @Override
    public ContractInfo listContractPTI(String customerName, String citizenId, String email, String phoneNumber,
            Long fromDate, Long toDate, Integer page, Integer size, List<Integer> userIds, String source, String medium,
            String campaign) throws AnvuiBaseException {
        if (fromDate == null ^ toDate == null)
            throw new AnvuiBaseException(ErrorInfo.BAD_REQUEST);

        boolean includeNullUser = false;
        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getUser();
            if (user == null)
                throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }

        if (user.getUserType().equals(UserType.ADMIN.getValue())) {
            if (userIds == null || userIds.isEmpty())
                includeNullUser = true;
        } else {
            userIds = new ArrayList<>();
            userIds.add(user.getId());
        }

        ContractInfo rsList;
        try {
            List<Integer> status = new ArrayList<>(Arrays.asList(ContractStatus.PAID.getValue()));

            rsList = contractRepo.listContractBy(customerName, citizenId, email, phoneNumber, fromDate, toDate, userIds,
                    includeNullUser, status, page, size, source, medium, campaign);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }

        return rsList;
    }

    @Override
    public Map<Object, Object> getListContract(String citizenId, String email, String phoneNumber, Date fromDate,
            Date toDate, Integer createdUserId, Boolean includeNullUser, Integer page, Integer size) {
        AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                .getAuthentication().getDetails();
        User user = userDetails.getUser();

        if (user.getUserType().equals(UserType.ADMIN.getValue())) {
            includeNullUser = true;
            log.info("admin");
        } else {
            includeNullUser = false;
            createdUserId = user.getId();
            log.info("userId: " + createdUserId);
        }

        return contractRepo.getListContract(citizenId, email, phoneNumber, fromDate, toDate, createdUserId,
                includeNullUser, page, size);
    }

    @Override
    public List<IncomeUser> getIncomeReportByPTI(Long fromDate, Long toDate) throws AnvuiBaseException {
        List<IncomeUser> rsList = new ArrayList<>();

        Long totalContract = 0L;
        Double totalIncome = 0.0;

        IncomeUser all = new IncomeUser();
        all.setUserName("all");
        rsList.add(all);

        // get pti users report
        List<Integer> ptiIds = new ArrayList<>();
        List<String> types = new ArrayList<>();
        types.add(UserType.PTI_USER.getValue());
        types.add(UserType.ADMIN.getValue());
        List<IdOnly> ptiIdOnly = userRepo.findAllByUserTypeIn(types);
        ptiIdOnly.forEach((idOnly) -> {
            ptiIds.add(idOnly.getId());
        });

        try {
            rsList.addAll(contractRepo.reportIncomeByUser(new Date(toDate), new Date(fromDate), true, false, ptiIds));
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }

        // get agencies report
        types = new ArrayList<>();
        types.add(UserType.AGENCY.getValue());

        Set<Integer> parentIds = uClosureRepo.findRootUsers();

        List<User> agencies = userRepo.findAll(parentIds);

        for (User agency : agencies) {
            log.info("agencyId: " + agency.getId());
            Set<IdOnly> agencyUserIdOnly = userRepo.findAllByParentId(agency.getId());
            List<Integer> agencyUserIds = new ArrayList<>();
            agencyUserIdOnly.forEach((idOnly) -> {
                agencyUserIds.add(idOnly.getId());
            });
            agencyUserIds.add(agency.getId());

            try {
                IncomeUser income = contractRepo
                        .reportIncomeByUser(new Date(toDate), new Date(fromDate), false, true, agencyUserIds).get(0);
                income.setUserId(agency.getId());
                income.setFullName(agency.getFullName());
                income.setUserName(agency.getUserName());
                income.setUserType(UserType.AGENCY.getValue());

                rsList.add(income);
            } catch (Exception e) {
                log.log(Level.SEVERE, e.getMessage(), e);
                throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
            }
        }

        // total result
        for (IncomeUser rs : rsList) {
            if (rs.getTotalContracts() != null)
                totalContract += rs.getTotalContracts();
            if (rs.getTotalFee() != null)
                totalIncome += rs.getTotalFee();
        }

        all.setTotalContracts(totalContract);
        all.setTotalFee(totalIncome);

        return rsList;
    }

    @Override
    public List<IncomeUser> getIncomeReportByAgency(Date fromDate, Date toDate, Integer userId)
            throws AnvuiBaseException {
        if (fromDate == null && toDate == null) {
            fromDate = DateTimeHelper.getFirstDayOfMonths(new Date(), TimeZone.getTimeZone("GMT+7:00"));
            toDate = DateTimeHelper.addDay(fromDate, DateTimeHelper.MONTH, 1, TimeZone.getTimeZone("GMT+7:00"));
        }

        if (fromDate == null ^ toDate == null) {
            throw new AnvuiBaseException(ErrorInfo.DATE_FORMAT_INVALID);
        }

        log.info(String.valueOf(fromDate.getTime()));
        log.info(String.valueOf(toDate.getTime()));
        User userLogged = null;

        try {
            AuthenticationDetails userDetail = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();

            userLogged = userDetail.getAuthenticatedUser();

            if (userLogged == null)
                throw new Exception();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }

        List<User> users;
        if (userLogged.getUserType().equals(UserType.AGENCY.getValue())) {

            if (userId == null) {
                users = userRepo.getAllByParentId(userLogged.getId());
            } else {
                if (uClosureRepo.findDescendanceIdFromId(userLogged.getId()).contains(userId))
                    users = userRepo.getAllByParentId(userId);
                else
                    throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
            }

        } else if (userLogged.getUserType().equals(UserType.ADMIN.getValue())) {

            if (userId == null) {
                Set<Integer> rootAgencyIds = uClosureRepo.findRootUsers();
                users = userRepo.findAll(rootAgencyIds);
            } else {
                users = userRepo.getAllByParentId(userId);
            }

        } else {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }

        List<IncomeUser> rsList = new LinkedList<>();
        try {
            Set<Integer> logUsers = new HashSet<>();

            if (userId != null) {
                userLogged = userRepo.findOne(userId);

                logUsers.add(userId);

                IncomeUser rsLogged = contractRepo.getContractReportForUser(fromDate, toDate, logUsers);
                rsLogged.setUser(userLogged);
                rsList.add(rsLogged);
            } else {
                if (!userLogged.getUserType().equals(UserType.ADMIN.getValue())) {
                    logUsers.add(userLogged.getId());

                    IncomeUser rsLogged = contractRepo.getContractReportForUser(fromDate, toDate, logUsers);
                    rsLogged.setUser(userLogged);
                    rsList.add(rsLogged);
                }
            }

            for (User user : users) {
                List<Integer> userIds = new ArrayList<>(uClosureRepo.findDescendanceIdFromId(user.getId()));
                log.info(userIds.toString());
                IncomeUser rs = contractRepo.getContractReportForUser(fromDate, toDate, userIds);
                rs.setUser(user);

                rsList.add(rs);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }

        return rsList;
    }

    @Override
    public ContractInfo getSaleContract(String citizenId, String customerName, String email, String phoneNumber,
            Long fromDate, Long toDate, Integer page, Integer size) throws AnvuiBaseException {
        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getUser();
            if (user == null)
                throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        List<Integer> userIds = new ArrayList<>();
        userIds.add(user.getId());

        boolean includeNullUser = false;
        if (user.getUserType().equals(UserType.ADMIN.getValue())) {
            includeNullUser = true;
            Set<IdOnly> idsOnly = userRepo.findAllBy();
            idsOnly.forEach((idOnly) -> {
                userIds.add(idOnly.getId());
            });
        }

        ContractInfo rs;
        try {
            rs = contractRepo.listCodContract(customerName, citizenId, email, phoneNumber, fromDate, toDate, userIds,
                    page, size, includeNullUser);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }

        return rs;
    }

    @Override
    public Contract completeCodContract(Integer id) throws AnvuiBaseException {
        Contract contract = contractRepo.findOne(id);
        if (contract == null)
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_NOT_EXISTED);

        if (customerService.contractIsExisted(id, contract.getInheritCustomer().getCitizenId(),
                contract.getPartner().getCitizenId()))
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_EXISTED);

        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getUser();
            if (user == null)
                throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        contract.setConfirm(true);

        if (contract.getContractStatus().equals(ContractStatus.COD.getValue())) {
            contract.setContractStatus(ContractStatus.PAID.getValue());
        } else if (contract.getContractStatus().equals(ContractStatus.AGENCY_COD_CREATED.getValue())) {
            contract.setContractStatus(ContractStatus.AGENCY_COD_DONE.getValue());
        } else if (contract.getContractStatus().equals(ContractStatus.AGENCY_TRANSFER_CREATED.getValue())) {
            contract.setContractStatus(ContractStatus.AGENCY_TRANSFER.getValue());
            contract.setConfirm(false);
        } else {
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_UPDATE_NOT_ALLOWED);
        }

        contract.getInheritCustomer().setTag(CustomerTag.COD.getValue());

        return contractRepo.save(contract);
    }

    @Override
    public Contract completeAccountTransferContract(Integer contractId) throws AnvuiBaseException {
        Contract contract = contractRepo.findOne(contractId);

        if (contract == null)
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_NOT_EXISTED);

        if (!contract.getContractStatus().equals(ContractStatus.AGENCY_TRANSFER.getValue())) {
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_UPDATE_NOT_ALLOWED);
        }

        contract.setContractStatus(ContractStatus.AGENCY_TRANSFER_DONE.getValue());

        return contractRepo.save(contract);
    }

    @Override
    public Contract cancelContract(Integer id) throws AnvuiBaseException {
        Contract contract = contractRepo.findOne(id);
        if (contract == null)
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_NOT_EXISTED);

        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getUser();
            if (user == null)
                throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }

        contract.setContractStatus(ContractStatus.CANCELLED.getValue());

        return contractRepo.save(contract);
    }

    @Override
    public ContractWrapper confirmContract(Integer id) throws AnvuiBaseException {
        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getUser();
            if (user == null)
                throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }

        Contract contract = contractRepo.findOne(id);
        if (contract == null)
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_NOT_EXISTED);

        if (!user.getUserType().equals(UserType.ADMIN.getValue())) {
            if (contract.getCreatedUserId() != user.getId())
                throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        contract.setConfirm(true);
        contractRepo.save(contract);

        ContractWrapper res = new ContractWrapper(contract);
        return res;
    }

    @Override
    public List<ContractWrapper> getNotExportedContract(Boolean includeExported, Long fromDate, Long toDate)
            throws AnvuiBaseException {
        List<InsurancePackage> packList = packageRepo.findAllByProductIdAndActive(1, true);
        Map<Integer, InsurancePackage> packMap = new HashMap<>();
        for (InsurancePackage pack : packList) {
            packMap.put(pack.getId(), pack);
        }

        List<Integer> exportStatus = new ArrayList<>();
        exportStatus.add(ExportStatus.NOT_EXPORTED.getValue());
        exportStatus.add(ExportStatus.UPDATED.getValue());
        if (includeExported) {
            exportStatus.add(ExportStatus.EXPORTED.getValue());
            exportStatus.add(ExportStatus.UPDATE_EXPORTED.getValue());
        }

        Date dateFrom = new Date(fromDate);
        Date dateTo = new Date(toDate);

        List<Integer> completedStatus = new ArrayList<>(ContractStatus.COMPLETED_SET);
        List<ContractWrapper> contracts = contractRepo.getContractWithAgencyUser(completedStatus, null, exportStatus,
                dateFrom, dateTo, 1);
        for (ContractWrapper contract : contracts) {
            contract.setInsurancePackage(packMap.get(contract.getContract().getPackageId()));
        }

        return contracts;
    }

    @Override
    public void exportContracts(List<Integer> ids) throws AnvuiBaseException {
        List<Contract> contracts = contractRepo.findAll(ids);

        for (Contract contract : contracts) {
            switch ((int) contract.getExportStatus()) {
            // NOT_EXPORTED
            case 0:
                contract.setExportStatus((byte) ExportStatus.EXPORTED.getValue());
                break;
            case 2:
                contract.setExportStatus((byte) ExportStatus.UPDATE_EXPORTED.getValue());
                break;
            default:
                break;
            }
        }

        contractRepo.save(contracts);
    }

    @Override
    public List<RejectedContract> listRejectedContract(Long fromDate, Long toDate) throws AnvuiBaseException {
        Date dateFrom = new Date(fromDate);
        Date dateTo = new Date(toDate);

        List<RejectedContract> rejectedList;
        try {
            rejectedList = contractRepo.listRejectedReason(dateFrom, dateTo);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }

        return rejectedList;
    }

    @Override
    public Contract cancelCodContract(Integer id) throws AnvuiBaseException {
        Contract contract = contractRepo.findOne(id);

        if (contract == null)
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_NOT_EXISTED);

        if (contract.getContractStatus() != ContractStatus.COD.getValue())
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_STATUS_INVALID);

        contract.setContractStatus(ContractStatus.CREATED.getValue());
        contract.getInheritCustomer().setTag(CustomerTag.PENDING.getValue());
        contract.getPartner().setTag(CustomerTag.PENDING.getValue());

        contractRepo.save(contract);

        return contract;
    }

    @Override
    public List<DailyReport> getDailyReportForAgency(Date fromDate, Date toDate) throws AnvuiBaseException {
        User user;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            user = userDetails.getUser();

            if (user == null)
                throw new Exception();
        } catch (Exception e) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }

        Collection<Integer> userIds;
        if (user.getUserType().equals(UserType.AGENCY.getValue())) {
            userIds = uClosureRepo.findDescendanceIdFromId(user.getId());
        } else if (user.getUserType().equals(UserType.ADMIN.getValue())) {
            userIds = uClosureRepo.findRootUsers();
            Set<Integer> rootIds = new HashSet<>();
            for (Integer id : userIds) {
                rootIds.addAll(uClosureRepo.findDescendanceIdFromId(id));
            }
            userIds.addAll(rootIds);
        } else {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }

        if (fromDate == null) {
            fromDate = DateTimeHelper.getFirstDayOfMonths(new Date(), TimeZone.getTimeZone("GMT+7:00"));
        }
        if (toDate == null) {
            toDate = DateTimeHelper.addDay(fromDate, DateTimeHelper.MONTH, 1, TimeZone.getTimeZone("GMT+7:00"));
        }
        List<Date> date = DateTimeHelper.getAllDateBetween(fromDate, toDate);

        List<DailyReport> report = new LinkedList<>();

        try {
            for (int i = 0; i < date.size() - 1; i++) {
                DailyReport rep = contractRepo.getContractReport(date.get(i), date.get(i + 1), userIds);
                report.add(rep);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            throw new AnvuiBaseException(ErrorInfo.INTERNAL_SERVER_ERROR);
        }

        return report;
    }

    public static Contract setDateForContract(Contract contract, Date startDate) {
        contract.setCreatedDate(DateTimeHelper.getFirstTimeOfDay(startDate));
        contract.setAppliedDate(DateTimeHelper.addDay(startDate, "month", 1));
        contract.setStartDate(DateTimeHelper.addDay(contract.getCreatedDate(), "year", 3));
        contract.setEndDate(DateTimeHelper.addDay(contract.getStartDate(), "year", 5));

        return contract;
    }
}
