package vn.anvui.bsn.beans.banner;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import vn.anvui.bsn.beans.authentication.AuthenticationHelperInterface;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.banner.BannerRequest;
import vn.anvui.dt.entity.Banner;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.enumeration.BannerType;
import vn.anvui.dt.enumeration.UserType;
import vn.anvui.dt.repos.banner.BannerRepository;

@Service
public class BannerServiceImpl implements BannerService {

    @Autowired
    BannerRepository bannerRepo;
    
    @Autowired
    AuthenticationHelperInterface authHelper;
    
    @Override
    public List<Banner> listBanner(boolean deleted, BannerType type) {
        User authUser = null;
        try {
            authUser = authHelper.getUserFromAuthentication();
        } catch (Exception e) {
            // do nothing
        }
        
        if (deleted && (authUser == null || !authUser.getUserType().equals(UserType.ADMIN.getValue()))) {
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        }
        
        return bannerRepo.findAllByActiveAndBannerTypeOrderByIdDesc(!deleted, type.getValue());
    }

    @Override
    public Banner createBanner(BannerRequest req, BannerType type) {
        Banner banner = req.createBannerFromRequest(type);
        
        return bannerRepo.save(banner);
    }
    
    @Override
    public Banner updateBanner(Integer id, BannerRequest req, BannerType type) {
        Banner banner = bannerRepo.findOne(id);
        if (banner == null || !banner.getBannerType().equals(type.getValue()))
            throw new AnvuiBaseException(ErrorInfo.DATA_NOT_EXIST);
        
        banner = req.updateBannerFromRequest(banner);
        return bannerRepo.save(banner);
    }

    @Override
    public void updateActiveBanner(Integer id, boolean active, BannerType type) {
        
        Banner banner = bannerRepo.findOne(id);
        if (banner == null || !banner.getBannerType().equals(BannerType.BANNER.getValue()))
            throw new AnvuiBaseException(ErrorInfo.DATA_NOT_EXIST);
        
        banner.setActive(active);

        bannerRepo.save(banner);
    }

}
