package vn.anvui.bsn.beans.notification;

import java.util.List;

import vn.anvui.bsn.dto.notification.NotificationDto;
import vn.anvui.dt.entity.Notification;
import vn.anvui.dt.enumeration.DeviceType;

public interface NotificationService {

    void markAsRead(List<Integer> notiId);

    void createAndSendNotification(NotificationDto dto);

    List<Notification> listNotification(Integer page, Integer count, boolean isAgency);

    void addUserToken(String token, Integer userId, DeviceType type);

    void subscribeSystemTopic(List<String> tokens, Integer userId);
}
