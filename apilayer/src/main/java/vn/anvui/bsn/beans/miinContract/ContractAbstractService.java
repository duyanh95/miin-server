package vn.anvui.bsn.beans.miinContract;

import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;

import vn.anvui.api.amqp.MessageProducer;
import vn.anvui.bsn.beans.mailing.MailingService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.miinContract.MiinContractRequest;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.ContractEffectiveDate;
import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.entity.InsurancePackage;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.enumeration.CustomerTag;
import vn.anvui.dt.enumeration.PeriodType;
import vn.anvui.dt.enumeration.TransactionType;
import vn.anvui.dt.repos.contract.ContractEffectiveDateRepository;
import vn.anvui.dt.repos.contract.ContractRepository;
import vn.anvui.dt.utils.DateTimeHelper;

public abstract class ContractAbstractService<T extends MiinContractRequest> {
    private final Logger log = Logger.getLogger("ContractAbstractService");
    public final String BCC = "chamsockhachhang@epti.vn";
    
    @Autowired
    protected MessageProducer msgProducer;
    
    @Autowired
    protected MailingService mailService;
    
    @Autowired
    protected ContractRepository contractRepo;
    
    @Autowired
    ContractEffectiveDateRepository dateRepo;
    
    protected Contract updateContractPricing(Contract contract, InsurancePackage pkg, User createdUser) {
        contract.setFee(pkg.getFee());
        contract.setIncome(pkg.getFee() * (1 - createdUser.getCommission()));
        contract.setCommission(contract.getFee() - contract.getIncome());
        
        return contract;
    }
    
    protected ContractStatus getStatusFromTxnType(Integer txnType, User user) {
        ContractStatus status;
        if (txnType == null || txnType.equals(TransactionType.ONLINE_TXN.getValue())) {
            status = ContractStatus.AGENCY_CREATED;
        } else if (txnType.equals(TransactionType.COD_TXN.getValue())) {
            if (!user.isCodAllowed()) {
                throw new AnvuiBaseException(ErrorInfo.CONTRACT_COD_CREATION_NOT_ALLOWED);
            }
            status = ContractStatus.AGENCY_COD_DONE;
        } else if (txnType.equals(TransactionType.ACCOUNT_TRANSFER.getValue())) {
            if (!user.isCodAllowed()) {
                throw new AnvuiBaseException(ErrorInfo.CONTRACT_COD_CREATION_NOT_ALLOWED);
            }
            status = ContractStatus.AGENCY_TRANSFER;
        } else {
            throw new AnvuiBaseException(ErrorInfo.CONTRACT_TRANSACTION_TYPE_INVALID);
        }
        
        return status;
    }
    
    public abstract void sendStatusNotify(Contract contract, boolean success);
    
    protected boolean packageIdIsValid(InsurancePackage pkg, Integer productId) {
        return pkg.getProductId().equals(productId);
    }
    
    
    public Contract updateContractAfterTransaction(Contract contract) {
        if (ContractStatus.COMPLETED_SET.contains(contract.getContractStatus())) {
            contract = updateEffectiveDate(contract);
        } else if (contract.getContractStatus().equals(ContractStatus.CREATED.getValue())) {
            contract.setContractStatus(ContractStatus.PAID.getValue());
        } else if (contract.getContractStatus().equals(ContractStatus.AGENCY_CREATED.getValue())) {
            contract.setContractStatus(ContractStatus.AGENCY_PAID.getValue());
        } else if (contract.getContractStatus().equals(ContractStatus.CUSTOMER_CREATED.getValue())) {
            contract.setContractStatus(ContractStatus.CUSTOMER_PAID.getValue());
        }
        
        if (contract.getContractCode().length() < 15) {
            createContractCode(null);
        }
        contract.getInheritCustomer().setTag(CustomerTag.ONLINE.getValue());
        
        return contract;
    }
    
    protected Contract updateEffectiveDate(Contract contract) {
        
        if (contract.getPeriodType() == null)
            return contract;
        
        Date newStartDate;
        if (contract.getEndDate() == null || contract.getEndDate().getTime() < System.currentTimeMillis()) {
            newStartDate = DateTimeHelper.addDay(
                    DateTimeHelper.getFirstTimeOfDay(new Date(), TimeZone.getTimeZone("GMT+7:00")), 
                    DateTimeHelper.DAY_OF_YEAR, 1, TimeZone.getTimeZone("GMT+7:00"));
            
            contract.setStartDate(newStartDate);
        } else {
            newStartDate = contract.getEndDate();
        }
        
        log.info(newStartDate.toString());
        
        String typeOfTime;
        if (contract.getPeriodType().equals(PeriodType.WEEKLY.getValue())) {
            typeOfTime = DateTimeHelper.WEEK;
        } else if (contract.getPeriodType().equals(PeriodType.MONTHLY.getValue())) {
            typeOfTime = DateTimeHelper.MONTH;
        } else if (contract.getPeriodType().equals(PeriodType.YEARLY.getValue())) {
            typeOfTime = DateTimeHelper.YEAR;
        } else {
            return contract;
        }
        
        Date endDate = DateTimeHelper.addDay(newStartDate, typeOfTime, 1, TimeZone.getTimeZone("GMT+7:00"));
        
        contract.setEndDate(endDate);
        
        createEffectiveDate(contract);
        
        return contract;
    }
    
    private ContractEffectiveDate createEffectiveDate(Contract contract) {
        ContractEffectiveDate effectiveDate = new ContractEffectiveDate(contract);
        
        return dateRepo.save(effectiveDate);
    }
    
    public void updatePhase(Contract contract) {
        
    };
    
    protected abstract String createContractCode(String agencyCode);
    
    protected abstract Customer createCustomer(T req);
    
    protected abstract Contract createContract(T req, User user, Customer customer,
            ContractStatus status);
}
