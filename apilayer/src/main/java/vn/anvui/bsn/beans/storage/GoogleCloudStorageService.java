package vn.anvui.bsn.beans.storage;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.file.FileObject;
import vn.anvui.dt.common.AuthenticationDetails;
import vn.anvui.dt.entity.StorageResources;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.enumeration.UserType;
import vn.anvui.dt.repos.storage.StorageRepository;

@Service
public class GoogleCloudStorageService implements StorageService {
    private Logger log = Logger.getLogger(GoogleCloudStorageService.class.getName());
    
    Storage storage;
    
    @Value("${gcp.storage.bucket}")
    String bucket;
    
    @Value("${server.port}")
    String port;
    
    @Autowired
    StorageRepository storageRepos;
    
    @PostConstruct
    void init() {
        storage = StorageOptions.getDefaultInstance().getService();
    }

    @Override
    public StorageResources uploadFile(byte[] file, String fileName) {
        User loggedUser = null;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            loggedUser = userDetails.getUser();
        } catch (Exception e) {
            log.warning("anonymous user");
        }
        
        Integer userId = loggedUser != null ? loggedUser.getId() : null;
        
        String contentType;
        switch (fileName.substring(fileName.lastIndexOf("."))) {
            case ".png":
                contentType = "image/png";
                break;
            
            case ".jpeg":
            case ".jpg":
                contentType = "image/jpeg";
                break;
                
            case ".pdf":
                contentType = "application/pdf";
                break;
                
            default:
                throw new AnvuiBaseException(ErrorInfo.BAD_REQUEST);
        }
        
        BlobId blobId = BlobId.of(bucket, fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType(contentType).build();
        
        storage.create(blobInfo, file);
        
        return storageRepos.save(new StorageResources(fileName, userId, fileName));
    }

    @Override
    public FileObject getFile(String fileName) {
        User loggedUser = null;
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            loggedUser = userDetails.getUser();
        } catch (Exception e) {
            log.log(Level.INFO, e.getMessage(), e);
            log.warning("anonymous user");
        }
        
        log.info(fileName);
        
        StorageResources resource = storageRepos.findOneByResourceUrl(fileName);
        
        if (resource == null) {
            throw new AnvuiBaseException(ErrorInfo.DATA_NOT_EXIST);
        }
        
        log.info(resource.getUserId().toString());
        
        if (resource.getUserId() != null && (loggedUser == null ||
                !(loggedUser.getUserType().equals(UserType.ADMIN.getValue()) 
                    || loggedUser.getId().equals(resource.getUserId()))))
            throw new AnvuiBaseException(ErrorInfo.ACCESS_DENIED);
        
        Blob blob = storage.get(BlobId.of(bucket, resource.getStorageUrl()));
        
        return new FileObject(blob.getContent(), blob.getName(), blob.getContentType());
    }

}
