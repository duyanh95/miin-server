package vn.anvui.bsn.beans.miinContract.health;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.transaction.Transactional;

import org.apache.commons.lang3.text.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import vn.anvui.bsn.beans.miinContract.ContractAbstractService;
import vn.anvui.bsn.beans.miinContract.general.MiinGeneralContractService;
import vn.anvui.bsn.beans.product.ProductService;
import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.bsn.common.ErrorInfo;
import vn.anvui.bsn.dto.contract.ContractNotifyData;
import vn.anvui.bsn.dto.miinContract.HealthContractRequest;
import vn.anvui.dt.common.AuthenticationDetails;
import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.entity.InsurancePackage;
import vn.anvui.dt.entity.PackagePeriod;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.enumeration.ContractStatus;
import vn.anvui.dt.enumeration.ProductType;
import vn.anvui.dt.model.ContractWrapper;
import vn.anvui.dt.repos.contract.ContractRepository;
import vn.anvui.dt.repos.customer.CustomerRepository;
import vn.anvui.dt.repos.insurancePackage.InsurancePackageRepository;
import vn.anvui.dt.repos.insurancePackage.PackagePeriodRepository;

@Service
@PropertySource("classpath:health_notification.properties")
public class HealthContractServiceImpl extends ContractAbstractService<HealthContractRequest>
        implements HealthContractService {
    final static Logger log = Logger.getLogger(HealthContractServiceImpl.class.getSimpleName());

    final static Integer PRODUCT_ID = ProductType.HEALTH.getValue();
    
    @Autowired
    HealthContractNotification healthNoti;

    @Autowired
    CustomerRepository customerRepo;

    @Autowired
    ContractRepository contractRepo;

    @Autowired
    ProductService productService;

    @Autowired
    InsurancePackageRepository pkgRepo;
    
    @Autowired
    PackagePeriodRepository periodRepo;
    
    @Autowired
    MiinGeneralContractService generalContractService;
    
    @Override
    public Contract createCustomerContract(HealthContractRequest req) throws AnvuiBaseException {
        User user = null;
        try {
            AuthenticationDetails userDetail = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();

            user = userDetail.getAuthenticatedUser();

        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }

        Customer customer = createCustomer(req);

        Contract contract = createContract(req, user, customer, ContractStatus.CUSTOMER_CREATED);
        
        return contract;
    }

    @Override
    protected Customer createCustomer(HealthContractRequest req) {
        Customer customer = new Customer();

        customer.setCustomerName(WordUtils.capitalizeFully(req.getCustomerName()));
        customer.setPhoneNumber(req.getPhoneNumber());
        customer.setEmail(req.getEmail());
        customer.setCitizenId(req.getCitizenId());
        customer.setDob(req.getDob());
        customer.setCreatedDate(new Date());

        return customerRepo.save(customer);
    }

    @Override
    @Transactional
    protected Contract createContract(HealthContractRequest req, User user, Customer customer, ContractStatus status) {

        Contract contract = new Contract();
        contract.setCreatedTime(new Date());
        contract.setCreatedUserId(user.getId());
        
        InsurancePackage pkg = pkgRepo.findOne(req.getPackageId());
        
        if (!packageIdIsValid(pkg, PRODUCT_ID)) {
            throw new AnvuiBaseException(ErrorInfo.PACKAGE_INVALID);
        }
        
        PackagePeriod period = periodRepo.findFirstByPackageIdAndPeriodType(pkg.getId(), req.getPeriodType());
        if (period == null || !period.getPackageId().equals(pkg.getId())) {
            throw new AnvuiBaseException(ErrorInfo.PACKAGE_INVALID);
        }
        contract.setPackageId(pkg.getId());
        contract.setPeriodType(req.getPeriodType());
        contract.setPhase(0);
        contract = calculateFee(contract, period, user);
        
        contract.setInheritCustomer(customer);
        contract.setContractStatus(status.getValue());
        contract.setProductId(PRODUCT_ID);
        
        contract.setContractCode(createContractCode(user.getAgencyCode()));

        contract = contractRepo.save(contract);
        log.info("contractId: " + contract.getId());
        
        customer.setContractId(contract.getId());
        customerRepo.save(customer);
        
        contract = updateEffectiveDate(contract);
        return contract;
    }

    protected Contract calculateFee(Contract contract, PackagePeriod period, User user) {
        Double fee = period.getFee();
        Double comission = user.getCommission() == null? 0 : user.getCommission();
        
        contract.setFee(fee);
        contract.setCommission(fee * comission);
        contract.setIncome(fee - contract.getIncome());
        
        return contract;
    }

    @Override
    public Contract createAgencyContract(HealthContractRequest req) throws AnvuiBaseException {
        User user = null;
        try {
            AuthenticationDetails userDetail = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();

            user = userDetail.getAuthenticatedUser();

        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }
        
        Customer customer = createCustomer(req);
        log.info("customerId: " + customer.getId());
        
        ContractStatus status = getStatusFromTxnType(req.getTxnType(), user);
        
        Contract contract = createContract(req, user, customer, status);
        
        customer.setContractId(contract.getId());
        customerRepo.save(customer);
        
        return contract;
    }

    @Override
    protected String createContractCode(String agencyCode) {
        Integer number = productService.getProductContractNumber(PRODUCT_ID);
        
        if (!StringUtils.isEmpty(agencyCode)) {
            agencyCode = "-" + agencyCode;
        } else {
            agencyCode = "";
        }

        return String.format("MIIN-SK%s%011d", agencyCode, number);
    }

    @Override
    public void sendSuccessMessage(ContractWrapper wrapper) throws AnvuiBaseException {
        healthNoti.sendSuccessMessage(wrapper);
    }

    @Override
    public void sendPhaseNotification(ContractNotifyData wrapper) {
        healthNoti.sendPhaseNotification(wrapper);
    }

    @Override
    public void sendStatusNotify(Contract contract, boolean success) {
        healthNoti.sendStatusNotification(contract, success);
    }

}
