package vn.anvui.bsn.beans.contactLog;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.anvui.bsn.common.AnvuiBaseException;
import vn.anvui.dt.entity.ContactLog;
import vn.anvui.dt.repos.contactLog.ContactLogRepository;

@Service
public class ContactLogServiceImpl implements ContactLogService {
    public final Logger log = Logger.getLogger(ContactLogServiceImpl.class.getSimpleName());
    
    @Autowired
    ContactLogRepository logRepo;

    @Override
    public List<ContactLog> getContactLog(String phoneNumber) throws AnvuiBaseException {
        return logRepo.findFirst10ByPhoneNumberOrderByIdDesc(phoneNumber);
    }
}
