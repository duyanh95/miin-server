package vn.anvui.bsn.common;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ErrorInfo {

    private static final Logger logger = LoggerFactory.getLogger(ErrorInfo.class);

    private static Properties properties;

    private int httpStatus = 500;

    static {
        try {
            properties = new Properties();
            InputStream is = ErrorInfo.class.getResourceAsStream("/error_info.properties");
            properties.load(is);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    private int code;
    private List<String> messages = new ArrayList<>();
    private Map<String, String> info = new HashMap<>();

    public static ErrorInfo newInstance(int code, String message) {
        
        return new ErrorInfo(code, message);
    }

    public ErrorInfo(int code, int status, String... messages) {
        this.code = code;
        this.httpStatus = status;
        for (String message : messages) {
            this.messages.add(message);
        }
    }

    public ErrorInfo(int code, String... messages) {
        this.code = code;
        for (String message : messages) {
            this.messages.add(message);
        }
    }
    
    public int getCode() {
        return code;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public String getMessage() {
        return messages.size() > 0 ? messages.get(0) : null;
    }
    
    public Map<String, String> getData() {
        return this.info;
    }
    
    public ErrorInfo addInfo(String key, String value) {
        this.info.put(key, value);
        return this;
    }


    public static final int UNKNOWN_ERROR_CODE = 10000;

    public static final ErrorInfo INTERNAL_SERVER_ERROR = new ErrorInfo(1000, properties.getProperty("server.error"));
    public static final ErrorInfo LOGIN_FAILED = new ErrorInfo(1001, 403, properties.getProperty("login.error"));
    public static final ErrorInfo BAD_REQUEST = new ErrorInfo(1003, 400, properties.getProperty("request.error"));
    public static final ErrorInfo STATE_INVALID = new ErrorInfo(1004, properties.getProperty("state.invalid"));
    public static final ErrorInfo MULTIPLE_ACCOUNT_LOGIN_ERROR = new ErrorInfo(1005,
            properties.getProperty("multiple.account.login.error"));
    public static final ErrorInfo ACCOUNT_NOT_ACTIVE_LOGIN_FAILED = new ErrorInfo(1006,
            properties.getProperty("account.not.active.login.error"));
    public static final ErrorInfo ACCOUNT_DE_ACTIVE_LOGIN_FAILED = new ErrorInfo(1007,
            properties.getProperty("account.de.active.login.error"));
    public static final ErrorInfo INVALID_LOGIN_ROLE = new ErrorInfo(1008,
            properties.getProperty("invalid.login.role"));
    public static final ErrorInfo UNABLE_TO_SAVE_DATA = new ErrorInfo(1009, "Unable to save data");
    public static final ErrorInfo EMPTY = new ErrorInfo(1010, 200, "Data empty");
    public static final ErrorInfo ACCESS_DENIED = new ErrorInfo(1011, 403, "Access denied");
    public static final ErrorInfo DATA_NOT_EXIST = new ErrorInfo(1012, 409, "data not existed");
    public static final ErrorInfo DATE_FORMAT_INVALID = new ErrorInfo(1013, 400, "Date format invalid");
    public static final ErrorInfo PARAMETER_MISSING = new ErrorInfo(1014, 400, "Request parameter missing");
    public static final ErrorInfo TYPE_MISMATCHED = new ErrorInfo(1015, 400, "Parameter type mismatched");

    // Customer
    public static final ErrorInfo CUSTOMER_NAME_EMPTY = new ErrorInfo(2001, 400, "Name empty");
    public static final ErrorInfo CUSTOMER_EMAIL_EMPTY = new ErrorInfo(2002, 400, "Email empty");
    public static final ErrorInfo CUSTOMER_CITIZEN_ID_EMPTY = new ErrorInfo(2003, 400, "CitizenId empty");
    public static final ErrorInfo PACKAGE_INVALID = new ErrorInfo(2004, 400, "Package invalid");
    public static final ErrorInfo CUSTOMER_SAME_GENDER = new ErrorInfo(2005, 400, "Customer same gender");
    public static final ErrorInfo CONTRACT_EXISTED = new ErrorInfo(2006, 409, "Contract existed");
    public static final ErrorInfo CUSTOMER_DATE_EMPTY = new ErrorInfo(2007, 400, "date empty");
    public static final ErrorInfo CUSTOMER_NULL = new ErrorInfo(2008, 409, "customer not existed");
    public static final ErrorInfo CONTRACT_NULL = new ErrorInfo(2009, 409, "Contract not existed");
    public static final ErrorInfo CONTRACT_UPDATE_NOT_ALLOWED = new ErrorInfo(2010, 403, "Contract cannot be updated");
    public static final ErrorInfo CONTRACT_NOT_EXISTED = new ErrorInfo(2011, 400, "Contract not existed");
    public static final ErrorInfo CONTRACT_STATUS_INVALID = new ErrorInfo(2012, 400, "Contract status invalid");
    public static final ErrorInfo CUSTOMER_GENDER_MISSIING = new ErrorInfo(2013, 400, "Gender missing");
    public static final ErrorInfo CONTRACT_COMPLETED = new ErrorInfo(2014, 409, "Contract is already completed");
    public static final ErrorInfo CONTRACT_TAG_INVALID = new ErrorInfo(2015, 400, "Type is invalid");
    public static final ErrorInfo CONTRACT_EXPORT_NOT_ACCEPTED = new ErrorInfo(2016, 400, "Contract export failed");
    public static final ErrorInfo CONTRACT_DATE_INVALID = new ErrorInfo(2017, 400, "Date invalid");
    public static final ErrorInfo CONTRACT_TRANSACTION_TYPE_INVALID = new ErrorInfo(2018, 400, "txnType invalid");
    public static final ErrorInfo CONTRACT_COD_CREATION_NOT_ALLOWED = new ErrorInfo(2019, 400, "cod contract not allowed");
    public static final ErrorInfo DOB_INVALID = new ErrorInfo(2020, 400, "date of birth invalid");
    
    //Transaction
    public static final ErrorInfo TRANSACTION_FAIL = new ErrorInfo(3000, "Transaction fail");
    public static final ErrorInfo TRANSACTION_NULL = new ErrorInfo(3001, 400, "Transaction not existed");
    public static final ErrorInfo TRANSACTION_BAD_REQUEST = new ErrorInfo(3002, 400, "Transaction data invalid");
    public static final ErrorInfo TRANSACTION_EXPIRED = new ErrorInfo(3003, 440, "Transaction expired");
    public static final ErrorInfo TRANSACTION_DATA_MISSING = new ErrorInfo(3004, 400, "Transaction data missing");
    public static final ErrorInfo TRANSACTION_ALREADY_COMPLETE = new ErrorInfo(3005, 409, 
            "Transaction already completed");
    public static final ErrorInfo TRANSACTION_CONTRACT_ALREADY_COMPLETED = new ErrorInfo(3006, 409,
            "Other payment type transaction has already been completed");
    public static final ErrorInfo TRANSACTION_PAYMENT_TYPE_INVALID = new ErrorInfo(3007, 400, "txnType invalid");
    
    //Mailing service
    public static final ErrorInfo MAIL_PDF_CREATED_FAIL = new ErrorInfo(4000, "Pdf creation fail");
    public static final ErrorInfo MAIL_SEND_FAIL = new ErrorInfo(4001, "Email send fail");
    public static final ErrorInfo MAIL_CUSTOMER_INFO_MISSING = new ErrorInfo(4002, "Information missing");
    
    //Tracking
    public static final ErrorInfo TRACKING_FIELD_NOT_EXISTED = new ErrorInfo(5000, 400, "field not existed");
    
    //User
    public static final ErrorInfo USER_PASSWORD_NOT_MATCHED = new ErrorInfo(6000, 400, "passwords not matched");
    public static final ErrorInfo USER_NAME_NULL = new ErrorInfo(6001, 400, "user name null or empty");
    public static final ErrorInfo USER_NOT_EXISTED = new ErrorInfo(6002, 400, "user not existed");
    public static final ErrorInfo AGENCY_CODE_IS_MISSING = new ErrorInfo(6003, 400, "agency code missing");
    public static final ErrorInfo USER_PARENT_NOT_EXISTED = new ErrorInfo(6004, 400, "user parent not existed");
    public static final ErrorInfo AGENCY_CODE_EXISTED = new ErrorInfo(6005, 400, "agency code already existed");
    public static final ErrorInfo USER_NAME_EXISTED = new ErrorInfo(6006, 400, "user name existed");
    public static final ErrorInfo USER_COMMISSION_INVALID = new ErrorInfo(6007, 400, "commission value invalid");
    public static final ErrorInfo USER_INFO_MISSING = new ErrorInfo(6008, 400, "user info missing");
    public static final ErrorInfo PHONE_NUMBER_EXISTED = new ErrorInfo(6009, 409, "phone number existed");
    public static final ErrorInfo EMAIL_EXISTED = new ErrorInfo(6010, 409, "email existed");
    
    //Role
    public static final ErrorInfo ROLE_PRIVILEGES_NULL = new ErrorInfo(7000, 400, "privileges not found");
    public static final ErrorInfo ROLE_NULL = new ErrorInfo(7001, 400, "role not found");
    
    //File
    public static final ErrorInfo FILE_NOT_FOUND = new ErrorInfo(8000, 404, "File not found");
    
    //Claim
    public static final ErrorInfo CLAIM_DOCS_MISSING = new ErrorInfo(9000, 400, "Claim doc missing");
}
