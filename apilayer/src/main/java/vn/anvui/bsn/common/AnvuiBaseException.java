package vn.anvui.bsn.common;

public class AnvuiBaseException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private ErrorInfo error;
	
	public AnvuiBaseException(Throwable cause) {
		super(cause);
		this.setError(ErrorInfo.INTERNAL_SERVER_ERROR);
	}
	
	public AnvuiBaseException(ErrorInfo error) {
		this.setError(error);
	}

	public ErrorInfo getError() {
		return error;
	}

	public void setError(ErrorInfo error) {
		this.error = error;
	}
}
