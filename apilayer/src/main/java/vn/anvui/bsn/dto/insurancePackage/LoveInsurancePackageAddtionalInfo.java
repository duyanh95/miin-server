package vn.anvui.bsn.dto.insurancePackage;

public class LoveInsurancePackageAddtionalInfo {
    private String waitTime;
    
    private String endTime;

    public LoveInsurancePackageAddtionalInfo() {

    }
    
    public LoveInsurancePackageAddtionalInfo(String waitTime, String endTime) {
        this.waitTime = waitTime;
        this.endTime = endTime;
    }

    public String getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(String waitTime) {
        this.waitTime = waitTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
