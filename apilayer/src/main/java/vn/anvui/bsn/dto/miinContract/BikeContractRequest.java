package vn.anvui.bsn.dto.miinContract;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;

public class BikeContractRequest extends MiinContractRequest {
    @NotEmpty
    String registeredNumber;
    
    @NotEmpty
    String registeredAddress;
    
    @JsonFormat(pattern = "dd/MM/yyyy", timezone = "GMT+7:00")
    Date startDate;
    
    @NotNull
    Integer packageId;

    public String getRegisteredNumber() {
        return registeredNumber;
    }

    public String getRegisteredAddress() {
        return registeredAddress;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Integer getPackageId() {
        return packageId;
    }

}
