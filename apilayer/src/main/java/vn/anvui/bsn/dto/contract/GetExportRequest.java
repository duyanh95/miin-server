package vn.anvui.bsn.dto.contract;

import org.hibernate.validator.constraints.NotEmpty;

public class GetExportRequest {
    @NotEmpty
    private String type;
    
    private Boolean includeExported;
    
    private Long fromDate;
    
    private Long toDate;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getIncludeExported() {
        return includeExported;
    }

    public void setIncludeExported(Boolean includeExported) {
        this.includeExported = includeExported;
    }

    public Long getFromDate() {
        return fromDate;
    }

    public void setFromDate(Long fromDate) {
        this.fromDate = fromDate;
    }

    public Long getToDate() {
        return toDate;
    }

    public void setToDate(Long toDate) {
        this.toDate = toDate;
    }
}
