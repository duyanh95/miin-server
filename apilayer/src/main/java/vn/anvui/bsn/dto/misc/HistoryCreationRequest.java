package vn.anvui.bsn.dto.misc;

import javax.validation.constraints.NotNull;

import vn.anvui.dt.entity.History;
import vn.anvui.dt.entity.User;

public class HistoryCreationRequest {
    @NotNull
    Integer productId;
    
    Integer packageId;
    
    String inputData;
    
    public History createHistory(User user) {
        History hist = new History(user.getId(), this.productId);
        
        hist.setPackageId(this.packageId);
        hist.setInputData(this.inputData);
        
        return hist;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public String getInputData() {
        return inputData;
    }

    public void setInputData(String inputData) {
        this.inputData = inputData;
    }
    
    
}
