package vn.anvui.bsn.dto.transaction.epay;

import vn.anvui.bsn.dto.transaction.TransactionRequest;

public class EpayTransactionRequest extends TransactionRequest {
    private Integer paymentType;
    
    private String returnURL;

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public String getReturnURL() {
        return returnURL;
    }

    public void setReturnURL(String returnURL) {
        this.returnURL = returnURL;
    }
}
