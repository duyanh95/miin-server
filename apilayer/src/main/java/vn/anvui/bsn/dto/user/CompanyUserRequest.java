package vn.anvui.bsn.dto.user;

import javax.validation.constraints.NotNull;

public class CompanyUserRequest {

	@NotNull
	private Integer companyId;
	
	@NotNull
	private Integer userId;

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
