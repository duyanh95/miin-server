package vn.anvui.bsn.dto.notification;

import java.util.HashMap;
import java.util.Map;

public class NotificationDto {
    Integer notificationType;

    String title;
    
    String content;
    
    private Integer userId;
    
    Map<String, String> data = new HashMap<>();
    
    public Integer getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(Integer notificationType) {
        this.notificationType = notificationType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
