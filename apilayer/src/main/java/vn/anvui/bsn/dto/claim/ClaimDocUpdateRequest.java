package vn.anvui.bsn.dto.claim;

import javax.validation.constraints.NotNull;

public class ClaimDocUpdateRequest extends ClaimDocCreationRequest {
    @NotNull
    Boolean accepted;
    
    String error;
    
    String solution;
    
    String note;

    public Boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
