package vn.anvui.bsn.dto.miinContract;

import vn.anvui.dt.model.ContractWrapper;

public class PTIResponse {
    ContractWrapper responseData;
    
    String successMessage;

    public ContractWrapper getResponseData() {
        return responseData;
    }

    public void setResponseData(ContractWrapper responseData) {
        this.responseData = responseData;
    }

    public String getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }
    
}
