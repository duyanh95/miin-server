package vn.anvui.bsn.dto.miinContract;

import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;

import vn.anvui.bsn.dto.miinContract.validator.AgeBetween;

public class LoveContractRequest extends MiinContractRequest {
    @NotEmpty
    String citizenId;
    
    @NotNull
    @Min(1)
    @Max(2) 
    Integer gender;
    
    @NotNull
    @JsonFormat(pattern = "dd/MM/yyyy", timezone = "GMT+7:00")
    @AgeBetween(min = 16, max = 40)
    Date dob;
    
    @NotEmpty
    String partnerName;
    
    @NotEmpty
    String partnerCitizenId;

    @NotNull
    @Min(1)
    @Max(2)
    Integer partnerGender;

    @NotNull
    @JsonFormat(pattern = "dd/MM/yyyy", timezone = "GMT+7:00")
    @AgeBetween(min = 16, max = 40)
    Date partnerDob;

    @NotNull
    Integer packageId;

    String address;
    
    @JsonFormat(pattern = "dd/MM/yyyy", timezone = "GMT+7:00")
    Date startDate;

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getPartnerCitizenId() {
        return partnerCitizenId;
    }

    public void setPartnerCitizenId(String partnerCitizenId) {
        this.partnerCitizenId = partnerCitizenId;
    }

    public Integer getPartnerGender() {
        return partnerGender;
    }

    public void setPartnerGender(Integer partnerGender) {
        this.partnerGender = partnerGender;
    }

    public Date getPartnerDob() {
        return partnerDob;
    }

    public void setPartnerDob(Date partnerDob) {
        this.partnerDob = partnerDob;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}
