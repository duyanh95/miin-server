package vn.anvui.bsn.dto.miinContract.validator;

import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import vn.anvui.dt.utils.DateTimeHelper;

public class AgeBetweenValidator implements ConstraintValidator<AgeBetween, Date> {
    
    private int min;
    private int max;

    @Override
    public void initialize(AgeBetween annotation) {
        this.min = annotation.min();
        this.max = annotation.max();
    }

    @Override
    public boolean isValid(Date value, ConstraintValidatorContext context) {
        Date currentDate = new Date();
        
        Date lowerLimit = DateTimeHelper.addDay(currentDate, DateTimeHelper.YEAR, -max);
        Date upperLimit = DateTimeHelper.addDay(currentDate, DateTimeHelper.YEAR, -min);
        
        return value.before(upperLimit) && value.after(lowerLimit);
    }

}
