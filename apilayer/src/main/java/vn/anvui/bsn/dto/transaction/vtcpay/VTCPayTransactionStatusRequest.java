package vn.anvui.bsn.dto.transaction.vtcpay;

import vn.anvui.bsn.dto.transaction.TransactionStatusRequest;

public class VTCPayTransactionStatusRequest extends TransactionStatusRequest {
    private String data;
    
    private String signature;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
