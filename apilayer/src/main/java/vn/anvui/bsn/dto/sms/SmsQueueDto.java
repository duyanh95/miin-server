package vn.anvui.bsn.dto.sms;

import java.util.List;

public class SmsQueueDto {
    private List<String> receivers;
    
    private String content;

    public String getContent() {
        return content;
    }
    
    public SmsQueueDto() {
        
    }

    public SmsQueueDto(List<String> receivers, String content) {
        super();
        this.receivers = receivers;
        this.content = content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getReceivers() {
        return receivers;
    }

    public void setReceivers(List<String> receivers) {
        this.receivers = receivers;
    }
}
