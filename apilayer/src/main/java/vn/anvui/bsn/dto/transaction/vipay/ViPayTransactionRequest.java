package vn.anvui.bsn.dto.transaction.vipay;

import vn.anvui.bsn.dto.transaction.TransactionRequest;

public class ViPayTransactionRequest extends TransactionRequest {
    private String ip;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
