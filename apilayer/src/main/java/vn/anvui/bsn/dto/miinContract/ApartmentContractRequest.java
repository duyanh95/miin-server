package vn.anvui.bsn.dto.miinContract;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class ApartmentContractRequest extends MiinContractRequest {
    @NotNull
    @Min(500000000)
    Double value;
    
    @NotNull
    @Min(1)
    @Max(5)
    Integer duration;
    
    @NotEmpty
    String address;
    
    @NotEmpty
    String buyerName;
    
    @NotEmpty
    String buyerPhone;

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getBuyerPhone() {
        return buyerPhone;
    }

    public void setBuyerPhone(String buyerPhone) {
        this.buyerPhone = buyerPhone;
    }
}
