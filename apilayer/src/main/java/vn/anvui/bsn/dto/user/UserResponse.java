package vn.anvui.bsn.dto.user;

import vn.anvui.dt.entity.User;

public class UserResponse {

	private User user;
	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
