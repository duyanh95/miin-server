package vn.anvui.bsn.dto.claim;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class ClaimDocCreationRequest {
    @NotNull
    Integer claimDocId;
    
    @NotEmpty
    List<String> resourceURL;
    
    public Integer getClaimDocId() {
        return claimDocId;
    }

    public void setClaimDocId(Integer claimDocId) {
        this.claimDocId = claimDocId;
    }

    public List<String> getResourceURL() {
        return resourceURL;
    }

    public void setResourceURL(List<String> resourceURL) {
        this.resourceURL = resourceURL;
    }
}
