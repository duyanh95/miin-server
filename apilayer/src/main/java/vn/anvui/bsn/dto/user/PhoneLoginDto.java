package vn.anvui.bsn.dto.user;

import vn.anvui.bsn.dto.sms.SmsQueueDto;
import vn.anvui.dt.entity.User;

public class PhoneLoginDto {
    private User user;
    
    private SmsQueueDto sms;

    public PhoneLoginDto(User user, SmsQueueDto sms) {
        this.user = user;
        this.sms = sms;
    }
    
    public User getUser() {
        return user;
    }

    public SmsQueueDto getSms() {
        return sms;
    }

}
