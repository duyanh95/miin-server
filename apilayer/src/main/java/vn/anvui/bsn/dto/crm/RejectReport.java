package vn.anvui.bsn.dto.crm;

public class RejectReport {
    private String reason;
    
    private Integer count;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
