package vn.anvui.bsn.dto.user;

import java.util.Date;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonFormat;

import vn.anvui.dt.entity.User;
import vn.anvui.dt.enumeration.UserStatus;

public class UserRequest {
    @NotEmpty
    private String userName;

    @NotEmpty
    private String password;

    @NotEmpty
    private String passwordConfirm;
    
    private String fullName;
    
    private String phoneNumber;

    @Email
    private String email;
    
    @JsonFormat(pattern = "dd/MM/yyyy", timezone = "GMT+7:00")
    private Date dob;

    private String avatar;

    private String address;

    private String province;

    private String agencyCode;
    
    private String roleName;
    
    private String citizenId;
    
    private String citizenIdImageFront;
    
    private String citizenIdImageBack;
    
    private String bankAccount;
    
    private String bankInfo;

    private Integer parentId;
    
    private Double commission;
    
    private Boolean codAllowed;
    
    private Boolean confirm;
    
    // fill optional information
    public User createUser() {
        User user = new User();
        user.setBankAccount(this.bankAccount);
        user.setBankInfo(this.bankInfo);
        user.setAddress(this.address);
        user.setProvince(this.province);
        user.setCitizenId(this.citizenId);
        user.setStatus(UserStatus.ACTIVE.getValue());
        user.setDob(getDob());
        
        return user;
    }
    
    public User updateUser(User user) {
        if (!StringUtils.isEmpty(this.avatar)) {
            user.setAvatar(this.avatar);
        }
        
        if (!StringUtils.isEmpty(this.fullName)) {
            user.setFullName(this.fullName);
        }
        
        if (!StringUtils.isEmpty(this.citizenId)) {
            user.setCitizenId(this.citizenId);
        }
        
        if (!StringUtils.isEmpty(this.citizenIdImageFront)) {
            user.setCitizenIdImageFront(this.citizenIdImageFront);
        }
        
        if (!StringUtils.isEmpty(this.citizenIdImageBack)) {
            user.setCitizenIdImageBack(this.citizenIdImageBack);
        }
        
        if (!StringUtils.isEmpty(this.bankAccount)) {
            user.setBankAccount(this.bankAccount);
        }
        
        if (!StringUtils.isEmpty(this.bankInfo)) {
            user.setBankInfo(this.bankInfo);
        }
        
        if (!StringUtils.isEmpty(this.email)) {
            user.setEmail(this.email);
        }
        
        if (!StringUtils.isEmpty(this.address)) {
            user.setAddress(this.address);
        }
        
        if (!StringUtils.isEmpty(this.province)) {
            user.setProvince(this.province);
        }
        
        if (!StringUtils.isEmpty(this.dob)) {
            user.setDob(this.dob);
        }
        
        return user;
    }
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public Boolean getCodAllowed() {
        return codAllowed;
    }

    public void setCodAllowed(Boolean codAllowed) {
        this.codAllowed = codAllowed;
    }

    public String getBankInfo() {
        return bankInfo;
    }

    public void setBankInfo(String bankInfo) {
        this.bankInfo = bankInfo;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getCitizenIdImageFront() {
        return citizenIdImageFront;
    }

    public void setCitizenIdImageFront(String citizenIdImageFront) {
        this.citizenIdImageFront = citizenIdImageFront;
    }

    public String getCitizenIdImageBack() {
        return citizenIdImageBack;
    }

    public void setCitizenIdImageBack(String citizenIdImageBack) {
        this.citizenIdImageBack = citizenIdImageBack;
    }

    public Boolean getConfirm() {
        return confirm;
    }

    public void setConfirm(Boolean confirm) {
        this.confirm = confirm;
    }
}
