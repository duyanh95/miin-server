package vn.anvui.bsn.dto.claim;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.entity.claim.Claim;
import vn.anvui.dt.enumeration.ClaimStatus;

public class ClaimCreationRequest {
    @NotNull
    Integer contractId;
    
    @NotNull
    private
    Integer claimType;
    
    @JsonFormat(pattern = "dd/MM/yyyy", timezone = "GMT+7:00")
    Date fromDate;
    
    @JsonFormat(pattern = "dd/MM/yyyy", timezone = "GMT+7:00")
    Date toDate;
    
    List<ClaimDocCreationRequest> documents;
    
    Integer compensationMethod;
    
    String compensationNote;
    
    public Claim createClaimFromRequest(Contract contract, User loggedUser) {
        Claim claim = new Claim();
        
        claim.setContractId(contract.getId());
        claim.setClaimType(this.getClaimType());
        claim.setClaimStatus(ClaimStatus.PROCESSING.getValue());
        claim.setComposationMethod(this.getCompensationMethod());
        claim.setCompensationNote(this.getCompensationNote());
        claim.setProductId(contract.getProductId());
        claim.setUpdateTime(new Date());
        claim.setUserId(loggedUser.getId());
        claim.setFromDate(this.getFromDate());
        claim.setToDate(this.getToDate());
        
        return claim;
    }
    
    public Claim updateClaimWithRequest(Claim claim, User loggedUser) {
        if (this.getFromDate() != null)
            claim.setFromDate(this.getFromDate());
           
        if (this.getToDate() != null)
            claim.setToDate(this.getToDate());
        
        if (this.getCompensationMethod() != null)
            claim.setComposationMethod(this.getCompensationMethod());
        
        if (this.getCompensationNote() != null)
            claim.setCompensationNote(this.getCompensationNote());
        
        claim.setUpdateTime(new Date());
        claim.setUserId(loggedUser.getId());
        
        return claim;
    }
    
    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public List<ClaimDocCreationRequest> getDocuments() {
        return documents;
    }

    public void setDocuments(List<ClaimDocCreationRequest> documents) {
        this.documents = documents;
    }

    public Integer getCompensationMethod() {
        return compensationMethod;
    }

    public void setCompensationMethod(Integer compensationMethod) {
        this.compensationMethod = compensationMethod;
    }

    public String getCompensationNote() {
        return compensationNote;
    }

    public void setCompensationNote(String compensationNote) {
        this.compensationNote = compensationNote;
    }

    public Integer getClaimType() {
        return claimType;
    }

    public void setClaimType(Integer claimType) {
        this.claimType = claimType;
    }
}
