package vn.anvui.bsn.dto.claim;

import java.util.List;

import javax.validation.constraints.NotNull;

public class ClaimUpdateStatusRequest {
    @NotNull
    Integer claimStatus;
    
    List<Integer> acceptClaimDocId;

    public Integer getClaimStatus() {
        return claimStatus;
    }

    public void setClaimStatus(Integer claimStatus) {
        this.claimStatus = claimStatus;
    }

    public List<Integer> getAcceptClaimDocId() {
        return acceptClaimDocId;
    }

    public void setAcceptClaimDocId(List<Integer> acceptClaimDocId) {
        this.acceptClaimDocId = acceptClaimDocId;
    }
}
