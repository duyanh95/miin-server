package vn.anvui.bsn.dto.miinContract;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;

public class HealthContractRequest extends MiinContractRequest {
    
    @NotNull
    Integer packageId;
    
    @NotNull
    Integer periodType;
    
    @JsonFormat(pattern = "dd/MM/yyyy", timezone = "GMT+7:00")
    @NotNull
    Date dob;
    
    @NotEmpty
    private
    String citizenId;

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public Integer getPeriodType() {
        return periodType;
    }

    public void setPeriodType(Integer periodType) {
        this.periodType = periodType;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }
}
