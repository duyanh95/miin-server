package vn.anvui.bsn.dto.file;

public class FileObject {
    byte[] content;
    
    String fileName;
    
    private String contentType;
    
    public FileObject() {
        super();
    }
    
    public FileObject(byte[] content, String fileName, String contentType) {
        super();
        this.content = content;
        this.fileName = fileName;
        this.contentType = contentType;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
