package vn.anvui.bsn.dto.mailing;

import java.io.File;

public class AttachmentFile {
    File file;
    
    String fileName;
    
    private FileType fileType;
    
    public enum FileType {
        PDF("application/pdf");
        
        private String value;
        
        FileType(final String newValue) {
            value = newValue;
        }
        
        public String getValue() {
            return value;
        }
    }
    
    public AttachmentFile(String path, String fileName, FileType fileType) {
        super();
        this.file = new File(path);

        
        this.fileName = fileName;
        this.fileType = fileType;
    }
    
    public AttachmentFile(File file, String fileName, FileType fileType) {
        super();
        this.file = file;
        this.fileName = fileName;
        this.fileType = fileType;
    }

    public File getFile() {
        return file;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public FileType getFileType() {
        return fileType;
    }

}
