package vn.anvui.bsn.dto.sms;

import java.util.List;

public class SendToSpeedSmsRequest {
    private List<String> to;
    
    private String content;
    
    private String smsType;
    
    private String brandName;
    
    public SendToSpeedSmsRequest(List<String> to, String content, String smsType, String brandName) {
        this.to = to;
        this.content = content;
        this.smsType = smsType;
        this.brandName = brandName;
    }

    public List<String> getTo() {
        return to;
    }

    public void setTo(List<String> to) {
        this.to = to;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSmsType() {
        return smsType;
    }

    public void setSmsType(String smsType) {
        this.smsType = smsType;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
