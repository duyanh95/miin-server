package vn.anvui.bsn.dto.transaction.viettelpay;

public class ViettelPayCheckTransactionRequest {
    String billcode;
    
    String merchant_code;
    
    String order_id;
    
    String check_sum;

    public ViettelPayCheckTransactionRequest() {
        super();
    }
    
    public ViettelPayCheckTransactionRequest(String billcode, String merchant_code, String order_id, String check_sum) {
        super();
        this.billcode = billcode;
        this.merchant_code = merchant_code;
        this.order_id = order_id;
        this.check_sum = check_sum;
    }

    public String getBillCode() {
        return billcode;
    }

    public String getMerchant_code() {
        return merchant_code;
    }

    public String getOrder_id() {
        return order_id;
    }

    public String getCheck_sum() {
        return check_sum;
    }
}
