package vn.anvui.bsn.dto.account;

import org.hibernate.validator.constraints.NotEmpty;

public class OTPResetRequest {
    @NotEmpty
    private String phoneNumber;
    
    @NotEmpty
    private String otpCode;
  
    @NotEmpty
    private String password;
    
    @NotEmpty
    private String passwordConfirm;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getOtpCode() {
        return otpCode;
    }

    public void setOtpCode(String otpCode) {
        this.otpCode = otpCode;
    }
}
