package vn.anvui.bsn.dto.mailing;

import java.util.List;

import javax.validation.constraints.NotNull;

public class MailRequest {
    @NotNull
    List<String> toList;
    
    @NotNull
    String content;
    
    @NotNull
    String subject;
    
    List<String> ccList;
    
    List<String> bccList;

    public List<String> getToList() {
        return toList;
    }

    public void setToList(List<String> toList) {
        this.toList = toList;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<String> getCcList() {
        return ccList;
    }

    public void setCcList(List<String> ccList) {
        this.ccList = ccList;
    }

    public List<String> getBccList() {
        return bccList;
    }

    public void setBccList(List<String> bccList) {
        this.bccList = bccList;
    }
    
    
}
