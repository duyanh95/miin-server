package vn.anvui.bsn.dto.transaction.vipay;

import java.util.Map;

import vn.anvui.bsn.dto.transaction.TransactionStatusRequest;

public class ViPayTransactionStatusRequest extends TransactionStatusRequest {
    private Map<String, String[]> params;

    public Map<String, String[]> getParams() {
        return params;
    }

    public void setParams(Map<String, String[]> params) {
        this.params = params;
    }
}
