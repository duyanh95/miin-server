package vn.anvui.bsn.dto.role;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

public class RoleRequest {
    
    @NotEmpty
    private String name;
    
    @NotEmpty
    private List<String> privileges;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(List<String> privileges) {
        this.privileges = privileges;
    }
}
