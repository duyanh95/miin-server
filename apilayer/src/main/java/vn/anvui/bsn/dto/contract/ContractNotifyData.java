package vn.anvui.bsn.dto.contract;

import vn.anvui.dt.enumeration.PhaseActionType;
import vn.anvui.dt.model.ContractWrapper;

public class ContractNotifyData {
    ContractWrapper contractWrapper;
    
    PhaseActionType action;

    public ContractNotifyData(ContractWrapper contract, PhaseActionType action) {
        super();
        this.contractWrapper = contract;
        this.action = action;
    }

    public ContractWrapper getContractWrapper() {
        return contractWrapper;
    }

    public PhaseActionType getAction() {
        return action;
    }
}
