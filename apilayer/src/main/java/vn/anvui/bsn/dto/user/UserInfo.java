package vn.anvui.bsn.dto.user;

import vn.anvui.dt.repos.user.UserRepository;

public class UserInfo {
    Integer id;
    
    String userName;
                                                                                            
    public UserInfo() {
        super();
    }

    public UserInfo(Integer id, String userName) {
        super();
        this.id = id;
        this.userName = userName;
    }
    
    public UserInfo(UserRepository.UserIdNameOnly user) {
        super();
        this.id = user.getId();
        this.userName = user.getUserName();
    }

    public Integer getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }
}
