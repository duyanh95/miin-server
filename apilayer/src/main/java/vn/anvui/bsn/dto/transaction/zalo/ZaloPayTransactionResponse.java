package vn.anvui.bsn.dto.transaction.zalo;

public class ZaloPayTransactionResponse {
    Integer returncode;
    
    String returnmessage;

    public Integer getReturncode() {
        return returncode;
    }

    public void setReturncode(Integer returncode) {
        this.returncode = returncode;
    }

    public String getReturnmessage() {
        return returnmessage;
    }

    public void setReturnmessage(String returnmessage) {
        this.returnmessage = returnmessage;
    }
}
