package vn.anvui.bsn.dto.transaction.zalo;

import vn.anvui.bsn.dto.transaction.TransactionStatusRequest;

public class ZaloPayTransactionStatusRequest extends TransactionStatusRequest {
    String data;
    
    String mac;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
}
