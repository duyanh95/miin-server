package vn.anvui.bsn.dto.customer;

import java.util.Date;
import java.util.logging.Logger;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;

import vn.anvui.dt.entity.Contract;
import vn.anvui.dt.entity.Customer;
import vn.anvui.dt.enumeration.TransactionType;
import vn.anvui.dt.utils.DateTimeHelper;

public class CoupleRequest {
    @NotEmpty
    String customerName;
    
    @NotEmpty
    String citizenId;
    
    @NotNull
    Integer gender;
    
    @NotNull
    @Min(0)
    Long dob;
    
    String photo;

    @NotEmpty
    String phoneNumber;
    
    @NotNull
    String email;
    
    @NotEmpty
    String partnerName;
    
    @NotEmpty
    String partnerCitizenId;
    
    @NotNull
    Integer partnerGender;
    
    @NotNull
    @Min(0)
    Long partnerDob;
    
    String partnerPhoto;
    
    @NotNull
    Integer packageId;
    
    Integer trackingId;
    
    Integer status = null;
    
    String address;
    
    Integer txnType = TransactionType.ONLINE_TXN.getValue();
    
    String note;
    
    @JsonFormat(pattern = "dd-MM-yyyy")
    Date startDate;
    
    public CoupleRequest() {
        super();
    }
    
    public CoupleRequest(Contract contract) {
        super();
        Customer customer = contract.getInheritCustomer();
        Customer partner = contract.getPartner();
        
        this.setCustomerName(customer.getCustomerName());
        this.setCitizenId(customer.getCitizenId());
        this.setDob(customer.getDob().getTime());
        this.setGender(customer.getGender());
        
        this.setPhoneNumber(customer.getPhoneNumber());
        this.setEmail(customer.getEmail());
        
        this.setPartnerName(partner.getCustomerName());
        this.setPartnerDob(partner.getDob().getTime());
        this.setPartnerCitizenId(partner.getCitizenId());
        this.setPartnerGender(partner.getGender());
        
        this.setPackageId(contract.getPackageId());
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Long getDob() {
        return dob;
    }

    public void setDob(Long dob) {
        this.dob = dob;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getPartnerCitizenId() {
        return partnerCitizenId;
    }

    public void setPartnerCitizenId(String partnerCitizenId) {
        this.partnerCitizenId = partnerCitizenId;
    }

    public Integer getPartnerGender() {
        return partnerGender;
    }

    public void setPartnerGender(Integer partnerGender) {
        this.partnerGender = partnerGender;
    }

    public Long getPartnerDob() {
        return partnerDob;
    }

    public void setPartnerDob(Long partnerDob) {
        this.partnerDob = partnerDob;
    }

    public String getPartnerPhoto() {
        return partnerPhoto;
    }

    public void setPartnerPhoto(String partnerPhoto) {
        this.partnerPhoto = partnerPhoto;
    }

    public Integer getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(Integer trackingId) {
        this.trackingId = trackingId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getTxnType() {
        return txnType;
    }

    public void setTxnType(Integer txnType) {
        this.txnType = txnType;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public boolean ageIsValid() {
        Logger log = Logger.getLogger("validAge");
        Date currentDate = new Date();
        
        Date lowerLimit = DateTimeHelper.addDay(currentDate, DateTimeHelper.YEAR, -40);
        Date upperLimit = DateTimeHelper.addDay(currentDate, DateTimeHelper.YEAR, -16);
        
        log.info("dob must be between " + lowerLimit + " and " + upperLimit);
        
        return (this.partnerDob >= lowerLimit.getTime() && this.partnerDob <= upperLimit.getTime())
                || (this.dob >= lowerLimit.getTime() && this.dob <= upperLimit.getTime());
    }
} 
