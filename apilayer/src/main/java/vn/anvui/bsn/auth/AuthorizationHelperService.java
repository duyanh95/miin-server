package vn.anvui.bsn.auth;

import vn.anvui.dt.entity.User;
import vn.anvui.dt.enumeration.PTIPrivileges;

public interface AuthorizationHelperService {
    User getLoggedUser();
    
    boolean hasPrivilege(PTIPrivileges... privileges);
}
