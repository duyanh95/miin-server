package vn.anvui.bsn.auth;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import vn.anvui.dt.common.AuthenticationDetails;
import vn.anvui.dt.entity.Privilege;
import vn.anvui.dt.entity.User;

@SuppressWarnings("serial")
public class AnvuiAuthenticationToken extends PreAuthenticatedAuthenticationToken {
    
    static Logger log = Logger.getLogger(AnvuiAuthenticationToken.class.getSimpleName());

    private boolean permissionChanged = false;
    
    public AnvuiAuthenticationToken(User user, Collection<Privilege> privileges, boolean permissionChanged) {
        super(user.getUserName(), user.getPassword(), getAuthoritiesFromUser(privileges));
        this.permissionChanged = permissionChanged;
    }
    
    public AnvuiAuthenticationToken(User user, Collection<Privilege> privileges) {
        this(user, privileges, false);
    }

    public static List<SimpleGrantedAuthority> getAuthoritiesFromUser(Collection<Privilege> privileges) {
        final List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        
        privileges.forEach(privilege -> {
            authorities.add(new SimpleGrantedAuthority(privilege.getName()));
        });
        
        return authorities;
    }

    public String getToken() {
        return ((AuthenticationDetails) getDetails()).getAccessToken().getToken();
    }

    public Date getExpiredDate() {
        return ((AuthenticationDetails) getDetails()).getExpiredDate();
    }
    
    public User getUser() {
        return ((AuthenticationDetails) getDetails()).getUser();
    }

    public boolean isPermissionChanged() {
        return permissionChanged;
    }

    public void setPermissionChanged(boolean permissionChanged) {
        this.permissionChanged = permissionChanged;
    }
}
