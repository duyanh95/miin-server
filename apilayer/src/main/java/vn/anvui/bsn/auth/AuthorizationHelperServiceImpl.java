package vn.anvui.bsn.auth;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import vn.anvui.dt.common.AuthenticationDetails;
import vn.anvui.dt.entity.User;
import vn.anvui.dt.enumeration.PTIPrivileges;
import vn.anvui.dt.repos.userClosure.UserClosureRepository;

@Service
public class AuthorizationHelperServiceImpl implements AuthorizationHelperService {
    
    @Override
    public User getLoggedUser() {
        try {
            AuthenticationDetails userDetails = (AuthenticationDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getDetails();
            return userDetails.getUser();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean hasPrivilege(PTIPrivileges... privileges) {
        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        
        List<String> authoritiesStr = authorities.stream().map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        
        for (PTIPrivileges privilege : privileges) {
            if (!authoritiesStr.contains(privilege.getValue())) {
                return false;
            }
        }
        
        return true;
    }
    
}
