package vn.anvui.bsn.utils;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

public final class DataTransferObjectHelper {

	private static final Logger logger = LoggerFactory.getLogger(DataTransferObjectHelper.class);

	private final static String FILTER_PROPERTIES_BY_NAME = "FILTER_PROPERTIES_BY_NAME";
	private final static ObjectMapper mapper = new ObjectMapper();

	@JsonFilter(FILTER_PROPERTIES_BY_NAME)
	private static class PropertyFilterMixIn {
	}

	static {
		mapper.addMixIn(Object.class, PropertyFilterMixIn.class);
	}

	/**
	 * serialize all properties but some properties which listed in array will
	 * be ignore
	 */
	// public final static String EXCLUDE_MODE = "exclude";

	/**
	 * Only properties listed in array will be serialize
	 */
	// public final static String INCLUDE_MODE = "include";
	private DataTransferObjectHelper() {
	}

	/**
	 * @author duongbui
	 * @param source
	 * @return
	 */
	public static String[] getIgnoreProperties(Object source) {

		BeanWrapper beanWrapper = new BeanWrapperImpl(source);

		PropertyDescriptor[] propertyDescriptors = beanWrapper.getPropertyDescriptors();

		Set<String> emptyProperties = new HashSet<String>();

		for (PropertyDescriptor descriptor : propertyDescriptors) {

			Object propertyValue = beanWrapper.getPropertyValue(descriptor.getName());

			if (propertyValue == null) {
				emptyProperties.add(descriptor.getName());
			}
		}

		String[] result = new String[emptyProperties.size()];

		return emptyProperties.toArray(result);
	}

	/**
	 * 
	 * @param source
	 * @param extra
	 * @param properties
	 * @return Map<Object, Object>
	 * @throws FruitBaseException
	 */
	public static Map<Object, Object> simplify(Object source, String[] properties, Map<Object, Object> extra) {

		Map<Object, Object> output = simplify(source, properties);
		if (extra != null && !extra.isEmpty()) {
			for (Map.Entry<Object, Object> e : extra.entrySet()) {
				output.put(e.getKey(), e.getValue());
			}
		}

		return output;
	}

	public static Map<Object, Object> simplify(Object source, String... properties) {

		Map<Object, Object> output = null;

		FilterProvider filters = new SimpleFilterProvider().addFilter(FILTER_PROPERTIES_BY_NAME,
				SimpleBeanPropertyFilter.filterOutAllExcept(properties));

		ObjectWriter writer = mapper.writer(filters);

		try {
			String json = writer.writeValueAsString(source);

			output = mapper.readValue(json, new TypeReference<HashMap<Object, Object>>() {
			});

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return output;
	}

	public static <T> Collection<Object> simplify(Collection<T> sources, String... properties) {

		Collection<Object> output = null;
		if (sources != null && !sources.isEmpty()) {
			output = new ArrayList<Object>();
			for (Object source : sources) {
				Object simpleObject = simplify(source, properties);
				output.add(simpleObject);
			}
		}
		return output;
	}

	public static Map<Object, Object> convertToMap(Object source, String... ignoreProperties) {

		Map<Object, Object> shallowCopy = null;

		if (source != null) {

			shallowCopy = new HashMap<Object, Object>();
			BeanWrapper beanWrapper = new BeanWrapperImpl(source);
			PropertyDescriptor[] propertyDescriptors = beanWrapper.getPropertyDescriptors();

			for (PropertyDescriptor descriptor : propertyDescriptors) {

				String key = descriptor.getName();
				if (!"class".equals(key) && !Arrays.asList(ignoreProperties).contains(key)) {
					Object value = beanWrapper.getPropertyValue(key);
					shallowCopy.put(key, value);
				}
			}
		}
		return shallowCopy;
	}

	public static Map<Object, Object> convertToMap(Object source, Map<Object, Object> extra,
			String... ignoreProperties) {

		Map<Object, Object> output = convertToMap(source, ignoreProperties);
		if (extra != null && !extra.isEmpty()) {
			for (Map.Entry<Object, Object> e : extra.entrySet()) {
				output.put(e.getKey(), e.getValue());
			}
		}

		return output;

	}

//	public static List<Map<Object, Object>> fromJsonArrayString(String json) {
//		if (StringUtils.isEmpty(json)) {
//			return null;
//		}
//		JSONArray jsonArray = new JSONArray(json);
//		List<Map<Object, Object>> list = new ArrayList<Map<Object, Object>>();
//		for (int i = 0; i < jsonArray.length(); i++) {
//			list.add((Map<Object, Object>) fromJsonString(jsonArray.get(i).toString()));
//		}
//		return list;
//	}

	/**
	 * TODO: REVIEW IMPACT
	 * ******************************************************
	 * **********************************
	 * 
	 * @param json
	 * @param clazz
	 * @return
	 */
	public static <T> T fromJsonString(String json, Class<T> clazz) {

		T t = null;
		try {
			t = new ObjectMapper().readValue(json, clazz);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return t;
	}

	public static Map<Object, Object> fromJsonString(String json) {
		if (json == null) {
			return null;
		}
		Map<Object, Object> sObject = null;
		try {
			sObject = new ObjectMapper().readValue(json, new TypeReference<HashMap<Object, Object>>() {
			});
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return sObject;
	}

	public static String toJsonString(Object object) {

		String json = null;
		try {
			json = new ObjectMapper().writeValueAsString(object);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return json;
	}

	public static void copyBeanProperties(final Object source, final Object target, final String... properties) {

		final BeanWrapper src = new BeanWrapperImpl(source);
		final BeanWrapper trg = new BeanWrapperImpl(target);

		for (final String propertyName : properties) {
			trg.setPropertyValue(propertyName, src.getPropertyValue(propertyName));
		}

	}

	public static void copy(Object source, Object target) {
		BeanUtils.copyProperties(source, target, getIgnoreProperties(source));
	}

	/**
	 * Extra must be a valid json string
	 * 
	 * @param extra
	 * @return
	 */
	@Deprecated
	public static Map<String, Object> buildVelocityData(String notificationExtra) {

		Map<String, Object> data = new HashMap<>();

		Map<Object, Object> extra = DataTransferObjectHelper.fromJsonString(notificationExtra);

		if (extra != null && !extra.isEmpty()) {
			for (Map.Entry<Object, Object> entry : extra.entrySet()) {
				data.put((String) entry.getKey(), entry.getValue());
			}
		}
		return data;
	}
}
