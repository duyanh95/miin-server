-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: 192.168.1.253    Database: PTI
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `claim_type`
--

LOCK TABLES `claim_type` WRITE;
/*!40000 ALTER TABLE `claim_type` DISABLE KEYS */;
INSERT  IGNORE INTO `claim_type` (`id`, `product_id`, `name`) VALUES (1,1,NULL),(2,2,NULL),(3,3,'ốm đau'),(4,3,'ốm đau, phẫu thuật'),(5,3,'tai nạn'),(6,3,'tai nạn, phẫu thuật'),(7,3,'tai nạn, tử vong'),(8,3,'tai nạn, phẫu thuật, tử vong'),(9,4,NULL);
/*!40000 ALTER TABLE `claim_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `claim_type_requirement`
--

LOCK TABLES `claim_type_requirement` WRITE;
/*!40000 ALTER TABLE `claim_type_requirement` DISABLE KEYS */;
INSERT  IGNORE INTO `claim_type_requirement` (`claim_type_id`, `product_claim_requirement_id`) VALUES (1,1),(1,2),(1,3),(3,4),(4,4),(5,4),(6,4),(7,4),(8,4),(3,5),(4,5),(5,5),(6,5),(7,5),(8,5),(3,6),(4,6),(5,6),(6,6),(7,6),(8,6),(3,7),(4,7),(5,7),(6,7),(7,7),(8,7),(4,8),(6,8),(8,8),(5,9),(7,9),(8,9),(7,10),(8,10),(8,11),(8,12);
/*!40000 ALTER TABLE `claim_type_requirement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `package`
--

LOCK TABLES `package` WRITE;
/*!40000 ALTER TABLE `package` DISABLE KEYS */;
INSERT  IGNORE INTO `package` (`id`, `package_name`, `fee`, `insurance`, `description`, `active`, `product_id`, `image`, `additional_info`, `has_period_option`) VALUES (1,'Hân hoan lời chào',480000,8000000,'Nhận ngay 8 triệu sau khi kết hôn',1,1,'https://storage.googleapis.com/miin/bhty1.png','{\"waitTime\":\"{\\\"years\\\":3}\",\"endTime\":\"{\\\"years\\\":5}\"}','\0'),(2,'Say đắm quyến rũ',1180000,20000000,'Nhận ngay  20 triệu sau khi kết hôn',1,1,'https://storage.googleapis.com/miin/bhty2.png','{\"waitTime\":\"{\\\"years\\\":3}\",\"endTime\":\"{\\\"years\\\":5}\"}','\0'),(3,'Đam mê bất tận',1850000,30000000,'Nhận ngay 30 triệu sau khi kết hôn',1,1,'https://storage.googleapis.com/miin/bhty3.png','{\"waitTime\":\"{\\\"years\\\":3}\",\"endTime\":\"{\\\"years\\\":5}\"}','\0'),(4,'test',15000,150000,'test',0,1,NULL,NULL,'\0'),(5,'Bạc',400000,0,'HỖ TRỢ\n100.000/ngày nằm viện\n1 triệu/lần phẫu thuật',1,3,'https://storage.googleapis.com/miin/bhnv1.png','100000,1000000,10000000',''),(6,'Vàng',800000,0,'HỖ TRỢ\n200.000/ngày nằm viện\n2 triệu/lần phẫu thuật',1,3,'https://storage.googleapis.com/miin/bhnv2.png','200000,2000000,20000000',''),(7,'Kim Cương',1200000,0,'HỖ TRỢ\n300.000/ngày nằm viện\n3 triệu/lần phẫu thuật',1,3,'https://storage.googleapis.com/miin/bhnv3.png','300000,3000000,30000000',''),(8,'Gói 1',66000,0,'Bảo hiểm xe máy bắt buộc',1,4,'https://storage.googleapis.com/miin/bhxm1.png','',''),(9,'Gói 2',76000,0,'Bảo hiểm xe máy bắt buộc\nBảo hiểm tại nạn người trên xe hạn mức 5 triệu/người/vụ',1,4,'https://storage.googleapis.com/miin/bhxm2.png','','\0'),(10,'Gói 3',86000,0,'Bảo hiểm xe máy bắt buộc\nBảo hiểm tai nạn người trên xe hạn mức 10 triệu/người/vụ',1,4,'https://storage.googleapis.com/miin/bhxm3.png','','\0');
/*!40000 ALTER TABLE `package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `period_package`
--

LOCK TABLES `period_package` WRITE;
/*!40000 ALTER TABLE `period_package` DISABLE KEYS */;
INSERT  IGNORE INTO `period_package` (`id`, `package_id`, `period_type`, `fee`) VALUES (1,5,0,8400),(2,5,1,36000),(3,5,2,432000),(4,6,0,16800),(5,6,1,72000),(6,6,2,864000),(7,7,0,25200),(8,7,1,108000),(9,7,2,1296000);
/*!40000 ALTER TABLE `period_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `privilege`
--

LOCK TABLES `privilege` WRITE;
/*!40000 ALTER TABLE `privilege` DISABLE KEYS */;
INSERT  IGNORE INTO `privilege` (`id`, `name`, `created_date`) VALUES (1,'READ_CONTRACT',NULL),(2,'CREATE_CONTRACT',NULL),(3,'UPDATE_CONTRACT',NULL),(4,'DELETE_CONTRACT',NULL),(5,'READ_ADMIN',NULL),(6,'CREATE_ADMIN',NULL),(7,'UPDATE_ADMIN',NULL),(8,'DELETE_ADMIN',NULL),(9,'READ_USER',NULL),(10,'CREATE_USER',NULL),(11,'UPDATE_USER',NULL),(12,'DELETE_USER',NULL),(13,'READ_AGENCY',NULL),(14,'CREATE_AGENCY',NULL),(15,'UPDATE_AGENCY',NULL),(16,'DELETE_AGENCY',NULL),(17,'MARKETING_PARTNER',NULL);
/*!40000 ALTER TABLE `privilege` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `privilege_role`
--

LOCK TABLES `privilege_role` WRITE;
/*!40000 ALTER TABLE `privilege_role` DISABLE KEYS */;
INSERT  IGNORE INTO `privilege_role` (`privilege_id`, `role_id`) VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(1,2),(2,2),(3,2),(9,2),(13,2),(14,2),(15,2),(16,2),(1,3),(2,3),(3,3),(1,4),(2,4),(3,4),(5,4),(1,5),(5,5),(17,6),(2,7),(3,7),(9,7),(13,7),(15,7);
/*!40000 ALTER TABLE `privilege_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT  IGNORE INTO `product` (`id`, `product_name`, `order`, `package_type`, `contract_number`, `description`) VALUES (1,'bảo hiểm tình yêu',0,0,1,NULL),(2,'bảo hiểm chung cư',0,1,1,NULL),(3,'bảo hiểm nằm viện',0,0,1,NULL),(4,'bảo hiểm trách nhiệm dân sự xe máy',0,0,1,NULL);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `product_claim_requirement`
--

LOCK TABLES `product_claim_requirement` WRITE;
/*!40000 ALTER TABLE `product_claim_requirement` DISABLE KEYS */;
INSERT  IGNORE INTO `product_claim_requirement` (`id`, `name`, `number_of_image`) VALUES (1,'giấy kết hôn',1),(2,'thẻ căn cước NAM',2),(3,'thẻ căn cước NỮ',2),(4,'giấy ra viện',1),(5,'CMT/Thẻ căn cước/GIấy khai sinh',2),(6,'Hóa đơn biên lai thu tiền',1),(7,'Biên lai thu tiền (bảng kê chi tiết dịch vụ trong bện viện)',1),(8,'Chứng nhận phẩu thuật (nếu có)',1),(9,'Biên bản hoặc đơn trình báo tai nạn',1),(10,'Giấy chứng tử (nếu có)',1),(11,'Giấy ủy quyền (nếu có)',1),(12,'Giấy tờ  chứng minh quyền thừa kế (nếu có)',1);
/*!40000 ALTER TABLE `product_claim_requirement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `province`
--

LOCK TABLES `province` WRITE;
/*!40000 ALTER TABLE `province` DISABLE KEYS */;
INSERT  IGNORE INTO `province` (`id`, `province`) VALUES (1,'Hà Nội'),(2,'Hà Giang'),(4,'Cao Bằng'),(6,'Bắc Kạn'),(8,'Tuyên Quang'),(10,'Lào Cai'),(11,'Điện Biên'),(12,'Lai Châu'),(14,'Sơn La'),(15,'Yên Bái'),(17,'Hoà Bình'),(19,'Thái Nguyên'),(20,'Lạng Sơn'),(22,'Quảng Ninh'),(24,'Bắc Giang'),(25,'Phú Thọ'),(26,'Vĩnh Phúc'),(27,'Bắc Ninh'),(30,'Hải Dương'),(31,'Hải Phòng'),(33,'Hưng Yên'),(34,'Thái Bình'),(35,'Hà Nam'),(36,'Nam Định'),(37,'Ninh Bình'),(38,'Thanh Hóa'),(40,'Nghệ An'),(42,'Hà Tĩnh'),(44,'Quảng Bình'),(45,'Quảng Trị'),(46,'Thừa Thiên Huế'),(48,'Đà Nẵng'),(49,'Quảng Nam'),(51,'Quảng Ngãi'),(52,'Bình Định'),(54,'Phú Yên'),(56,'Tỉnh Khánh Hò'),(58,'Ninh Thuận'),(60,'Bình Thuận'),(62,'Kon Tum'),(64,'Gia Lai'),(66,'Đắk Lắk'),(67,'Đắk Nông'),(68,'Lâm Đồng'),(70,'Bình Phước'),(72,'Tây Ninh'),(74,'Bình Dương'),(75,'Đồng Nai'),(77,'Bà Rịa - Vũng Tàu'),(79,'Thành phố Hồ Chí Minh'),(80,'Long An'),(82,'Tiền Giang'),(83,'Bến Tre'),(84,'Trà Vinh'),(86,'Vĩnh Long'),(87,'Đồng Tháp'),(89,'An Giang'),(91,'Kiên Giang'),(92,'Cần Thơ'),(93,'Hậu Giang'),(94,'Sóc Trăng'),(95,'Bạc Liêu'),(96,'Cà Mau');
/*!40000 ALTER TABLE `province` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT  IGNORE INTO `role` (`id`, `name`, `created_date`) VALUES (1,'ADMIN',NULL),(2,'AGENCY',NULL),(3,'AGENCY_USER',NULL),(4,'PTI_USER',NULL),(5,'CUSTOMER_CARE',NULL),(6,'MARKETING_PARTNER',NULL),(7,'COLLAB',NULL);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `system_variable`
--

LOCK TABLES `system_variable` WRITE;
/*!40000 ALTER TABLE `system_variable` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_variable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'PTI'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-29 14:26:22
